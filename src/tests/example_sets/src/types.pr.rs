/*SPEC*
property set<T> {
    \c <: (Container) -> ((for-all-elems c) \a -> ((unique-count? a) c))
}

type Set<S> = {c impl (Container) | (set c)}
*ENDSPEC*/

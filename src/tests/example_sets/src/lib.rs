#![feature(type_alias_impl_trait)]

mod types;
use types::Set;

use primrose_library::traits::*;
use rand::{rngs::StdRng, Rng};
use std::hint::black_box;

pub fn insert_n(rng: &mut StdRng, n: usize) {
    let mut set: Set<usize> = Set::default();

    for _ in 0..n {
        set.insert(rng.gen());
    }

    for _ in 0..2 * n {
        black_box(|x| x)(black_box(set.contains(&rng.gen())));
    }
}

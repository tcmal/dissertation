use criterion::BenchmarkId;
use criterion::Criterion;
use criterion::{criterion_group, criterion_main};

fn bench_sieve(c: &mut Criterion) {
    for size in [50, 500, 50_000].iter() {
        c.bench_with_input(BenchmarkId::new("prime_sieve", size), size, |b, &n| {
            b.iter(|| prime_sieve::prime_sieve(n));
        });
    }
}

criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(20);
    targets = bench_sieve
);
criterion_main!(benches);

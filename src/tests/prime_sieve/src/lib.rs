#![feature(type_alias_impl_trait)]

mod types;
use types::*;

use primrose_library::traits::{Container, Stack};

/// Find all primes < `n` using the sieve of eratosthenes
pub fn prime_sieve(n: usize) -> Primes<usize> {
    // Add numbers to sieve through
    let mut sieve = Sieve::default();
    (2..n).rev().for_each(|x| sieve.push(x));

    let mut primes = Primes::default();
    while let Some(x) = sieve.pop() {
        primes.insert(x);

        // remove all multiples of x from sieve
        let mut sieved = 2 * x;
        while sieved < n {
            sieve.remove(sieved);
            sieved += x;
        }
    }

    primes
}

#[cfg(test)]
mod tests {
    use crate::prime_sieve;
    use primrose_library::traits::*;
    use std::fmt::Display;

    fn assert_coneq<E: Display, T: Container<E>>(mut left: T, right: Vec<E>) {
        assert_eq!(left.len(), right.len(), "length must be equal");
        for i in right.into_iter() {
            assert!(left.contains(&i), "must container element {}", i);
        }
    }

    #[test]
    fn test10() {
        assert_coneq(prime_sieve(10), vec![2, 3, 5, 7]);
    }

    #[test]
    fn test100() {
        assert_coneq(
            prime_sieve(100),
            vec![
                2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79,
                83, 89, 97,
            ],
        );
    }
}

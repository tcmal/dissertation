/*SPEC*
property lifo<T> {
    \c <: (Stack) -> (forall \x -> ((equal? (op-pop ((op-push c) x))) x))
}

property ascending<T> {
    \c -> ((for-all-consecutive-pairs c) leq?)
}


type Sieve<S> = {c impl (Container, Stack) | (lifo c)}
type Primes<S> = {c impl (Container) | (ascending c)}
*ENDSPEC*/

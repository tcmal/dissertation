/*SPEC*
property unique<T> {
    \c <: (Container) -> ((for-all-elems c) \a -> ((unique-count? a) c))
}

type Map<K,V> = {c impl (Mapping) | true}
type Set<T> = {c impl (Container) | (unique c)}
 *ENDSPEC*/

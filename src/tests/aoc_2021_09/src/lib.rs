#![feature(type_alias_impl_trait)]

use primrose_library::traits::*;
use rand::{rngs::StdRng, Rng};
use std::collections::VecDeque;

mod types;
use types::*;

type Queue<T> = VecDeque<T>;

pub struct HeightMap {
    map: Map<(usize, usize), usize>,
    cols: usize,
    rows: usize,
}

impl HeightMap {
    pub fn new(map: Map<(usize, usize), usize>, cols: usize, rows: usize) -> Self {
        Self { map, cols, rows }
    }

    pub fn gen(rng: &mut StdRng, n: usize) -> Self {
        let mut map = Map::default();
        for x in 0..n {
            for y in 0..n {
                map.insert((x, y), rng.gen());
            }
        }

        Self {
            map,
            cols: n,
            rows: n,
        }
    }

    fn is_low_point(&self, x: usize, y: usize) -> bool {
        [
            self.map.get(&(x + 1, y)),
            self.map.get(&(x.overflowing_sub(1).0, y)),
            self.map.get(&(x, y + 1)),
            self.map.get(&(x, y.overflowing_sub(1).0)),
        ]
        .into_iter()
        .flatten()
        .all(|v| v > self.map.get(&(x, y)).unwrap())
    }

    fn risk_level(&self, x: usize, y: usize) -> usize {
        self.map.get(&(x, y)).unwrap() + 1
    }

    pub fn part1(&self) -> usize {
        let coords = (0..self.cols).flat_map(|x| (0..self.rows).map(move |y| (x, y)));

        coords
            .filter(|(x, y)| self.is_low_point(*x, *y))
            .map(|(x, y)| self.risk_level(x, y))
            .sum::<usize>()
    }

    fn basin_size(&self, x: usize, y: usize) -> usize {
        let mut visited = Set::default();
        let mut queue = Queue::default();
        queue.push_back((x, y));

        let mut size = 0;
        while let Some((x, y)) = queue.pop_front() {
            if !(0..self.cols).contains(&x)
                || !(0..self.rows).contains(&y)
                || visited.contains(&(x, y))
            {
                continue;
            }

            let val = self.map.get(&(x, y)).unwrap();
            if *val == 9 {
                continue;
            }

            size += 1;
            visited.insert((x, y));
            for coord in [
                (x + 1, y),
                (x.overflowing_sub(1).0, y),
                (x, y + 1),
                (x, y.overflowing_sub(1).0),
            ] {
                queue.push_back(coord)
            }
        }

        size
    }

    pub fn part2(&self) -> usize {
        let mut basins = (0..self.cols)
            .flat_map(|x| (0..self.rows).map(move |y| (x, y)))
            .filter(|(x, y)| self.is_low_point(*x, *y))
            .map(|(x, y)| self.basin_size(x, y))
            .collect::<Vec<_>>();

        basins.sort();
        basins.reverse();
        basins.iter().take(3).product()
    }
}

#[cfg(test)]
mod tests {
    use crate::HeightMap;

    macro_rules! mkRow {
        ($($v:expr) *) => {{
            let mut c = Vec::default();
            $(c.push($v);)*
            c
        }}
    }
    macro_rules! mkMap {
        ($rows:expr, $cols:expr, $($r:expr),*) => {{
            let mut c = Vec::default();
            $(
                let r = $r;
                assert_eq!(r.len(), $cols);
                c.push($r);
            )*
            assert_eq!(c.len(), $rows);
            HeightMap::new(
                c.into_iter().enumerate().flat_map(|(y, xs)| xs.into_iter().enumerate().map(move |(x, v)| ((x, y), v))).collect(),
                $cols,
                $rows
            )
        }}
    }

    #[test]
    fn test_small() {
        let map = small_input();

        assert_eq!(map.part1(), 15);
        assert_eq!(map.part2(), 1134);
    }

    #[test]
    fn test_large() {
        let map = large_input();

        assert_eq!(map.part1(), 560);
        assert_eq!(map.part2(), 959136);
    }

    fn small_input() -> HeightMap {
        mkMap! {
            5, 10,
            mkRow!{2 1 9 9 9 4 3 2 1 0},
            mkRow!{3 9 8 7 8 9 4 9 2 1},
            mkRow!{9 8 5 6 7 8 9 8 9 2},
            mkRow!{8 7 6 7 8 9 6 7 8 9},
            mkRow!{9 8 9 9 9 6 5 6 7 8}
        }
    }

    fn large_input() -> HeightMap {
        mkMap! {
            100, 100,
            mkRow!{9 7 5 6 7 8 9 5 4 5 9 8 7 6 5 2 3 4 5 8 9 1 0 1 2 3 6 7 8 9 9 9 8 9 9 9 1 3 5 9 9 9 9 8 6 6 4 3 5 9 7 4 3 2 1 9 2 1 2 3 4 9 8 9 9 9 4 3 4 6 6 7 9 8 9 8 7 9 9 9 9 8 7 6 5 4 9 8 7 6 5 4 3 5 6 9 7 6 5 4},
            mkRow!{6 5 4 5 8 9 8 9 5 9 8 7 6 5 4 1 2 3 6 7 8 9 2 4 5 4 5 8 9 8 9 8 7 8 9 8 9 4 9 8 9 8 7 6 5 4 3 2 3 9 6 5 6 3 9 8 9 2 9 4 9 8 7 8 9 8 9 1 3 4 5 6 9 6 5 6 6 7 8 9 9 7 6 5 4 3 4 9 9 7 9 8 5 6 7 8 9 5 4 3},
            mkRow!{5 4 3 4 5 6 7 8 9 9 7 6 5 4 3 0 4 4 5 6 7 8 9 9 7 9 6 9 8 7 6 5 6 9 8 7 8 9 8 7 6 9 9 5 4 3 2 1 9 8 9 6 7 9 8 7 8 9 8 9 4 3 6 5 6 7 8 9 4 5 6 9 8 7 4 4 5 6 7 9 9 8 7 6 5 1 3 4 5 9 8 7 6 7 8 9 9 4 3 2},
            mkRow!{6 5 4 5 6 7 8 9 3 2 9 8 4 3 2 1 2 6 7 7 8 9 7 8 9 8 9 1 9 9 7 6 7 8 9 6 7 8 9 6 5 9 8 7 5 9 9 0 9 7 8 9 9 8 7 6 7 6 7 8 9 2 3 4 7 8 9 6 5 9 9 8 7 6 3 2 4 5 6 7 8 9 6 5 4 3 5 6 7 8 9 8 7 8 9 7 8 9 2 1},
            mkRow!{7 6 5 7 7 8 9 7 9 3 9 6 5 4 3 2 4 5 7 8 9 4 6 9 8 7 9 0 1 9 8 9 8 9 4 5 6 7 8 9 4 7 9 8 9 8 8 9 8 6 6 7 8 9 6 5 5 5 6 7 8 9 4 5 6 7 8 9 7 8 9 9 7 5 4 3 8 6 7 8 9 8 7 8 5 4 6 7 8 9 3 9 8 9 5 6 7 8 9 2},
            mkRow!{9 8 6 9 8 9 5 6 7 9 8 7 6 5 6 6 5 6 8 9 2 3 9 8 7 6 8 9 2 4 9 9 9 2 3 6 5 8 9 2 3 5 7 9 8 7 6 7 6 5 5 7 8 9 9 4 3 4 5 6 7 8 9 6 9 8 9 9 8 9 9 8 7 6 5 6 7 8 9 9 3 9 9 9 6 5 7 9 9 3 2 1 9 3 4 5 6 9 4 3},
            mkRow!{4 9 8 9 9 5 4 2 3 4 9 8 9 8 7 8 6 7 9 0 1 9 8 7 6 5 7 8 9 9 8 9 9 1 2 3 4 5 9 5 4 5 6 7 9 6 5 4 3 4 3 4 6 7 8 9 2 3 6 7 8 9 9 9 8 9 8 7 9 9 9 9 8 7 8 9 8 9 9 1 2 3 9 8 7 9 8 9 5 2 1 0 1 2 3 9 7 8 9 4},
            mkRow!{3 4 9 6 5 4 3 1 2 9 9 9 8 9 8 8 7 8 9 3 2 9 8 7 6 4 3 9 9 7 7 7 8 9 3 4 5 7 8 9 5 9 7 9 8 7 6 3 2 1 2 9 7 8 9 2 1 2 3 4 9 9 9 8 7 8 9 6 9 8 9 8 9 8 9 7 9 7 8 9 9 4 5 9 8 9 9 5 4 3 2 4 2 6 7 8 9 9 6 5},
            mkRow!{2 1 2 9 6 9 2 0 9 8 9 8 7 5 9 9 9 9 8 9 9 8 7 6 5 3 2 9 8 6 5 6 7 8 9 6 8 9 9 7 6 7 8 9 9 8 5 4 9 0 9 8 9 9 4 3 2 3 5 6 7 9 8 7 6 7 8 9 8 7 6 7 8 9 7 6 6 6 5 9 8 9 6 8 9 8 7 6 5 4 3 5 3 4 9 9 9 8 7 6},
            mkRow!{3 0 9 8 9 8 9 9 8 7 5 9 5 4 3 4 5 6 7 8 9 9 8 7 6 4 3 4 9 7 4 5 6 7 8 9 9 3 9 8 7 8 9 9 8 7 6 9 8 9 8 7 8 9 6 4 3 4 9 8 9 8 9 6 5 5 7 9 9 6 5 6 7 8 9 5 4 3 4 8 7 8 9 9 9 9 8 7 8 5 4 5 4 5 6 7 8 9 9 8},
            mkRow!{5 9 8 7 6 7 8 9 7 6 4 3 4 3 2 3 5 6 8 9 6 7 9 8 6 5 4 9 8 6 5 6 7 8 9 4 3 2 3 9 8 9 2 0 9 8 9 8 7 6 7 6 9 8 7 9 7 6 8 9 8 7 6 5 3 4 6 7 9 9 6 7 8 9 9 4 3 2 3 4 6 9 4 3 9 9 9 8 9 6 6 6 5 6 7 9 9 1 0 9},
            mkRow!{9 8 7 6 5 6 7 8 9 7 3 2 1 0 1 2 6 7 8 9 5 7 7 9 7 6 5 6 9 7 6 8 9 9 6 5 5 3 9 9 9 8 9 9 9 9 8 7 6 5 4 5 8 9 8 9 8 7 9 7 9 9 4 3 2 3 5 6 7 8 9 8 9 9 8 6 5 5 4 5 6 8 9 9 8 9 9 9 9 8 7 7 6 7 9 8 8 9 1 2},
            mkRow!{9 9 6 5 4 7 8 9 8 9 4 3 2 3 2 4 5 8 9 3 4 5 6 9 8 8 9 8 9 8 9 9 3 8 9 6 6 9 8 9 9 7 8 8 9 9 9 8 9 6 5 6 7 8 9 3 9 9 8 6 5 4 3 2 1 3 3 7 8 8 9 9 3 2 9 7 6 7 6 6 8 9 9 8 7 8 9 4 3 9 8 9 7 9 8 7 7 8 9 4},
            mkRow!{9 8 5 4 3 6 5 6 7 9 6 4 5 4 6 5 6 7 8 9 5 6 7 8 9 9 9 9 9 9 0 1 2 7 8 9 9 8 7 9 8 6 7 7 8 9 1 9 9 9 8 7 8 9 1 2 4 5 9 9 8 9 4 3 0 1 2 4 5 6 7 8 9 1 9 8 7 8 7 9 9 8 6 5 6 7 8 9 2 3 9 9 9 8 6 5 6 7 8 9},
            mkRow!{8 7 4 3 2 3 4 5 6 8 9 5 6 9 7 7 8 9 9 7 6 7 9 9 8 7 8 8 9 5 4 3 5 6 9 9 8 7 6 5 9 4 5 6 9 1 0 1 9 9 9 8 9 4 3 3 5 6 9 8 7 8 7 5 1 2 3 8 6 9 8 9 1 0 1 9 8 9 9 8 7 6 5 4 5 6 7 8 9 4 5 9 8 7 5 4 5 6 8 9},
            mkRow!{7 6 5 4 5 6 5 6 9 9 9 6 7 9 8 9 9 9 8 9 7 8 9 8 7 6 8 7 8 9 5 9 6 8 9 9 8 9 5 4 3 2 3 9 8 9 1 9 8 7 8 9 6 5 9 7 6 7 9 9 6 5 4 3 2 3 4 7 8 9 9 9 9 9 9 9 9 3 9 8 6 7 6 3 4 5 6 9 9 9 9 8 7 6 4 3 4 7 9 9},
            mkRow!{8 7 6 5 7 7 6 7 8 9 8 7 8 9 9 8 7 6 7 9 9 9 0 9 6 5 5 6 9 9 9 8 9 9 9 8 7 6 4 3 2 1 9 8 7 8 9 8 7 6 9 8 7 9 8 9 9 9 8 9 8 8 6 5 3 4 5 6 7 9 9 8 7 8 8 9 1 2 9 9 5 4 3 2 3 4 5 6 7 8 9 9 8 7 5 4 5 9 8 9},
            mkRow!{9 8 7 8 9 8 7 8 9 5 9 8 9 9 8 7 6 5 6 7 8 9 1 9 5 3 4 8 9 8 6 7 8 9 3 9 8 7 6 4 3 2 9 7 6 7 9 7 6 5 4 9 9 8 7 8 7 6 7 9 9 8 7 6 4 5 6 7 8 9 9 9 6 6 7 8 9 9 8 7 6 5 4 3 4 6 6 7 8 9 6 9 8 7 6 5 9 9 7 8},
            mkRow!{4 9 8 9 2 9 8 9 3 4 5 9 9 9 9 9 7 3 2 6 7 8 9 8 9 1 9 9 8 7 5 9 9 5 4 9 9 8 7 5 4 9 8 6 5 6 8 9 6 5 3 9 8 7 6 5 4 5 6 8 9 9 9 8 7 6 7 9 9 9 9 5 4 5 6 7 8 9 9 8 7 8 5 4 5 7 8 9 9 4 5 6 9 9 7 9 8 7 6 7},
            mkRow!{3 4 9 4 3 4 9 2 1 0 2 9 8 7 9 7 6 5 3 5 6 7 9 6 7 9 8 8 9 5 4 8 9 6 9 8 9 9 7 6 9 8 7 6 4 5 6 8 9 3 2 1 9 8 7 4 3 6 5 7 8 9 9 9 8 9 8 9 9 9 8 9 3 4 5 6 8 9 9 9 8 9 8 7 6 8 9 3 4 3 4 9 2 3 9 8 9 8 5 6},
            mkRow!{4 5 9 9 9 5 9 9 2 1 2 9 8 5 9 8 7 8 7 6 7 9 4 5 9 9 7 6 5 4 3 7 8 9 9 7 8 9 9 9 8 7 6 4 3 4 5 8 9 2 1 0 9 7 6 5 2 3 4 5 9 8 9 9 9 9 9 9 7 6 7 4 2 3 4 7 9 7 8 9 9 2 9 8 7 9 4 2 1 2 9 8 9 9 8 7 6 4 3 2},
            mkRow!{5 9 8 7 8 9 7 8 9 3 9 8 7 3 2 9 8 9 8 7 9 6 5 6 7 9 8 7 6 6 5 6 7 9 5 6 7 9 9 9 6 5 4 3 2 3 5 6 8 9 2 9 8 9 7 7 3 5 5 9 8 7 8 9 9 9 8 7 6 5 4 3 1 2 3 4 5 6 7 8 9 3 9 9 9 8 9 1 0 9 8 7 8 9 7 6 5 4 2 1},
            mkRow!{9 9 9 6 4 5 6 7 8 9 8 7 6 5 3 4 9 9 9 8 9 7 6 9 8 9 9 8 9 8 7 7 8 9 4 5 9 8 9 8 7 2 1 0 1 2 4 7 8 9 9 8 7 8 9 8 9 6 6 9 8 6 7 8 9 9 9 9 7 6 2 1 0 1 2 3 4 5 6 9 9 9 8 9 8 7 8 9 9 9 7 6 5 9 8 7 5 4 3 2},
            mkRow!{8 9 6 5 3 4 7 8 9 9 9 8 9 6 7 6 7 9 9 9 9 8 9 9 9 2 3 9 8 9 8 8 9 1 2 9 8 7 6 5 4 3 2 1 5 3 4 5 9 9 8 7 6 7 8 9 8 7 9 6 6 5 3 9 9 8 7 8 9 4 3 2 3 2 3 4 5 6 7 8 9 6 7 8 9 6 7 9 8 9 8 5 4 5 9 8 7 6 4 4},
            mkRow!{7 9 9 6 7 5 6 7 8 9 4 9 9 8 9 7 9 8 9 9 8 9 9 8 7 5 4 6 7 8 9 9 2 0 9 8 9 8 7 8 5 4 3 2 6 4 5 6 7 8 9 6 5 6 9 9 9 9 8 5 4 3 2 4 4 5 6 7 8 9 5 3 4 3 4 5 6 7 8 9 6 5 3 9 4 5 9 8 7 6 5 4 3 4 6 9 8 7 5 6},
            mkRow!{6 7 8 9 7 6 7 8 9 2 3 4 9 9 8 9 8 7 8 9 7 5 5 9 9 7 6 7 8 9 6 4 3 9 8 7 6 9 8 9 7 6 5 3 4 5 6 8 8 9 2 3 4 7 8 9 9 9 7 8 4 2 1 3 3 4 5 6 7 8 9 4 6 4 5 6 7 9 9 8 7 6 2 1 3 4 5 9 8 7 6 7 2 3 4 5 9 8 8 7},
            mkRow!{5 6 7 8 9 7 8 9 2 1 2 9 8 9 7 6 5 6 7 8 9 4 4 0 9 8 9 8 9 8 9 5 9 8 7 6 5 6 9 9 9 8 7 6 5 6 7 8 9 2 1 2 5 9 9 9 8 7 6 5 2 1 0 1 2 3 4 7 8 9 9 8 7 5 6 7 8 9 9 7 6 4 3 0 2 3 9 8 7 8 5 4 1 2 3 4 5 9 9 8},
            mkRow!{4 4 4 9 8 9 9 9 1 0 9 8 7 6 6 5 4 5 6 7 9 3 2 1 9 9 7 9 8 7 8 9 9 7 9 5 4 5 6 9 9 9 8 9 8 7 8 9 7 4 2 3 6 8 9 9 9 8 5 4 3 2 1 2 3 4 5 8 9 6 5 9 9 8 7 8 9 9 9 5 4 3 2 1 3 9 8 7 6 5 4 3 2 4 4 5 6 7 8 9},
            mkRow!{3 2 3 9 7 8 9 8 9 9 8 7 6 5 3 4 3 6 7 8 9 4 9 9 8 7 6 4 7 6 9 8 8 6 7 4 3 4 6 7 8 9 9 4 9 8 9 8 6 5 6 4 7 9 9 9 8 7 6 5 6 4 2 3 4 5 6 7 8 9 4 4 5 9 8 9 3 9 8 7 6 5 4 5 4 9 9 9 7 8 5 4 5 7 5 6 7 8 9 9},
            mkRow!{4 5 9 8 5 6 6 7 9 8 9 5 4 3 2 1 2 5 6 7 8 9 8 6 9 8 4 3 4 5 9 7 6 5 4 3 2 3 5 6 9 7 4 3 2 9 2 9 9 8 7 5 6 7 8 9 9 8 7 6 7 6 3 4 5 6 7 9 9 5 2 3 4 7 9 9 2 0 9 8 7 6 5 7 9 8 7 9 8 7 6 5 6 9 6 7 8 9 9 8},
            mkRow!{5 9 8 6 4 5 4 6 7 7 8 9 4 3 1 0 3 4 5 8 9 8 9 5 4 9 9 4 5 6 9 8 7 4 3 2 1 6 7 8 9 6 5 4 1 0 1 2 3 9 8 6 7 8 9 4 6 9 8 7 8 7 4 7 6 7 9 6 5 4 3 4 5 6 7 8 9 9 9 9 9 7 6 7 9 8 6 5 9 8 7 6 8 9 7 8 9 8 7 7},
            mkRow!{6 7 9 7 3 2 3 4 7 6 9 8 9 3 2 1 2 3 8 9 9 7 8 9 9 9 8 9 6 8 9 9 8 7 4 3 4 5 8 9 6 5 4 3 2 1 2 4 4 5 9 7 8 9 3 3 4 2 9 8 9 6 5 6 7 8 9 7 6 5 4 6 7 8 8 9 9 8 9 9 9 8 9 8 9 9 9 6 8 9 8 9 9 9 8 9 8 7 5 6},
            mkRow!{8 9 8 3 2 1 2 3 4 5 6 7 8 9 3 2 3 4 6 9 8 6 9 9 7 8 7 9 9 9 9 8 7 6 5 6 5 6 9 8 7 6 5 4 3 4 4 5 6 7 9 8 9 3 2 1 0 1 2 9 8 9 7 7 8 9 9 8 7 6 9 8 8 9 9 9 8 7 8 9 9 9 9 9 3 9 8 7 9 1 9 9 9 9 9 5 9 8 4 3},
            mkRow!{9 9 7 5 9 0 3 5 5 9 7 8 9 9 9 4 5 6 9 9 7 5 9 8 6 5 6 7 8 9 6 9 8 7 6 7 7 8 9 9 8 7 6 5 4 5 5 8 7 8 9 9 6 5 3 2 1 4 3 9 7 9 8 9 9 8 9 9 8 9 8 9 9 0 1 9 7 6 7 8 9 8 9 1 2 3 9 8 9 0 9 8 9 8 9 4 3 2 3 2},
            mkRow!{4 5 9 9 8 9 9 6 7 8 9 9 9 9 8 9 7 9 8 7 5 4 5 7 8 4 5 6 9 5 5 6 9 8 9 8 8 9 2 3 9 9 7 6 5 8 6 9 8 9 9 8 7 9 4 3 2 9 9 7 6 5 9 5 6 7 8 9 9 8 7 8 9 1 2 9 6 5 4 5 6 7 8 9 3 4 5 9 9 9 8 7 6 7 8 9 2 1 0 1},
            mkRow!{5 9 8 7 7 8 8 9 8 9 9 9 9 8 7 8 9 9 8 5 4 3 4 6 2 3 4 9 5 4 3 4 5 9 8 9 9 0 1 9 8 9 8 7 7 9 7 8 9 9 8 9 9 8 9 4 9 8 7 6 5 4 3 4 5 6 9 9 8 7 6 7 9 3 9 8 7 4 3 9 7 9 9 9 9 7 6 7 8 9 7 6 5 5 5 8 9 3 1 2},
            mkRow!{9 8 7 6 5 6 7 8 9 9 8 9 8 7 6 7 9 8 7 6 6 2 1 0 1 2 6 8 9 4 2 3 9 8 7 8 9 2 9 8 6 2 9 9 8 9 8 9 9 8 7 7 5 6 8 9 9 8 7 5 5 3 2 3 4 9 8 6 5 6 5 6 8 9 9 9 9 9 9 8 9 0 1 9 8 9 7 8 9 9 8 5 4 3 4 7 8 9 2 3},
            mkRow!{9 8 9 5 4 5 6 7 9 8 7 8 6 5 4 5 6 9 8 7 7 8 2 3 2 3 5 8 9 3 1 9 8 7 6 9 9 4 9 7 5 4 5 6 9 9 9 9 9 8 6 5 4 5 6 9 7 7 6 4 4 2 1 2 9 8 7 5 4 5 4 5 7 8 8 9 8 7 8 7 8 9 9 8 7 8 9 9 9 8 7 6 3 2 7 6 7 8 9 4},
            mkRow!{8 7 5 4 2 3 4 5 9 6 5 6 5 4 3 4 7 9 7 6 5 4 3 4 5 6 6 7 8 9 9 8 7 6 5 6 8 9 8 7 6 7 6 7 8 9 9 8 8 7 8 4 3 4 5 9 6 5 4 3 2 1 0 9 8 7 6 5 3 2 3 4 5 6 7 8 9 6 5 6 7 8 9 7 6 7 8 9 8 7 6 5 2 1 2 5 6 7 8 9},
            mkRow!{9 7 6 4 3 4 5 7 8 9 4 3 2 1 2 3 4 5 9 7 6 5 4 5 6 7 7 9 9 9 9 8 7 5 4 5 7 8 9 8 7 8 9 9 9 9 8 7 9 6 5 1 2 9 9 8 7 9 6 4 5 2 1 3 9 8 9 9 2 1 2 3 7 8 9 9 2 3 4 5 6 9 8 9 5 7 8 9 7 6 5 4 3 0 1 3 8 8 9 4},
            mkRow!{9 8 6 5 7 5 6 8 9 6 5 4 3 0 1 6 5 6 9 8 7 6 7 6 7 8 9 3 4 9 8 7 6 6 3 4 6 7 8 9 8 9 4 3 4 5 9 6 5 4 3 2 9 8 9 9 9 8 7 5 9 3 2 4 5 9 9 8 2 0 1 2 6 9 2 1 0 1 2 3 7 9 7 8 4 6 9 8 6 5 4 3 2 1 2 3 9 9 9 5},
            mkRow!{2 9 7 6 7 8 7 9 9 8 6 5 2 1 2 7 6 7 8 9 8 8 8 7 8 9 3 2 3 9 8 7 5 4 2 3 7 8 9 9 9 5 3 2 3 9 8 7 6 5 4 4 9 7 9 3 2 9 8 7 8 4 3 5 7 9 8 7 6 2 2 4 5 9 3 3 1 2 6 4 9 8 6 5 3 7 8 9 7 8 5 4 3 4 3 4 6 7 8 9},
            mkRow!{1 2 9 7 8 9 8 9 9 9 7 6 3 3 4 6 7 8 9 3 9 9 9 8 9 9 0 1 2 4 9 8 3 2 1 2 6 9 8 9 7 6 2 1 2 3 9 8 7 9 5 9 8 6 8 9 1 0 9 8 7 6 5 6 7 8 9 9 5 4 3 4 9 8 9 4 5 9 8 9 8 9 7 6 2 2 3 9 8 9 6 5 6 5 6 5 6 9 9 4},
            mkRow!{0 1 2 9 9 6 9 8 9 9 8 7 4 5 5 7 8 9 5 2 1 2 9 9 9 8 9 9 4 9 8 7 4 4 3 3 5 6 7 8 9 2 1 0 1 4 5 9 9 8 9 8 9 5 7 8 9 1 2 9 8 7 6 7 9 9 8 7 6 6 4 9 8 7 8 9 6 7 9 8 7 6 5 1 0 1 4 7 9 9 7 6 7 7 8 7 8 9 2 3},
            mkRow!{1 9 9 8 6 5 4 6 9 8 9 8 6 6 6 7 8 9 4 3 2 9 8 9 6 7 8 8 9 8 7 6 5 6 5 6 7 8 9 9 4 3 2 3 4 5 6 9 8 7 6 7 5 4 5 7 9 2 9 9 9 8 7 8 9 8 9 8 7 8 9 8 7 6 9 8 9 8 9 9 8 9 3 2 4 2 3 5 6 9 8 9 8 8 9 8 9 2 1 2},
            mkRow!{9 8 8 7 6 4 3 5 6 7 8 9 8 9 8 9 9 6 5 5 9 8 7 6 5 6 7 7 5 9 8 7 6 7 7 7 9 9 8 8 9 4 3 4 5 6 7 9 6 6 5 3 4 3 8 6 8 9 8 7 9 9 8 9 6 7 8 9 8 9 8 7 6 5 6 7 8 9 1 0 9 8 9 4 5 3 5 6 7 8 9 7 9 9 9 9 9 9 0 9},
            mkRow!{8 7 9 8 4 3 2 3 4 5 9 9 9 2 9 8 9 7 8 9 8 7 6 5 4 3 5 6 4 5 9 8 7 8 9 8 9 7 6 7 8 9 4 6 7 7 9 8 5 4 3 2 1 2 3 5 7 8 9 6 8 9 9 4 5 6 8 9 9 8 7 9 7 4 5 6 9 9 2 9 8 7 8 9 7 4 5 9 8 9 5 6 7 8 9 9 9 8 9 8},
            mkRow!{9 6 5 4 3 2 1 4 5 6 8 9 0 1 9 7 8 9 9 0 9 7 5 6 3 2 3 4 3 4 6 9 8 9 9 9 5 4 5 6 7 9 5 9 8 9 8 7 7 3 2 1 0 1 7 6 7 9 3 5 6 7 9 5 6 7 9 9 9 9 6 5 6 3 5 6 7 8 9 8 7 6 7 8 9 5 6 7 9 5 4 5 6 7 8 9 8 7 6 7},
            mkRow!{8 7 6 5 4 3 0 1 2 7 7 8 9 9 8 6 7 9 2 1 9 8 4 3 2 1 0 1 2 4 5 6 9 9 8 7 6 3 4 5 8 9 9 8 9 8 7 6 5 4 3 2 1 2 8 7 9 3 2 4 5 7 8 9 9 8 9 9 8 7 9 4 3 2 3 7 9 9 8 7 6 5 6 9 8 9 7 8 9 9 3 4 9 8 9 8 7 5 4 5},
            mkRow!{9 8 7 6 5 4 1 2 3 5 6 9 9 9 7 5 6 7 9 2 3 9 9 8 3 4 3 2 3 6 7 8 9 8 7 6 5 4 5 6 9 8 7 6 5 9 8 7 6 5 4 3 2 3 4 8 9 2 1 2 5 9 8 9 9 9 9 9 8 6 5 4 2 1 2 6 7 8 9 6 5 4 5 6 7 8 9 9 6 8 9 9 9 9 8 9 6 5 3 6},
            mkRow!{2 9 8 7 6 7 8 3 4 6 9 8 8 8 5 4 5 6 8 9 4 9 8 7 6 5 4 3 4 7 8 9 4 9 8 7 6 5 9 7 9 9 8 7 6 7 9 8 7 6 5 4 3 5 6 9 4 3 0 1 4 5 7 9 9 8 9 8 7 6 5 3 1 0 4 5 8 9 7 5 4 3 4 5 6 9 1 2 5 6 7 8 9 8 7 8 9 7 4 7},
            mkRow!{1 2 9 8 8 9 8 7 6 9 8 7 6 7 4 3 4 5 7 8 9 9 9 8 7 6 5 4 5 7 9 1 3 4 9 8 7 6 7 8 9 3 9 8 9 8 9 9 8 7 6 5 4 6 7 8 9 7 1 2 3 4 6 8 9 6 7 9 8 7 4 3 2 2 3 6 9 8 6 4 2 1 2 5 7 8 9 3 4 7 8 9 6 7 6 7 8 9 5 8},
            mkRow!{0 1 9 9 9 5 9 8 9 9 7 6 5 3 2 2 3 4 6 7 8 9 7 9 9 8 6 7 8 9 8 9 9 5 9 9 9 7 8 9 0 2 3 9 9 9 9 9 9 8 8 7 6 7 8 9 9 5 4 3 4 6 7 9 6 5 6 7 9 8 5 4 3 9 8 7 8 9 5 4 3 2 3 4 8 9 9 4 5 8 9 6 5 6 5 6 9 8 9 9},
            mkRow!{9 9 8 6 5 4 5 9 9 8 7 5 4 3 0 1 2 5 8 8 9 7 6 5 8 9 7 8 9 5 6 7 8 9 8 7 9 8 9 5 2 3 5 6 9 9 9 8 8 9 9 8 7 8 9 9 8 7 5 5 6 9 8 9 7 6 9 8 9 9 8 6 4 5 9 8 9 7 6 5 5 3 6 5 6 7 8 9 6 9 6 5 4 5 4 5 6 7 8 9},
            mkRow!{8 7 6 5 4 3 4 4 5 9 6 4 3 2 1 3 4 7 8 9 7 6 5 4 6 9 8 9 3 4 5 6 7 8 9 6 7 9 9 4 3 4 5 7 8 9 7 6 7 8 9 9 8 9 6 8 9 8 6 7 8 9 9 9 8 9 8 9 8 9 7 6 5 8 9 9 9 8 7 6 9 8 7 6 7 8 9 8 7 8 9 4 3 4 3 4 5 8 9 8},
            mkRow!{9 8 7 6 3 2 1 3 9 8 9 5 4 3 4 4 5 6 9 9 8 7 6 9 7 9 9 1 2 3 6 7 8 9 4 5 6 9 8 7 6 5 6 7 9 9 8 5 6 7 8 9 9 5 4 7 7 9 7 8 9 7 6 8 9 9 7 6 7 9 8 7 6 7 8 9 2 9 8 7 8 9 8 7 8 9 9 9 8 9 9 3 2 1 2 3 4 9 8 7},
            mkRow!{2 9 9 5 4 5 6 9 8 7 8 9 6 4 7 6 7 7 8 9 9 8 9 8 9 7 5 4 3 4 7 8 9 4 3 1 2 3 9 8 7 6 8 9 8 7 5 4 5 8 7 8 9 2 3 5 6 9 8 9 6 6 5 9 8 7 6 5 4 5 9 9 7 8 9 4 3 4 9 9 9 6 9 8 9 9 9 9 9 9 8 9 3 9 3 5 9 8 7 6},
            mkRow!{1 9 8 7 5 6 9 8 7 6 7 8 9 5 6 9 8 9 9 2 4 9 8 7 8 9 6 5 4 5 6 9 6 5 3 2 3 4 6 9 8 7 9 8 7 6 5 3 4 5 6 9 0 1 2 3 4 5 9 6 4 3 4 5 9 8 7 4 3 2 1 9 8 9 8 9 4 9 8 7 6 5 6 9 9 9 8 9 9 8 7 8 9 8 9 6 7 9 6 5},
            mkRow!{0 1 9 8 6 7 8 9 7 5 6 7 9 9 7 8 9 5 4 3 9 9 8 6 7 8 9 6 5 6 7 8 9 9 4 3 4 5 9 9 9 8 9 9 8 7 5 4 5 6 7 9 9 2 3 4 6 9 8 7 6 2 3 9 7 6 4 3 2 1 0 1 9 6 7 8 9 8 7 6 5 4 5 6 9 9 7 9 5 4 6 7 5 7 7 9 9 6 5 4},
            mkRow!{3 2 3 9 9 8 9 7 6 4 5 6 7 8 9 9 9 6 7 9 7 6 5 5 6 8 9 9 7 7 8 9 7 8 9 4 9 9 7 8 9 9 3 0 9 8 6 5 6 8 9 7 8 9 6 5 9 8 7 5 4 3 4 9 9 8 9 4 3 3 1 2 4 5 9 9 9 9 8 7 6 5 7 9 8 7 6 7 6 3 2 3 4 5 6 7 8 9 4 3},
            mkRow!{4 5 9 9 8 9 9 8 6 5 6 9 8 9 9 9 8 7 9 8 6 5 4 4 5 7 9 8 9 8 9 7 6 7 8 9 8 7 6 7 8 9 2 1 2 9 7 8 9 9 7 6 7 8 9 6 7 9 8 7 5 6 9 8 9 9 8 7 6 5 3 5 5 6 7 8 9 9 9 8 7 9 9 8 7 6 5 4 3 4 1 2 9 6 7 9 9 4 3 2},
            mkRow!{5 9 8 8 7 6 7 9 7 8 9 6 9 7 8 9 9 9 8 7 6 5 3 3 4 5 6 7 8 9 3 4 5 8 9 8 9 6 5 6 7 8 9 3 9 9 8 9 9 7 6 5 6 7 8 9 8 9 9 9 6 9 8 7 6 5 9 8 8 9 4 6 7 7 8 9 8 9 9 9 9 8 9 9 8 7 9 3 2 1 0 9 8 9 9 8 9 9 4 9},
            mkRow!{9 8 7 6 5 4 3 9 8 9 6 5 6 6 7 8 9 8 7 6 5 4 1 2 5 6 7 8 9 4 2 3 4 9 8 7 6 5 4 5 6 7 9 9 8 9 9 9 8 9 7 6 7 8 9 6 9 9 9 9 9 8 7 6 5 4 6 9 9 6 5 7 8 9 9 6 7 9 9 9 8 7 9 9 9 9 8 9 3 4 9 8 7 8 5 6 7 8 9 8},
            mkRow!{6 5 6 5 4 3 2 3 9 1 2 3 4 5 6 9 9 9 8 5 4 3 2 7 6 8 8 9 5 4 3 4 5 6 9 8 5 4 3 1 0 9 8 8 7 6 7 6 7 8 9 7 8 9 9 5 9 8 7 9 9 9 8 8 6 3 2 9 8 7 6 8 9 5 4 5 6 7 8 9 5 6 8 9 9 8 7 8 9 9 8 7 6 7 4 7 9 9 6 7},
            mkRow!{5 4 3 2 1 0 1 2 3 9 3 4 5 8 9 9 9 9 8 6 6 7 9 8 9 9 9 7 6 7 5 6 6 9 8 7 6 5 4 5 9 8 7 8 5 4 3 5 6 7 8 9 9 6 8 9 8 7 6 8 9 9 9 9 9 9 1 9 9 8 7 9 5 4 3 5 7 8 9 4 4 5 6 9 8 7 6 7 9 9 9 8 4 3 2 3 4 5 5 6},
            mkRow!{8 5 5 6 2 1 4 3 9 8 9 9 6 7 9 9 8 9 8 7 7 8 9 9 9 9 9 8 7 8 7 8 9 9 9 8 7 6 5 9 8 7 6 5 4 3 2 4 8 8 9 5 4 5 9 8 7 6 5 7 8 9 9 9 8 8 9 8 9 9 8 9 9 5 5 6 8 9 5 3 3 4 9 8 9 6 5 6 7 8 9 6 5 4 0 1 2 3 4 5},
            mkRow!{7 6 7 9 3 4 5 9 8 7 8 8 9 9 8 8 7 5 9 9 8 9 9 7 8 8 9 9 8 9 8 9 9 9 9 9 9 8 7 8 9 9 7 6 5 2 1 0 1 9 7 9 3 9 8 7 6 5 4 5 6 7 9 8 7 6 6 7 8 9 9 9 8 7 8 7 9 5 4 1 2 9 8 7 6 5 4 5 6 9 9 9 6 4 1 4 4 5 6 9},
            mkRow!{8 7 9 8 9 5 6 9 8 6 6 7 9 8 7 6 5 4 3 5 9 8 7 6 7 6 9 8 9 9 9 9 9 8 8 8 9 9 8 9 0 1 9 6 5 4 2 1 4 5 6 8 9 9 8 6 5 4 3 3 4 9 8 9 6 5 5 6 7 9 2 1 9 8 9 9 5 4 3 0 1 2 9 6 5 4 3 4 7 5 9 8 9 3 2 3 4 5 7 8},
            mkRow!{9 9 8 7 8 9 9 7 6 5 4 9 8 7 6 5 4 3 2 4 8 9 6 5 6 5 9 7 8 9 9 9 8 7 6 7 9 9 9 3 1 9 8 7 8 4 3 2 3 6 7 9 8 7 6 5 4 3 1 2 9 8 7 6 5 4 4 5 9 8 9 2 3 9 6 5 4 3 2 1 2 9 8 7 7 5 2 1 2 3 8 7 8 9 3 4 5 6 7 9},
            mkRow!{9 7 6 5 9 7 8 9 6 4 3 4 9 8 7 6 4 2 1 2 7 8 9 4 5 4 5 6 7 9 9 8 7 6 5 6 8 9 4 3 2 3 9 8 7 5 6 3 4 7 8 9 9 8 8 6 5 1 0 1 2 9 8 7 6 2 3 4 6 7 8 9 9 9 8 7 6 5 3 2 3 5 9 9 9 4 3 0 1 4 5 6 7 8 9 6 8 7 8 9},
            mkRow!{8 9 6 4 5 6 8 9 9 1 2 5 6 9 9 5 4 3 2 3 5 9 9 3 2 3 4 5 6 7 9 9 8 7 4 5 9 8 5 5 9 4 5 9 8 7 8 4 5 8 9 9 9 9 7 7 6 2 1 2 9 8 7 5 4 3 4 5 6 8 9 7 8 9 9 8 8 6 4 3 5 6 9 8 6 5 4 3 2 3 6 8 9 9 8 7 9 8 9 7},
            mkRow!{7 8 4 3 4 5 6 9 8 9 3 4 6 9 8 6 5 4 8 7 6 7 8 9 1 2 3 4 8 7 8 9 9 6 5 9 8 7 6 9 8 9 6 8 9 8 9 5 6 9 9 9 8 7 6 5 4 3 4 3 4 9 8 6 6 4 5 7 7 9 7 6 7 8 7 9 9 8 5 4 6 7 8 9 7 8 5 6 5 4 5 9 9 9 9 8 9 9 8 6},
            mkRow!{6 5 3 2 3 4 5 6 7 8 9 6 7 9 8 7 6 6 9 8 7 8 9 1 0 1 2 3 5 6 7 8 9 7 6 7 9 8 9 8 7 8 9 9 9 9 7 6 7 8 9 9 9 9 7 6 5 5 6 4 5 6 9 8 7 5 6 8 9 8 6 5 8 7 6 9 8 7 6 7 7 8 9 9 8 9 8 7 6 5 6 7 8 9 9 9 7 6 7 5},
            mkRow!{4 3 2 1 2 3 4 7 8 9 8 7 9 2 9 9 8 7 9 9 8 9 8 9 2 3 4 4 5 7 9 9 9 9 9 9 9 9 8 7 6 8 9 9 8 9 9 9 8 9 9 9 8 9 8 9 6 6 7 5 8 7 8 9 8 6 7 9 8 7 8 4 5 4 5 9 9 8 9 8 9 9 8 9 9 1 9 9 7 8 7 8 9 9 8 9 6 5 4 3},
            mkRow!{3 2 1 0 3 4 5 8 9 7 9 8 9 1 2 9 9 9 7 9 9 8 7 8 9 4 5 6 6 8 9 9 8 8 8 7 8 9 9 6 5 6 9 8 7 9 3 2 9 9 9 8 7 8 9 8 9 7 9 6 9 8 9 5 9 7 8 9 7 6 4 3 2 3 4 8 9 9 4 9 6 6 7 9 9 0 1 9 8 9 9 9 7 8 7 9 5 4 3 2},
            mkRow!{4 3 2 1 2 9 6 8 9 6 5 9 1 0 9 7 8 5 6 8 9 7 6 7 8 9 6 7 7 8 9 8 7 6 5 6 9 8 9 5 4 5 4 7 6 8 9 1 0 9 8 7 6 5 6 7 9 9 8 9 2 9 2 4 9 8 9 9 8 6 5 2 1 4 6 7 8 9 3 2 4 5 6 7 8 9 2 9 9 5 6 9 6 5 6 7 9 3 2 1},
            mkRow!{6 5 4 2 3 8 9 9 8 9 4 3 2 9 7 6 5 4 5 7 3 4 5 7 7 8 9 9 8 9 3 9 8 5 4 9 8 7 6 4 3 4 3 4 5 6 8 9 1 2 9 8 5 4 5 6 9 8 7 9 1 0 1 2 5 9 7 8 9 5 4 3 0 3 4 9 9 6 4 3 4 6 7 9 9 9 9 8 7 6 7 8 9 4 5 7 8 9 1 0},
            mkRow!{9 5 4 3 5 7 8 9 7 8 9 4 9 8 6 5 4 3 4 8 2 3 4 5 6 7 8 9 9 3 2 9 8 7 6 7 9 8 6 3 2 1 2 3 4 7 8 9 2 9 9 9 9 3 5 7 9 7 6 8 9 1 3 3 4 5 6 7 8 9 4 2 1 2 5 8 9 7 5 9 5 9 8 9 9 9 8 9 8 7 8 9 4 3 4 6 7 8 9 1},
            mkRow!{8 7 5 4 6 9 9 5 6 7 8 9 9 8 7 6 3 2 1 0 1 2 5 6 7 8 9 3 2 0 1 2 9 8 7 8 9 7 5 6 1 0 1 3 5 6 7 8 9 8 9 9 8 9 6 9 8 6 5 7 8 9 9 4 5 6 7 8 9 9 5 3 2 3 6 7 9 7 6 7 7 8 9 8 7 9 7 5 9 9 9 8 9 2 3 4 8 9 8 9},
            mkRow!{8 7 6 7 9 7 6 4 5 6 9 1 2 9 8 6 5 4 2 1 5 3 4 5 7 9 5 4 3 1 4 3 4 9 8 9 7 5 4 3 2 1 2 4 6 7 8 9 6 7 9 8 7 8 9 8 7 6 4 5 9 7 8 9 6 8 8 9 9 8 9 4 3 4 8 9 9 9 7 8 8 9 6 7 6 8 9 9 9 7 6 7 9 0 1 9 9 8 7 8},
            mkRow!{9 8 9 8 9 9 5 3 4 5 8 9 3 4 9 8 5 4 3 4 6 4 7 6 8 9 6 5 3 2 6 4 5 6 9 9 9 6 5 6 3 5 3 4 7 8 9 4 5 9 8 7 6 7 8 9 5 4 3 4 5 6 7 8 9 9 9 9 8 7 8 9 4 5 9 8 7 8 9 9 9 6 5 4 5 9 9 8 7 6 5 8 9 1 9 8 9 7 6 7},
            mkRow!{3 9 1 9 8 9 4 2 3 6 7 8 9 9 8 7 6 5 5 5 7 5 8 8 9 8 7 6 5 3 9 5 6 7 8 9 8 7 6 7 7 6 4 5 8 9 4 3 9 8 7 6 5 6 9 8 9 5 2 3 4 5 8 9 9 9 8 7 7 6 7 8 9 6 7 9 5 6 5 8 9 9 8 7 6 9 8 9 8 7 6 7 9 9 8 7 5 6 5 6},
            mkRow!{2 1 0 9 7 8 9 1 2 9 8 9 4 2 9 8 9 6 7 9 8 9 9 9 9 9 8 9 7 9 8 9 7 8 9 9 9 9 7 8 8 7 9 7 8 9 5 2 0 9 9 5 4 3 6 7 8 9 1 2 5 6 7 8 9 9 9 6 5 4 5 6 8 9 8 9 4 3 4 7 8 9 9 8 7 9 7 6 9 8 7 9 8 7 6 8 4 5 4 5},
            mkRow!{3 9 9 8 6 7 9 2 9 8 9 3 2 1 2 9 8 7 8 9 9 6 8 9 9 9 9 8 9 8 7 8 9 9 9 9 9 9 8 9 9 8 9 9 9 7 6 2 1 9 8 6 5 4 5 6 7 8 9 7 6 7 8 9 8 9 8 7 6 8 6 7 9 9 9 7 6 4 5 6 7 8 9 9 8 9 6 5 7 9 9 8 9 6 5 4 3 3 3 4},
            mkRow!{9 8 7 7 5 6 8 9 9 7 8 9 3 2 4 5 9 8 9 3 4 5 6 7 8 9 9 7 6 7 6 7 9 8 9 8 9 9 9 3 4 9 8 7 6 5 4 3 9 9 9 7 8 7 6 7 8 9 4 9 9 8 9 5 6 9 9 8 7 9 7 8 9 8 9 8 7 9 6 7 8 9 7 6 9 8 7 6 8 9 8 7 8 9 9 3 2 1 2 3},
            mkRow!{9 9 6 5 4 7 9 8 7 6 7 8 9 3 4 6 7 9 0 1 2 3 7 8 9 9 7 6 5 8 5 9 8 7 6 7 8 8 9 1 9 8 9 9 7 6 9 9 8 9 9 8 9 8 7 8 9 6 3 2 1 9 3 4 9 8 7 9 8 9 8 9 6 7 8 9 9 8 9 9 9 9 9 5 3 9 8 7 9 8 7 6 6 7 8 9 9 0 1 4},
            mkRow!{7 6 5 4 3 9 8 7 6 5 6 9 5 4 5 6 9 8 9 5 4 5 6 9 9 9 8 5 4 3 4 9 7 6 5 6 7 7 8 9 8 7 8 9 8 9 8 9 7 8 4 9 7 9 8 9 6 5 4 9 0 1 2 9 9 9 6 7 9 3 9 3 5 6 7 9 8 7 9 8 9 9 8 9 2 0 9 8 9 9 8 5 4 5 6 7 8 9 2 3},
            mkRow!{8 7 4 3 2 1 9 7 5 4 9 8 9 5 7 9 8 7 9 9 5 6 7 8 9 4 9 6 9 2 9 8 9 5 4 3 4 6 7 8 9 6 6 7 9 9 7 6 5 4 3 4 6 5 9 8 9 6 9 8 9 2 9 8 7 6 5 4 3 2 1 2 3 5 6 9 7 6 7 6 7 6 7 8 9 1 5 9 9 9 5 5 3 4 5 6 7 8 9 5},
            mkRow!{9 8 5 4 5 9 8 8 5 3 8 7 8 9 9 8 7 6 7 8 9 7 9 9 2 3 4 9 8 9 9 7 8 9 3 2 3 7 8 9 6 5 5 6 7 9 8 7 6 2 1 0 3 4 6 7 8 9 9 7 9 3 4 9 8 9 6 5 4 1 0 1 2 9 9 8 7 5 6 5 4 5 6 7 9 2 3 4 9 7 4 3 2 3 5 6 7 8 9 6},
            mkRow!{8 7 6 5 9 9 7 5 4 2 3 6 7 8 8 9 6 5 8 9 8 9 3 2 1 0 9 8 7 8 5 6 7 8 9 3 4 5 7 8 9 3 4 5 6 7 9 8 5 4 2 1 2 3 5 8 9 9 7 6 7 9 5 6 9 8 9 4 3 2 1 2 9 8 7 6 5 4 3 2 3 6 7 9 9 3 9 9 8 6 3 2 1 2 4 5 5 6 8 9},
            mkRow!{9 8 7 9 8 7 6 5 2 1 3 4 5 6 7 8 9 4 7 6 7 8 9 3 2 2 9 7 6 5 4 5 6 9 9 6 5 9 8 9 3 1 2 3 9 9 8 7 6 5 3 4 5 6 9 9 6 7 6 5 6 7 9 7 9 7 7 9 9 3 2 3 4 9 9 7 4 3 2 1 4 5 6 7 8 9 8 7 6 5 4 2 0 1 2 3 4 6 7 8},
            mkRow!{5 9 8 9 8 5 4 3 1 0 4 5 6 8 9 9 4 3 4 5 6 7 8 9 4 5 9 8 7 6 5 7 8 9 8 7 6 7 9 2 1 0 5 7 8 9 9 9 7 9 4 5 6 7 8 9 5 4 5 4 6 9 8 9 8 6 5 7 8 9 9 4 5 6 9 6 5 4 9 2 3 6 7 9 9 3 9 8 7 6 5 6 2 9 3 4 6 8 8 9},
            mkRow!{4 1 9 8 7 6 4 3 2 1 6 9 7 9 7 4 3 2 5 7 7 8 9 9 8 6 7 9 9 8 6 9 9 3 9 8 9 8 9 3 2 3 4 5 7 8 9 9 9 8 9 6 9 8 9 5 4 3 5 3 5 9 7 6 9 8 4 5 6 7 8 9 6 9 8 9 7 9 8 9 4 7 8 9 3 2 1 9 8 7 6 5 4 8 9 5 6 7 8 9},
            mkRow!{3 0 1 9 8 7 8 9 3 2 7 8 9 7 6 5 5 3 4 5 6 8 9 9 8 7 9 9 9 8 7 8 9 4 5 9 9 9 5 4 3 4 5 6 9 9 9 9 8 7 8 9 8 9 5 4 3 2 1 2 3 4 9 5 4 3 2 3 7 8 9 9 9 8 7 8 9 8 7 8 9 9 9 4 2 1 0 1 9 8 7 6 5 6 7 8 7 8 9 5},
            mkRow!{2 1 2 3 9 8 9 5 4 9 8 9 8 9 8 7 6 8 9 6 7 9 4 3 9 9 8 9 9 9 8 9 6 5 6 9 8 7 6 5 4 5 6 7 8 9 9 8 7 6 7 8 7 8 9 5 9 4 3 4 5 9 8 9 5 4 3 4 8 9 9 9 8 7 6 9 5 5 6 6 7 8 9 4 3 2 1 9 8 9 9 9 8 7 9 9 8 9 6 4},
            mkRow!{4 2 5 7 8 9 8 6 7 8 9 7 7 6 9 8 7 8 9 9 8 9 5 4 9 8 7 8 9 9 9 8 7 8 8 9 9 8 7 6 9 7 7 8 9 9 9 9 6 5 4 5 6 7 8 9 7 5 4 5 9 8 7 8 9 5 4 5 9 9 9 8 7 6 5 4 4 3 4 5 6 9 6 5 4 5 9 8 7 6 9 9 9 9 4 5 9 9 9 5},
            mkRow!{4 3 4 5 9 9 8 7 8 9 7 6 4 5 6 9 8 9 7 9 9 9 6 9 9 7 6 7 8 9 9 9 8 9 9 6 7 9 8 7 9 8 8 9 4 9 8 7 6 4 3 4 9 8 9 9 8 6 9 6 9 7 6 7 8 9 5 6 7 8 9 9 8 7 6 3 1 2 3 6 7 8 9 6 7 9 8 7 6 5 8 9 5 4 3 5 6 7 8 9},
            mkRow!{5 6 5 6 7 9 9 8 9 3 2 1 2 6 9 8 9 7 5 7 7 8 9 8 7 6 5 6 7 8 7 8 9 6 4 5 9 8 9 8 9 9 9 2 3 5 9 8 6 5 4 5 7 9 5 4 9 9 8 9 9 6 5 6 9 8 7 7 8 9 3 4 9 8 7 4 5 6 4 9 8 9 9 8 9 9 9 6 5 4 7 8 9 3 2 3 5 8 8 9},
            mkRow!{6 7 6 7 8 9 9 9 6 5 4 3 5 9 8 7 6 5 4 5 6 9 8 7 6 5 4 4 6 5 6 7 8 9 5 9 8 7 6 9 2 1 0 1 2 4 5 9 8 6 7 8 8 9 4 2 9 8 7 9 8 7 4 6 8 9 9 8 9 3 2 9 8 7 6 5 6 7 8 9 9 1 0 9 5 9 8 7 6 5 6 7 9 2 0 2 3 9 7 9},
            mkRow!{9 8 9 8 9 9 9 8 7 6 7 8 6 9 8 6 5 4 3 2 1 0 9 8 7 6 3 2 3 4 7 8 9 9 9 8 7 6 5 4 3 2 1 2 3 5 6 9 8 7 8 9 9 6 5 9 8 7 6 5 3 2 3 4 9 0 1 9 1 0 1 2 9 8 8 6 7 9 9 5 4 3 2 3 4 5 9 9 7 6 7 8 9 3 4 3 4 5 6 8}
        }
    }
}

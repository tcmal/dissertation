use aoc_2021_09::HeightMap;
use criterion::{criterion_group, criterion_main, BatchSize, BenchmarkId, Criterion};
use rand::{rngs::StdRng, SeedableRng};

fn run_benches(c: &mut Criterion) {
    let mut rng = StdRng::seed_from_u64(42);
    for size in [100].iter() {
        c.bench_with_input(
            BenchmarkId::new("aoc_2021_09-part1", size),
            size,
            |b, &n| {
                b.iter_batched_ref(
                    || HeightMap::gen(&mut rng, n),
                    |map| map.part1(),
                    BatchSize::NumIterations(1),
                )
            },
        );

        c.bench_with_input(
            BenchmarkId::new("aoc_2021_09-part2", size),
            size,
            |b, &n| {
                b.iter_batched_ref(
                    || HeightMap::gen(&mut rng, n),
                    |map| map.part2(),
                    BatchSize::NumIterations(1),
                )
            },
        );
    }
}

criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(10);
    targets = run_benches
);
criterion_main!(benches);

use aoc_2022_09::{gen_moves, part1, part2};
use criterion::{criterion_group, criterion_main, BatchSize, BenchmarkId, Criterion};
use rand::{rngs::StdRng, SeedableRng};

fn run_benches(c: &mut Criterion) {
    let mut rng = StdRng::seed_from_u64(42);
    for size in [100, 1000, 2000].iter() {
        c.bench_with_input(
            BenchmarkId::new("aoc_2022_09-part1", size),
            size,
            |b, &n| {
                b.iter_batched_ref(
                    || gen_moves(&mut rng, n).collect::<Vec<_>>(),
                    |moves| part1(moves.iter()),
                    BatchSize::SmallInput,
                )
            },
        );

        c.bench_with_input(
            BenchmarkId::new("aoc_2022_09-part2", size),
            size,
            |b, &n| {
                b.iter_batched_ref(
                    || gen_moves(&mut rng, n).collect::<Vec<_>>(),
                    |moves| part2(moves.iter()),
                    BatchSize::SmallInput,
                )
            },
        );
    }
}

criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(10);
    targets = run_benches
);
criterion_main!(benches);

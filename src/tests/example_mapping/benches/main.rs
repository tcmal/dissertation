use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use rand::{rngs::StdRng, SeedableRng};

fn run_benches(c: &mut Criterion) {
    let mut rng = StdRng::seed_from_u64(42);
    for size in [50, 150, 2_500, 7_500].iter() {
        c.bench_with_input(
            BenchmarkId::new("example_mapping-insert_get", size),
            size,
            |b, &n| {
                b.iter(|| example_mapping::insert_get(&mut rng, n));
            },
        );
    }
}

criterion_group!(benches, run_benches);
criterion_main!(benches);

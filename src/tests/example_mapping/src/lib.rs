#![feature(type_alias_impl_trait)]
mod types;

use primrose_library::traits::Mapping;
use rand::{rngs::StdRng, Rng};
use std::hint::black_box;
use types::*;

pub fn insert_get(rng: &mut StdRng, n: usize) {
    let mut set: Map<usize, usize> = Map::default();

    for _ in 0..n {
        set.insert(rng.gen(), rng.gen());
    }

    for i in 0..2 * n {
        black_box(|x| x)(black_box(set.get(&i)));
    }
}

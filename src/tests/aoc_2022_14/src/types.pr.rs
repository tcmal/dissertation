/*SPEC*
property order-preserved<T> {
    \c <: (Container, Indexable) -> (forall \x -> ((equal? (op-last ((op-insert c) x))) x))
}

property unique<T> {
    \c <: (Container) -> ((for-all-elems c) \a -> ((unique-count? a) c))
}

type List<T> = {c impl (Container, Indexable) | (order-preserved c)}
type Set<T> = {c impl (Container) | (unique c)}
 *ENDSPEC*/

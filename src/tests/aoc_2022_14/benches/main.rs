use aoc_2022_14::tests::*;
use criterion::{criterion_group, criterion_main, BatchSize, BenchmarkId, Criterion};
use rand::{rngs::StdRng, SeedableRng};

fn run_benches(c: &mut Criterion) {
    c.bench_with_input(
        BenchmarkId::new("aoc_2022_14-part1", "small"),
        &SMALL_INPUT,
        |b, &inp| b.iter_batched_ref(|| parse_input(inp), |w| w.part1(), BatchSize::SmallInput),
    );
    c.bench_with_input(
        BenchmarkId::new("aoc_2022_14-part1", "large"),
        &LARGE_INPUT,
        |b, &inp| b.iter_batched_ref(|| parse_input(inp), |w| w.part1(), BatchSize::SmallInput),
    );

    c.bench_with_input(
        BenchmarkId::new("aoc_2022_14-part2", "small"),
        &SMALL_INPUT,
        |b, &inp| b.iter_batched_ref(|| parse_input(inp), |w| w.part2(), BatchSize::SmallInput),
    );
    c.bench_with_input(
        BenchmarkId::new("aoc_2022_14-part2", "large"),
        &LARGE_INPUT,
        |b, &inp| b.iter_batched_ref(|| parse_input(inp), |w| w.part2(), BatchSize::SmallInput),
    );
}

criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(10);
    targets = run_benches
);
criterion_main!(benches);

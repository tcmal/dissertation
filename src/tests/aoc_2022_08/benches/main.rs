use aoc_2022_08::HeightMap;
use criterion::{criterion_group, criterion_main, BatchSize, BenchmarkId, Criterion};
use rand::{rngs::StdRng, SeedableRng};

fn run_benches(c: &mut Criterion) {
    let mut rng = StdRng::seed_from_u64(1337);
    for size in [100, 200].iter() {
        c.bench_with_input(
            BenchmarkId::new("aoc_2022_08-part1", size),
            size,
            |b, &n| {
                b.iter_batched_ref(
                    || HeightMap::gen(&mut rng, n),
                    |map| map.part1(),
                    BatchSize::SmallInput,
                )
            },
        );

        c.bench_with_input(
            BenchmarkId::new("aoc_2022_08-part2", size),
            size,
            |b, &n| {
                b.iter_batched_ref(
                    || HeightMap::gen(&mut rng, n),
                    |map| map.part2(),
                    BatchSize::SmallInput,
                )
            },
        );
    }
}

criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(10);
    targets = run_benches
);
criterion_main!(benches);

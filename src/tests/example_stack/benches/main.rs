use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};

fn run_benches(c: &mut Criterion) {
    for size in [10, 1000, 10_000].iter() {
        c.bench_with_input(
            BenchmarkId::new("example_stack-push_pop", size),
            size,
            |b, &n| {
                b.iter(|| example_stack::push_pop(n));
            },
        );
    }
}

criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(20);
    targets = run_benches
);
criterion_main!(benches);

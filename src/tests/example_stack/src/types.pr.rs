/*SPEC*
property lifo<T> {
    \c <: (Stack) -> (forall \x -> ((equal? (op-pop ((op-push c) x))) x))
}

type StackCon<S> = {c impl (Stack) | (lifo c)}
*ENDSPEC*/

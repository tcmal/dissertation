#![feature(type_alias_impl_trait)]
mod types;

use primrose_library::traits::Stack;
use std::hint::black_box;
use types::*;

pub fn push_pop(n: usize) {
    let mut c = StackCon::default();
    for x in 0..n {
        c.push(x);
    }
    for _ in 0..n {
        black_box(|x| x)(c.pop());
    }
}

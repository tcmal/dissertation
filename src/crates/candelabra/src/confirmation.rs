use crate::types::ConTypeTo;
use anyhow::{Context, Result};
use camino::Utf8Path;
use log::{debug, log_enabled, trace, Level};
use primrose::{ConTypeName, ContainerSelection, ContainerSelector};
use std::{
    collections::HashMap,
    fs::File,
    io::Write,
    process::{Command, Stdio},
};

use crate::{
    cost::{
        benchmark::{parse_criterion_output, tee_output},
        BenchmarkResult,
    },
    Project, State,
};

/// A list of concrete selections for all container types in a project
pub type Selections = ConTypeTo<ContainerSelection>;

impl State {
    /// Run all benchmarks for the given project with the given container selections, returning the results
    pub fn run_benchmarks_with(
        &self,
        proj: &Project,
        selections: &Selections,
    ) -> Result<HashMap<String, BenchmarkResult>> {
        self.save_choices(proj, selections)
            .context("error setting up project")?;

        self.run_benchmarks(proj, &format!("{:?}", selections).replace('/', "-"))
            .context("error running benchmarks")
    }

    /// Run all benchmarks in the given project, returning their results.
    /// This can only parse criterion-formatted output
    pub fn run_benchmarks(
        &self,
        proj: &Project,
        baseline_name: &str,
    ) -> Result<HashMap<String, BenchmarkResult>> {
        let child = Command::new("cargo")
            .args([
                "bench",
                "--bench",
                "main",
                "--",
                "--save-baseline",
                baseline_name,
            ])
            .current_dir(proj.source_dir.as_std_path())
            .stdout(Stdio::piped())
            .stderr(if log_enabled!(Level::Debug) {
                Stdio::inherit()
            } else {
                Stdio::null()
            })
            .spawn()
            .context("Error running benchmark command")?;

        let output = tee_output(child)?;
        let mut map = parse_criterion_output(&output)
            .map(|(name, result)| (name.to_string(), result))
            .collect::<HashMap<_, _>>();

        map.insert("total".to_string(), map.values().sum());

        Ok(map)
    }

    /// Use the given selections for the container types in this project.
    /// Panics if a container type has not been selected.
    pub fn save_choices(&self, proj: &Project, selections: &Selections) -> Result<()> {
        debug!("Saving choices for project {}", proj.name);
        for (file, _) in self.project_candidate_list(proj)?.iter_2d() {
            let chosen = selections.iter_with_a(file);

            self.save_choices_file(file, chosen.map(|(ctn, sel)| (ctn, sel)))
                .with_context(|| format!("error setting up {}", file))?;
        }

        Ok(())
    }

    /// Use the given selections for the container types in this file.
    /// Panics if a container type has not been selected, or a choice is given for a container type not in this file.
    fn save_choices_file<'a>(
        &self,
        file: &Utf8Path,
        choices: impl Iterator<Item = (&'a ConTypeName, &'a ContainerSelection)>,
    ) -> Result<()> {
        debug!("Saving choices for {}", file);
        let selector = ContainerSelector::from_path(
            file.as_std_path(),
            self.paths.library_src.as_std_path(),
            self.model_size,
        )?;

        let new_code = selector.gen_replacement_file(choices);
        let new_path = file.to_string().replace(".pr", "");

        trace!("New code: {}", new_code);
        trace!("New path: {}", new_path);

        let mut f = File::create(new_path).context("error creating new source file")?;
        f.write_all(new_code.as_bytes())
            .context("error writing new code")?;

        Ok(())
    }
}

use camino::Utf8PathBuf;
use primrose::ConTypeName;
use serde::{Deserialize, Serialize};

/// Mapping from container type to some other type.
/// This is super common so we have an alias for it
pub type ConTypeTo<C> = Mapping2D<Utf8PathBuf, ConTypeName, C>;

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Mapping2D<A, B, C>(Vec<(A, Vec<(B, C)>)>);

impl<A: Clone + Eq, B: Clone + Eq, C> Mapping2D<A, B, C> {
    fn with_a_or_create(&mut self, a: &A) -> &mut Vec<(B, C)> {
        for (i, (ka, _)) in self.0.iter().enumerate() {
            if ka == a {
                return &mut self.0[i].1;
            }
        }

        self.0.push((a.clone(), vec![]));
        &mut self.0.last_mut().unwrap().1
    }

    pub fn get(&self, a: &A, b: &B) -> Option<&C> {
        self.0
            .iter()
            .find(|(ka, _)| ka == a)
            .and_then(|(_, bs)| bs.iter().find(|(kb, _)| kb == b).map(|(_, c)| c))
    }

    pub fn get_mut(&mut self, a: &A, b: &B) -> Option<&mut C> {
        self.0
            .iter_mut()
            .find(|(ka, _)| ka == a)
            .and_then(|(_, bs)| bs.iter_mut().find(|(kb, _)| kb == b).map(|(_, c)| c))
    }

    pub fn insert(&mut self, a: &A, b: &B, c: C) {
        let bs = self.with_a_or_create(a);
        if let Some((_, cv)) = bs.iter_mut().find(|(kb, _)| kb == b) {
            *cv = c;
        } else {
            bs.push((b.clone(), c));
        }
    }

    pub fn entry_mut<'a>(&'a mut self, a: &A, b: &'a B) -> EntryMut<'a, B, C> {
        let as_ = self.with_a_or_create(a);
        match as_.iter().position(|(kb, _)| kb == b) {
            Some(idx) => EntryMut::Occupied(&mut as_[idx].1),
            None => EntryMut::Vacant(b, as_),
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = (&A, &B, &C)> {
        self.0
            .iter()
            .flat_map(|(a, bs)| bs.iter().map(move |(b, c)| (a, b, c)))
    }

    pub fn keys(&self) -> impl Iterator<Item = (&A, &B)> {
        self.0
            .iter()
            .flat_map(|(a, bs)| bs.iter().map(move |(b, _)| (a, b)))
    }

    pub fn iter_2d(&self) -> impl Iterator<Item = (&'_ A, impl Iterator<Item = (&'_ B, &'_ C)>)> {
        self.0
            .iter()
            .map(|(a, bs)| (a, bs.iter().map(|(b, c)| (b, c))))
    }

    pub fn into_iter_2d(self) -> impl Iterator<Item = (A, impl Iterator<Item = (B, C)>)> {
        self.0.into_iter().map(|(a, bs)| (a, bs.into_iter()))
    }

    pub(crate) fn iter_with_a(&self, target_a: &A) -> impl Iterator<Item = &(B, C)> {
        self.0
            .iter()
            .find(|(a, _)| a == target_a)
            .into_iter()
            .flat_map(|(_, bs)| bs.iter())
    }
}

impl<A: Clone, B, C> IntoIterator for Mapping2D<A, B, C> {
    type Item = (A, B, C);

    type IntoIter = impl Iterator<Item = (A, B, C)>;

    fn into_iter(self) -> Self::IntoIter {
        self.0
            .into_iter()
            .flat_map(|(a, bs)| bs.into_iter().map(move |(b, c)| (a.clone(), b, c)))
    }
}

impl<A: Eq + Clone, B: Eq + Clone, C> FromIterator<(A, B, C)> for Mapping2D<A, B, C> {
    fn from_iter<T: IntoIterator<Item = (A, B, C)>>(iter: T) -> Self {
        let mut s = Self(vec![]);
        for (a, b, c) in iter {
            s.insert(&a, &b, c);
        }
        s
    }
}

impl<A: Eq + Clone, B: Eq + Clone, C> FromIterator<((A, B), C)> for Mapping2D<A, B, C> {
    fn from_iter<T: IntoIterator<Item = ((A, B), C)>>(iter: T) -> Self {
        let mut s = Self(vec![]);
        for ((a, b), c) in iter {
            s.insert(&a, &b, c);
        }
        s
    }
}

pub enum EntryMut<'a, B, C> {
    Occupied(&'a mut C),
    Vacant(&'a B, &'a mut Vec<(B, C)>),
}

impl<'a, B: Clone, C> EntryMut<'a, B, C> {
    pub fn and_modify(&mut self, f: impl FnOnce(&mut C)) -> &mut Self {
        match self {
            EntryMut::Occupied(c) => f(*c),
            EntryMut::Vacant(_, _) => (),
        };

        self
    }
    pub fn or_insert(&mut self, new: C) -> &mut Self {
        match self {
            EntryMut::Occupied(c) => **c = new,
            EntryMut::Vacant(b, bs) => bs.push(((**b).clone(), new)),
        };

        self
    }
}

//! Generating and caching primrose candidate results

use std::{collections::HashMap, fs::metadata, time::SystemTime};

use anyhow::{bail, Context, Result};
use camino::Utf8Path;
use log::{debug, warn};
use primrose::{ConTypeName, ContainerSelector, ImplName};
use serde::{Deserialize, Serialize};

use crate::{
    cache::{gen_tree_hash, FileCache},
    paths::Paths,
    project::Project,
    types::ConTypeTo,
    State,
};

/// List of candidates for a whole project, by file and by type name
pub type ProjectCandidateList = ConTypeTo<Vec<ImplName>>;

/// Candidate container types for a single file
type FileCandidates = HashMap<ConTypeName, Vec<ImplName>>;

/// Info for getting & caching candidate types
pub struct CandidatesStore {
    pub store: FileCache<Utf8Path, CacheEntry>,
    pub lib_hash: u64,
}

/// Entry in the benchmark cache
#[derive(Serialize, Deserialize, Debug)]
pub struct CacheEntry {
    lib_hash: u64,
    mod_time: SystemTime,
    value: FileCandidates,
}

impl CandidatesStore {
    /// Create a new store, using the given paths.
    /// Benchmarks are cached in `paths.target_dir / candelabra / primrose_results`
    pub fn new(paths: &Paths) -> Result<Self> {
        let base_dir = paths.target_dir.join("candelabra").join("primrose_results");

        let lib_hash =
            gen_tree_hash(&paths.library_crate).context("Error generating library hash")?;
        debug!("Initialised candidate cacher with hash {}", lib_hash);

        Ok(Self {
            store: FileCache::new(base_dir, move |k, v: &CacheEntry| {
                let mod_time = metadata(k)
                    .map(|m| m.modified())
                    .unwrap_or(Ok(SystemTime::UNIX_EPOCH))
                    .unwrap();
                v.lib_hash == lib_hash && v.mod_time == mod_time
            })?,
            lib_hash,
        })
    }
}

impl State {
    /// Run primrose on all files in the given project.
    /// Returns a list of all candidates for each container type in each file.
    pub fn project_candidate_list(&self, project: &Project) -> Result<ProjectCandidateList> {
        let mut all_candidates = ProjectCandidateList::default();
        for file in project.find_primrose_files()? {
            let result = match self.candidates.store.find(&file)? {
                Some(x) => x.value,
                None => self.calc_candidates(&file)?,
            };

            for (con_type_id, candidates) in result {
                all_candidates.insert(&file, &con_type_id, candidates);
            }
        }

        Ok(all_candidates)
    }

    /// Find candidate types for every selection site in a given path
    fn calc_candidates(&self, path: &Utf8Path) -> Result<FileCandidates> {
        let selector = ContainerSelector::from_path(
            path.as_std_path(),
            self.paths.library_src.as_std_path(),
            self.model_size,
        )
        .with_context(|| format!("error getting container selector for {}", path))?;

        let candidates: FileCandidates = selector
            .find_all_candidates()?
            .into_iter()
            .map(|(k, v)| (k.to_string(), v))
            .collect();

        if candidates.iter().any(|(_, impls)| impls.is_empty()) {
            bail!("Could not find candidates for all container types");
        }

        let mod_time = metadata(path)?.modified()?;
        if let Err(e) = self.candidates.store.put(
            path,
            &CacheEntry {
                lib_hash: self.candidates.lib_hash,
                value: candidates.clone(),
                mod_time,
            },
        ) {
            warn!("Error caching candidates for {}: {}", path, e);
        }
        Ok(candidates)
    }
}

//! Generating, caching, and using cost models
pub(crate) mod benchmark;
mod fit;

pub use benchmark::{BenchmarkResult, Results as BenchmarkResults};
pub use fit::{Cost, Estimator};

use std::collections::HashMap;

use anyhow::{anyhow, bail, Context, Result};

use benchmark::Results;
use log::{debug, warn};
use primrose::{LibSpec, LibSpecs};
use serde::{Deserialize, Serialize};

use crate::{
    cache::{gen_tree_hash, FileCache},
    cost::benchmark::run_benchmarks,
    paths::Paths,
    State,
};

/// Cost model for a container, capable of estimating cost of each supported operation.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CostModel {
    pub by_op: HashMap<String, Estimator>,
}

/// Information for getting & caching cost information for container implementations.
pub struct ResultsStore {
    store: FileCache<str, CacheEntry>,
    lib_specs: LibSpecs,
    lib_hash: u64,
}

/// Entry in the cost info cache
#[derive(Serialize, Deserialize)]
struct CacheEntry {
    /// Hash of the primrose library at the time measurements were taken
    lib_hash: u64,

    /// The resulting cost model
    model: CostModel,

    /// The raw benchmark results
    results: Results,
}

impl ResultsStore {
    /// Create a new store, using the given paths.
    /// Benchmarks are cached in `paths.target_dir / candelabra / benchmark_results`
    pub fn new(paths: &Paths) -> Result<Self> {
        let lib_specs =
            LibSpec::read_all(paths.library_src.as_std_path()).map_err(|e| anyhow!("{}", e))?;

        let base_dir = paths
            .target_dir
            .join("candelabra")
            .join("benchmark_results");

        let lib_hash =
            gen_tree_hash(&paths.library_crate).context("Error generating library hash")?;

        debug!("Initialised benchmark cacher with hash {}", lib_hash);

        Ok(Self {
            store: FileCache::new(base_dir, move |_, v: &CacheEntry| v.lib_hash == lib_hash)?,
            lib_specs,
            lib_hash,
        })
    }
}

impl State {
    /// Get or calculate the cost model for the given type.
    /// Will panic if `name` is not in library specs.
    pub fn cost_model(&self, name: &str) -> Result<CostModel> {
        Ok(self.cost_info(name)?.0)
    }

    /// Get information about the given type's cost, including raw benchmark results
    /// Will panic if `name` is not in library specs.
    pub fn cost_info(&self, name: &str) -> Result<(CostModel, Results)> {
        match self.results.store.find(name)? {
            Some(x) => Ok((x.model, x.results)),
            None => {
                let (model, results) = self.calc_cost_model(name)?;
                if let Err(e) = self.results.store.put(
                    name,
                    &CacheEntry {
                        lib_hash: self.results.lib_hash,
                        model: model.clone(),
                        results: results.clone(),
                    },
                ) {
                    warn!("Error caching benchmark outputs for {}: {}", name, e);
                }
                Ok((model, results))
            }
        }
    }

    /// Calculate cost information for the given type
    fn calc_cost_model(&self, name: &str) -> Result<(CostModel, Results)> {
        if !self.results.lib_specs.contains_key(name) {
            bail!("no such implementation in library");
        }

        let results = run_benchmarks(name, &self.paths, &self.results.lib_specs)?;
        Ok((build_cost_model(results.clone())?, results))
    }

    pub fn lib_specs(&self) -> &LibSpecs {
        &self.results.lib_specs
    }
}

fn build_cost_model(results: Results) -> Result<CostModel> {
    Ok(CostModel {
        by_op: results
            .by_op
            .into_iter()
            .map(|(op, os)| {
                debug!("Fitting op {} with {} observations", op, os.len());
                (op, Estimator::fit(&os))
            })
            .collect(),
    })
}

//! Benchmarking of container types

use std::io::{self, Read};
use std::process::{Child, Stdio};
use std::str::FromStr;
use std::{
    collections::HashMap,
    fs::{copy, create_dir, File},
    io::Write,
    process::Command,
};

use anyhow::{bail, Context, Result};
use log::{info, log_enabled, Level};
use primrose::{LibSpec, LibSpecs};
use serde::{Deserialize, Serialize};
use tempfile::{tempdir, TempDir};

use crate::paths::Paths;

use super::Cost;

/// The name of the element type we use for benchmarking
pub const ELEM_TYPE: &str = "usize";

/// String representation of the array of N values we use for benchmarking
pub const NS: &str = "[
    10, 50, 100, 250, 500, 1_000, 6_000, 12_000, 24_000, 36_000, 48_000, 60_000
]";

/// Fixed seed for benchmarking
pub const SEED: usize = 1337;

/// Results for a whole suite of benchmarks
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Results {
    /// Results for each collection operation
    pub by_op: HashMap<OpName, Vec<Observation>>,
}

/// Name of an operation
pub type OpName = String;

/// The first key in the tuple is the `n` of the container before the benchmark was taken, and the second the results of the benchmark.
pub type Observation = (usize, BenchmarkResult);

/// Results for a single benchmark
pub type BenchmarkResult = f64;

/// Run benchmarks for the given container type, returning the results.
/// Panics if the given name is not in the library specs.
pub fn run_benchmarks(name: &str, paths: &Paths, lib_specs: &LibSpecs) -> Result<Results> {
    let lib_spec = lib_specs
        .get(name)
        .expect("name passed to benchmarkspec not in libspecs");

    // Generate crate & source
    let crate_ = prepare_crate(name, paths, lib_spec)?;

    // Build and run
    info!("Building and running benchmarks for {}", name);
    let child = Command::new("cargo")
        .args(["run", "--release", "--", "--bench"])
        .current_dir(crate_.path())
        .env("CARGO_TARGET_DIR", &paths.target_dir) // Share target directory
        .stdout(Stdio::piped())
        .stderr(if log_enabled!(Level::Debug) {
            Stdio::inherit()
        } else {
            Stdio::null()
        })
        .spawn()
        .context("Error running build command")?;

    // Deserialise benchmark results
    let output = tee_output(child)?;
    let measurements = parse_criterion_output(&output).flat_map(|(name, result)| {
        let (op, n) = name.trim().split_once('/')?;
        Some((op.to_string(), usize::from_str(n).ok()?, result))
    });

    let mut by_op = HashMap::new();
    for (op, n, result) in measurements {
        by_op
            .entry(op.to_string())
            .and_modify(|v: &mut Vec<(usize, BenchmarkResult)>| v.push((n, result)))
            .or_insert(vec![(n, result)]);
    }

    Ok(Results { by_op })
}

pub(crate) fn tee_output(mut child: Child) -> Result<String> {
    // tee the output to stdout and a vector
    let mut stdout = child.stdout.take().unwrap();
    let mut output = Vec::new();
    let mut buf = vec![0; 100];
    while let Ok(None) = child.try_wait() {
        let n = stdout.read(&mut buf)?;
        let read = &buf[0..n];
        if log_enabled!(Level::Trace) {
            io::stdout().write_all(read)?;
        }
        output.extend(read);
    }

    let run_output = child.try_wait().unwrap().unwrap();
    if !run_output.success() {
        bail!("Error result from benchmark. Output: {:?}", run_output);
    }

    // Deserialise benchmark results
    String::from_utf8(output).context("Error interpreting output as UTF-8")
}

pub(crate) fn parse_criterion_output(
    output: &str,
) -> impl Iterator<Item = (String, BenchmarkResult)> + '_ {
    output
        .lines()
        .enumerate()
        .flat_map(|(i, l)| {
            // looking for lines like:
            // clear/100000            time:   [1.3279 µs 1.4378 µs 1.5520 µs]
            let mut spl = l.split("time:");
            let mut name = spl.next()?.trim().to_string();
            if name.is_empty() {
                // Sometimes criterion splits the name and time to different lines.
                // We could deal with this better, but it makes the parsing code much more complicated for little benefit.
                // Instead, fall back to the line number.
                name = format!("l{i}");
            }
            Some((name, spl.next()?))
        })
        .flat_map(|(name, timings)| {
            let mut timings = timings
                .trim()
                .strip_prefix('[')?
                .strip_suffix(']')?
                .split(' ');

            let _min = parse_time_str(timings.next()?, timings.next()?)?;
            let result = parse_time_str(timings.next()?, timings.next()?)?;
            // let _max = parse_time_str(timings.next()?, timings.next()?)?;

            Some((name, result))
        })
}

fn parse_time_str(quantity: &str, suffix: &str) -> Option<Cost> {
    Some(
        f64::from_str(quantity).ok()?
            * match suffix {
                "s" => 1e9,
                "ms" => 1e6,
                "µs" => 1e3,
                "ns" => 1.0,
                "ps" => 1e-3,
                s => unimplemented!("unrecognised suffix {}", s),
            },
    )
}

fn prepare_crate(name: &str, paths: &Paths, lib_spec: &LibSpec) -> Result<TempDir> {
    // Directory we will create the crate in
    let crate_tempdir = tempdir()?;
    let crate_dir = crate_tempdir.path();
    info!(
        "Preparing benchmarking crate for {} in {:?}",
        name, crate_dir
    );

    // Write the manifest
    let mut manifest =
        File::create(crate_dir.join("Cargo.toml")).context("Error creating Cargo.toml")?;
    manifest
        .write_all(
            format!(
                "
[package]
name = \"bench\"
version = \"0.1.0\"
edition = \"2021\"

[dependencies]
candelabra-benchmarker = {{ path = \"{}\" }}
primrose-library = {{ path = \"{}\" }}
",
                paths.benchmarker_crate, paths.library_crate,
            )
            .as_bytes(),
        )
        .context("Error writing Cargo.toml")?;

    // Ensure we use the right toolchain
    let orig_toolchain_file = paths.base.join("rust-toolchain.toml");
    copy(orig_toolchain_file, crate_dir.join("rust-toolchain.toml"))
        .context("Error writing rust-toolchain.toml")?;

    // Generate the code for running our benchmarks
    let mut benchmark_statements = String::new();

    // Add benchmarks for implemented traits
    let implemented_traits = lib_spec.interface_provide_map.keys();
    for tr in implemented_traits {
        if tr == "Mapping" {
            benchmark_statements += &format!(
            "candelabra_benchmarker::benchmark_{0}::<{name}<{ELEM_TYPE}, {ELEM_TYPE}>, _, _>(&mut rng, &NS);",
            tr.to_lowercase(),
        );
        } else {
            benchmark_statements += &format!(
                "candelabra_benchmarker::benchmark_{0}::<{name}<{ELEM_TYPE}>, _>(&mut rng, &NS);",
                tr.to_lowercase(),
            );
        }
    }

    // Write the benchmarking source, using our generated benchmarker code.
    let src_dir = crate_dir.join("src");
    create_dir(&src_dir).context("Error creating src directory")?;

    let mut src_file = File::create(src_dir.join("main.rs")).context("Error creating main.rs")?;
    src_file
        .write_all(
            format!(
                "
use candelabra_benchmarker::rand::{{rngs::StdRng, SeedableRng}};
const NS: &[usize] = &{NS};
fn main() {{
    let mut rng = StdRng::seed_from_u64({SEED});
    {benchmark_statements}
}}
",
            )
            .as_bytes(),
        )
        .context("Error writing to main.rs")?;

    Ok(crate_tempdir)
}

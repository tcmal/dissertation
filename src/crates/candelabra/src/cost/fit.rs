//! Fitting a 3rd-order polynomial to benchmark results
//! Based on code from al-jshen: <https://github.com/al-jshen/compute/tree/master>

use super::benchmark::Observation;
use log::trace;
use na::{Dyn, OVector};
use nalgebra::{dimension, Matrix, VecStorage};
use serde::{Deserialize, Serialize};

/// Estimates costs using a linear regression model.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Estimator {
    pub coeffs: [f64; 4],
}

/// Approximate cost of an action.
/// This is an approximation for the number of nanoseconds it would take.
pub type Cost = f64;

impl Estimator {
    /// Fit from the given set of observations, using the least squared method.
    pub fn fit(results: &[Observation]) -> Self {
        let results = Self::discard_outliers(results);
        let (xs, ys) = Self::to_data(&results);

        let xv = Self::prepare_input(&xs);
        let xtx = xv.transpose() * xv.clone();
        let xtxinv = xtx.try_inverse().unwrap();
        let xty = xv.transpose() * ys;
        let coeffs = xtxinv * xty;

        Self {
            coeffs: coeffs.into(),
        }
    }

    /// Reshape an input dataset into the required input for our model.
    fn prepare_input(
        xs: &[f64],
    ) -> Matrix<
        f64,
        dimension::Dyn,
        dimension::Const<4>,
        VecStorage<f64, dimension::Dyn, dimension::Const<4>>,
    > {
        let mut mat =
            Matrix::<_, dimension::Dyn, dimension::Const<4>, _>::from_element(xs.len(), 1.0);

        for (row, x) in xs.iter().enumerate() {
            // First column is all 1s, for constant factor
            // Linear, then powers
            for col in 1..=2 {
                mat[(row, col)] = x.powi(col as i32);
            }
            // Last column is logarithm
            mat[(row, 3)] = x.log2();
        }

        mat
    }

    /// Get the normalised root mean square error with respect to some data points
    pub fn nrmse(&self, results: &[Observation]) -> f64 {
        let (xs, ys) = Self::to_data(results);
        let mean = ys.sum() / xs.len() as f64;
        let mse: f64 = xs
            .iter()
            .zip(ys.iter())
            .map(|(x, y)| (y - self.estimatef(*x)).powi(2))
            .sum::<f64>()
            / xs.len() as f64;

        mse.sqrt() / mean
    }

    /// Estimate the cost of a given operation at the given `n`.
    pub fn estimate(&self, n: usize) -> Cost {
        self.estimatef(n as f64)
    }

    /// Estimate the cost of a given operation at the given `n`.
    pub fn estimatef(&self, n: f64) -> Cost {
        let mut raw = self.coeffs[0];
        for pow in 1..=2 {
            raw += n.powi(pow as i32) * self.coeffs[pow];
        }
        raw += n.log2() * self.coeffs[3];

        raw.max(0.0) // can't be below 0
    }

    /// Convert a list of observations to the format we use internally.
    fn to_data(results: &[Observation]) -> (Vec<f64>, OVector<f64, Dyn>) {
        let xs = results.iter().map(|(n, _)| *n as f64).collect::<Vec<_>>();
        let ys = OVector::<f64, Dyn>::from_iterator(
            results.len(),
            results.iter().map(|(_, results)| *results),
        );

        (xs, ys)
    }

    fn discard_outliers(results: &[Observation]) -> Vec<Observation> {
        let mut ns = results.iter().map(|(n, _)| *n).collect::<Vec<_>>();
        ns.dedup();
        let mut new_results = Vec::with_capacity(results.len());
        for &n in ns.iter() {
            let mut n_results: Vec<_> = results.iter().filter(|(n2, _)| *n2 == n).collect();
            let old_len = n_results.len();

            let mean = n_results.iter().map(|(_, x)| x).sum::<f64>() / n_results.len() as f64;
            let dev = (n_results
                .iter()
                .map(|(_, x)| (x - mean).powi(2))
                .sum::<f64>()
                / n_results.len() as f64)
                .sqrt();

            n_results.retain(|(_, x)| (*x - mean).abs() >= dev);
            trace!(
                "Discarded {} outliers for n = {n}",
                old_len - n_results.len()
            );

            new_results.extend(n_results);
        }

        new_results
    }
}

use camino::Utf8PathBuf;
use std::{
    env::{self, current_dir},
    path::PathBuf,
};

/// Paths used throughout execution
#[derive(Debug, Clone)]
pub struct Paths {
    pub base: Utf8PathBuf,
    pub library_crate: Utf8PathBuf,
    pub library_src: Utf8PathBuf,
    pub benchmarker_crate: Utf8PathBuf,
    pub target_dir: Utf8PathBuf,
}

impl Paths {
    pub fn from_base(base: Utf8PathBuf) -> Self {
        Paths {
            library_crate: base.join("crates").join("library"),
            library_src: base.join("crates").join("library").join("src"),
            benchmarker_crate: base.join("crates").join("benchmarker"),
            target_dir: {
                let mut pwd = current_dir().unwrap();
                pwd.push("target");
                pwd.try_into().unwrap()
            },
            base,
        }
    }
}

impl Default for Paths {
    fn default() -> Self {
        let path = if let Ok(var) = env::var("CANDELABRA_SRC_DIR") {
            var.into()
        } else {
            // Most the time this won't work, but it's worth a shot.
            let mut path = PathBuf::from(file!());
            path.pop(); // main.rs
            path.pop(); // src
            path.pop(); // candelabra-cli
            path.pop(); // crates
            if path.components().count() == 0 {
                path.push(".");
            }
            path
        };

        Paths::from_base(path.canonicalize().expect(
            "candelabra source directory not found. please specify it with CANDELABRA_SRC_DIR",
        ).try_into().expect("candelabra source directory has non-utf8 components in it (???)"))
    }
}

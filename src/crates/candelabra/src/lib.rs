#![feature(impl_trait_in_assoc_type)]

use anyhow::Result;
use cache::FileCache;

use crate::{candidates::CandidatesStore, cost::ResultsStore};

extern crate nalgebra as na;

mod cache;
pub mod candidates;
mod confirmation;
pub mod cost;
pub mod profiler;
pub mod select;
pub mod types;

mod paths;
mod project;
pub use paths::Paths;
pub use project::Project;

/// Shared state for program execution
pub struct State {
    /// Paths used throughout execution
    paths: Paths,

    /// Cache for candidate types for primrose files & annotations
    candidates: CandidatesStore,

    /// Results and cost models
    results: ResultsStore,

    /// Cache for profiler info
    profiler_info_cache: FileCache<String, profiler::CacheEntry>,

    /// The model size used for primrose operations
    model_size: usize,
}

impl State {
    pub fn new(paths: Paths) -> Result<Self> {
        Ok(Self {
            candidates: CandidatesStore::new(&paths)?,
            results: ResultsStore::new(&paths)?,
            profiler_info_cache: Self::usage_profile_cache(&paths)?,

            model_size: 3, // This could be made customisable in future
            paths,
        })
    }
}

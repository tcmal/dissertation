//! Profiling applications for info about container usage

mod info;

use anyhow::{Context, Result};
use camino::{Utf8Path, Utf8PathBuf};
use log::{info, log_enabled, trace, warn, Level};
use primrose::{ConTypeName, ContainerSelection, ContainerSelector, ImplName};
use serde::{Deserialize, Serialize};
use std::io::Write;
use std::{
    fs::{read_dir, File},
    io::Read,
    process::{Command, Stdio},
};
use tempfile::tempdir;

use crate::cache::{gen_tree_hash, FileCache};
use crate::cost::benchmark::tee_output;
use crate::project::Project;
use crate::types::{ConTypeTo, Mapping2D};
use crate::{Paths, State};

pub use self::info::{ProfilerPartition, UsageProfile};

type ConTypeRef<'a> = (&'a Utf8PathBuf, &'a ConTypeName);

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct CacheEntry {
    proj_hash: u64,
    proj_location: Utf8PathBuf,
    info: ProjectProfilingInfo,
}

/// Profiling
type ProjectProfilingInfo = ConTypeTo<UsageProfile>;

impl State {
    pub(crate) fn usage_profile_cache(paths: &Paths) -> Result<FileCache<String, CacheEntry>> {
        FileCache::new(
            paths.target_dir.join("candelabra").join("profiler_info"),
            |_, v: &CacheEntry| {
                let proj_hash = gen_tree_hash(&v.proj_location).unwrap_or(0);
                v.proj_hash == proj_hash
            },
        )
    }

    /// Get or calculate profiler info for the given project.
    /// Results are cached by the modification time of the project's source tree
    pub fn usage_profile(&self, project: &Project) -> Result<ProjectProfilingInfo> {
        match self.profiler_info_cache.find(&project.name)? {
            Some(x) => Ok(x.info),
            None => {
                let info = self.calc_usage_profile(project)?;

                let proj_hash = gen_tree_hash(&project.source_dir)
                    .context("Error generating project directory hash")?;
                if let Err(e) = self.profiler_info_cache.put(
                    &project.name,
                    &CacheEntry {
                        proj_hash,
                        proj_location: project.source_dir.clone(),
                        info: info.clone(),
                    },
                ) {
                    warn!("Error caching profiler info for {}: {}", &project.name, e);
                }

                Ok(info)
            }
        }
    }

    /// Calculate profiler info for the given project.
    fn calc_usage_profile(&self, project: &Project) -> Result<ProjectProfilingInfo> {
        let candidate_list = self.project_candidate_list(project)?;
        let ct_refs = candidate_list
            .iter()
            .map(|(f, ctn, _)| (f, ctn))
            .collect::<Vec<_>>();

        self.project_profiling_prep(project, &ct_refs)?;
        let mut acc = ProjectProfilingInfo::default();
        for name in project.benchmarks.iter() {
            for (f, ctn, new_results) in self
                .profile_benchmark(project, name, &ct_refs)
                .with_context(|| format!("Error profiling benchmark {}", name))?
                .iter()
            {
                acc.entry_mut(f, ctn)
                    .and_modify(|pi: &mut UsageProfile| pi.0.extend(new_results.0.iter().cloned()))
                    .or_insert(new_results.clone());
            }
        }

        Ok(acc)
    }

    /// Prepare the given project to be profiled, by replacing all candidate types with the profiler wrapper.
    fn project_profiling_prep(&self, project: &Project, ct_refs: &[ConTypeRef<'_>]) -> Result<()> {
        for (file, cts) in self.project_candidate_list(project)?.iter_2d() {
            self.file_profiling_prep(file, cts, ct_refs)
                .with_context(|| format!("error preparing {} for profiling", file))?;
        }

        Ok(())
    }

    /// Prepare the given file to be profiled, by replacing all candidate types with the profiler wrapper.
    fn file_profiling_prep<'a>(
        &self,
        file: &Utf8Path,
        candidates: impl Iterator<Item = (&'a ConTypeName, &'a Vec<ImplName>)>,
        con_types: &[ConTypeRef<'_>],
    ) -> Result<()> {
        info!("Setting up {} for profiling", file);

        let selector = ContainerSelector::from_path(
            file.as_std_path(),
            self.paths.library_src.as_std_path(),
            self.model_size,
        )
        .context("error creating container selector")?;

        let chosen = candidates
            .map(|(dest_name, impls)| {
                (
                    dest_name,
                    ContainerSelection::Profile(
                        impls[0].clone(),
                        con_types
                            .iter()
                            .position(|(search_file, search_ctn)| {
                                file == *search_file && dest_name == *search_ctn
                            })
                            .unwrap(),
                    ),
                )
            })
            .collect::<Vec<_>>();

        let new_code = selector.gen_replacement_file(chosen.iter().map(|(ctn, sel)| (*ctn, sel)));

        let new_path = file.to_string().replace(".pr", "");

        trace!("New code: {}", new_code);
        trace!("New path: {}", new_path);

        let mut f = File::create(new_path).context("error creating new source file")?;
        f.write_all(new_code.as_bytes())
            .context("error writing new code")?;

        Ok(())
    }

    /// Run the given benchmark on the project, and parse the resulting profiling information.
    fn profile_benchmark(
        &self,
        project: &Project,
        name: &str,
        con_types: &[ConTypeRef<'_>],
    ) -> Result<Mapping2D<Utf8PathBuf, ConTypeName, UsageProfile>> {
        let profiler_out_dir = tempdir()?;
        info!(
            "Running benchmark {} with out dir {:?}",
            name, profiler_out_dir
        );

        let child = Command::new("cargo")
            .current_dir(&project.source_dir)
            .args(["test", "--bench", name])
            .env("PROFILER_OUT_DIR", profiler_out_dir.as_ref()) // Where profiler info gets outputted
            .stdout(Stdio::piped())
            .stderr(if log_enabled!(Level::Debug) {
                Stdio::inherit()
            } else {
                Stdio::null()
            })
            .spawn()
            .context("Error running bench command")?;

        tee_output(child)?;

        let mut con_type_results = Mapping2D::default();
        for dir in read_dir(&profiler_out_dir)? {
            // each directory has an index, corresponding to the container type name
            let dir = dir?;
            let (f, ctn) = con_types[dir
                .file_name()
                .into_string()
                .unwrap()
                .parse::<usize>()
                .unwrap()];

            con_type_results.insert(
                f,
                ctn,
                UsageProfile::from(read_dir(dir.path())?.map(|f| -> Result<String> {
                    // read file contents
                    let mut contents = String::new();
                    File::open(f?.path())?.read_to_string(&mut contents)?;
                    Ok(contents)
                }))?,
            );
        }

        Ok(con_type_results)
    }
}

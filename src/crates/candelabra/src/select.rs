use std::collections::HashMap;

use crate::{cost::Cost, types::ConTypeTo, Project, State};

use anyhow::Result;
use log::trace;
use primrose::ContainerSelection;

pub type ProjectSelections = ConTypeTo<ContainerSelection>;
pub type ProjectCostEstimates = ConTypeTo<Vec<(ContainerSelection, Cost)>>;

impl State {
    /// Select a container implementation for each container type in the given project
    pub fn select(&self, project: &Project) -> Result<ProjectSelections> {
        Ok(self
            .rank_candidates(project)?
            .into_iter()
            .map(|(f, ctn, cs)| {
                (
                    f,
                    ctn,
                    cs.into_iter()
                        .reduce(
                            |acc @ (_, min), new @ (_, cost)| if min < cost { acc } else { new },
                        )
                        .unwrap()
                        .0,
                )
            })
            .collect())
    }

    pub fn rank_candidates(&self, project: &Project) -> Result<ProjectCostEstimates> {
        // get all candidates
        let all_candidates = self.project_candidate_list(project)?;

        // get profiling information
        let mut profiles = self.usage_profile(project)?;

        let mut acc = ProjectCostEstimates::default();
        for (f, ctn, candidates) in all_candidates.iter() {
            let mut costs = vec![];

            let profile_info = profiles.get_mut(f, ctn).unwrap();
            let cost_models = candidates
                .iter()
                .map(|name| Ok((name, self.cost_model(name)?)))
                .collect::<Result<HashMap<_, _>>>()?;

            // Get an estimate for each candidate
            for candidate in candidates {
                let model = self.cost_model(candidate)?;
                trace!("estimating cost of {candidate}");
                costs.push((
                    ContainerSelection::Singular(candidate.to_string()),
                    profile_info.estimate_cost(&model),
                ));
            }

            // Also attempt to split at an n value
            if let Some((split, cost)) = profile_info.check_for_nsplit(&cost_models) {
                costs.push((split, cost));
            }

            acc.insert(f, ctn, costs);
        }

        Ok(acc)
    }
}

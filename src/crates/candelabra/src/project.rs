use anyhow::{Context, Result};
use cargo_metadata::{camino::Utf8PathBuf, Package, Target};
use glob::glob;

/// A single package or crate that we wish to process.
#[derive(Debug, Clone)]
pub struct Project {
    pub name: String,
    pub benchmarks: Vec<String>,
    pub source_dir: Utf8PathBuf,
}

impl Project {
    pub fn new(package: Package) -> Self {
        Project {
            name: package.name.clone(),
            source_dir: package.manifest_path.parent().unwrap().to_path_buf(),
            benchmarks: package
                .targets
                .into_iter()
                .filter(Target::is_bench)
                .map(|t| t.name)
                .collect(),
        }
    }

    /// Find all primrose files (`.pr.rs`) in this project.
    pub fn find_primrose_files(&self) -> Result<Vec<Utf8PathBuf>> {
        glob(&format!("{}/**/*.pr.rs", self.source_dir))
            .unwrap()
            .flat_map(|p| p.map(|p| p.try_into()))
            .collect::<Result<Vec<_>, _>>()
            .context("error finding primrose files in project")
    }
}

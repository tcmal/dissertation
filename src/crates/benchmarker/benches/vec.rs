use rand::{rngs::StdRng, SeedableRng};

fn main() {
    let ns = [
        64, 128, 256, 512, 1024, 2048, 3072, 4096, 5120, 6144, 7168, 8192, 16384, 24576, 32768,
        40960, 49152, 57344, 65536,
    ];
    let mut rng = StdRng::seed_from_u64(42);
    candelabra_benchmarker::benchmark_container::<Vec<usize>, _>(&mut rng, &ns);
    candelabra_benchmarker::benchmark_indexable::<Vec<usize>, _>(&mut rng, &ns);
    candelabra_benchmarker::benchmark_stack::<Vec<usize>, _>(&mut rng, &ns);
}

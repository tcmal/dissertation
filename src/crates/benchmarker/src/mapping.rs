use primrose_library::traits::Mapping;
use rand::{
    distributions::{Distribution, Standard},
    rngs::StdRng,
    Rng,
};
use std::hint::black_box;

use crate::{benchmark_op, print_results};

pub fn benchmark_mapping<T, K, V>(rng: &mut StdRng, ns: &[usize])
where
    T: Mapping<K, V> + Default + Clone,
    K: Copy,
    V: Copy,
    Standard: Distribution<K> + Distribution<V>,
{
    for n in ns {
        scenario_populate::<T, K, V>(rng, *n);
        scenario_get::<T, K, V>(rng, *n);
        scenario_contains::<T, K, V>(rng, *n);
        scenario_remove::<T, K, V>(rng, *n);
        scenario_clear::<T, K, V>(rng, *n);
    }
}

fn scenario_populate<T, K, V>(rng: &mut StdRng, n: usize)
where
    T: Mapping<K, V> + Default + Clone,
    K: Copy,
    V: Copy,
    Standard: Distribution<K> + Distribution<V>,
{
    let mut results = benchmark_op(
        || {
            (
                T::default(),
                (0..n)
                    .map(|_| (rng.gen(), rng.gen()))
                    .collect::<Vec<(K, V)>>(),
            )
        },
        |(c, xs)| {
            for (k, v) in xs {
                c.insert(*k, *v);
            }
        },
    );

    // Since we've repeated n times in each run
    results.iter_mut().for_each(|t| *t /= n as f64);

    print_results("insert", n, &results);
}

fn scenario_get<T, K, V>(rng: &mut StdRng, n: usize)
where
    T: Mapping<K, V> + Default + Clone,
    K: Copy,
    V: Copy,
    Standard: Distribution<K> + Distribution<V>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();

            // decide where the element that we will search for will be
            let pivot = rng.gen_range(0..n);

            // insert the element at pivot, and keep track of what it is
            for _ in 0..pivot {
                c.insert(rng.gen(), rng.gen());
            }
            let chosen = rng.gen();
            c.insert(chosen, rng.gen());
            for _ in pivot..n {
                c.insert(rng.gen(), rng.gen());
            }

            (c, chosen)
        },
        |(c, chosen)| {
            black_box(c.get(chosen));
        },
    );

    print_results("get", n, &results);
}

fn scenario_contains<T, K, V>(rng: &mut StdRng, n: usize)
where
    T: Mapping<K, V> + Default + Clone,
    K: Copy,
    V: Copy,
    Standard: Distribution<K> + Distribution<V>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();

            // decide where the element that we will search for will be
            let pivot = rng.gen_range(0..n);

            // insert the element at pivot, and keep track of what it is
            for _ in 0..pivot {
                c.insert(rng.gen(), rng.gen());
            }
            let chosen = rng.gen();
            c.insert(chosen, rng.gen());
            for _ in pivot..n {
                c.insert(rng.gen(), rng.gen());
            }

            (c, chosen)
        },
        |(c, chosen)| c.contains(chosen),
    );

    print_results("contains", n, &results);
}

fn scenario_remove<T, K, V>(rng: &mut StdRng, n: usize)
where
    T: Mapping<K, V> + Default + Clone,
    K: Copy,
    V: Copy,
    Standard: Distribution<K> + Distribution<V>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();

            // decide where the element that we will search for will be
            let pivot = rng.gen_range(0..n);

            // insert the element at pivot, and keep track of what it is
            for _ in 0..pivot {
                c.insert(rng.gen(), rng.gen());
            }
            let chosen = rng.gen();
            c.insert(chosen, rng.gen());
            for _ in pivot..n {
                c.insert(rng.gen(), rng.gen());
            }

            (c, chosen)
        },
        |(c, chosen)| c.remove(chosen),
    );

    print_results("remove", n, &results);
}

fn scenario_clear<T, K, V>(rng: &mut StdRng, n: usize)
where
    T: Mapping<K, V> + Default + Clone,
    K: Copy,
    V: Copy,
    Standard: Distribution<K> + Distribution<V>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();

            for _ in 0..n {
                c.insert(rng.gen(), rng.gen());
            }
            c
        },
        |c| c.clear(),
    );

    print_results("clear", n, &results);
}

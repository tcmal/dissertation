use primrose_library::traits::Container;
use rand::{distributions::Standard, prelude::Distribution, rngs::StdRng, Rng};

use crate::{benchmark_op, print_results};

pub fn benchmark_container<T, E>(rng: &mut StdRng, ns: &[usize])
where
    T: Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    for n in ns {
        scenario_populate::<T, E>(rng, *n);
        scenario_contains::<T, E>(rng, *n);
        scenario_remove::<T, E>(rng, *n);
        scenario_clear::<T, E>(rng, *n);
    }
}

fn scenario_populate<T, E>(rng: &mut StdRng, n: usize)
where
    T: Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let mut results = benchmark_op(
        || (T::default(), (0..n).map(|_| rng.gen()).collect::<Vec<E>>()),
        |(c, xs)| {
            for x in xs {
                c.insert(*x);
            }
        },
    );

    // Since we've repeated n times in each run
    results.iter_mut().for_each(|t| *t /= n as f64);

    print_results("insert", n, &results);
}

fn scenario_contains<T, E>(rng: &mut StdRng, n: usize)
where
    T: Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();

            // decide where the element that we will search for will be
            let pivot = rng.gen_range(n / 4..n / 2 + n / 4);

            // insert the element at pivot, and keep track of what it is
            for _ in 0..pivot {
                c.insert(rng.gen());
            }
            let chosen = rng.gen();
            c.insert(chosen);
            for _ in pivot..n {
                c.insert(rng.gen());
            }

            (c, chosen)
        },
        |(c, search)| c.contains(search),
    );

    print_results("contains", n, &results);
}

fn scenario_remove<T, E>(rng: &mut StdRng, n: usize)
where
    T: Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();

            // decide where the element that we will search for will be
            let pivot = rng.gen_range(0..n);

            // insert the element at pivot, and keep track of what it is
            for _ in 0..pivot {
                c.insert(rng.gen());
            }
            let chosen = rng.gen();
            c.insert(chosen);
            for _ in pivot..n {
                c.insert(rng.gen());
            }

            (c, chosen)
        },
        |(c, remove)| c.remove(*remove),
    );

    print_results("remove", n, &results);
}

fn scenario_clear<T, E>(rng: &mut StdRng, n: usize)
where
    T: Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();
            for _ in 0..n {
                c.insert(rng.gen());
            }

            c
        },
        |c| c.clear(),
    );

    print_results("clear", n, &results);
}

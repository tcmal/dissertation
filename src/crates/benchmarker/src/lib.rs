mod container;
mod indexable;
mod mapping;
mod stack;

use std::{
    hint::black_box,
    time::{Duration, Instant},
};

pub use rand;

pub use container::*;
pub use indexable::*;
pub use mapping::*;
pub use stack::*;

const WARM_UP_TIME: Duration = Duration::from_millis(500);

pub type BenchmarkResult = f64;

fn print_results(op: &str, n: usize, measurements: &[BenchmarkResult]) {
    measurements.iter().for_each(|m| print_result(op, n, *m));
}

fn print_result(op: &str, n: usize, measurement: BenchmarkResult) {
    println!(
        "{}/{} time: [{:.3} ns {:.3} ns {:.3} ns]",
        op, n, measurement, measurement, measurement
    )
}

const TO_RUN: usize = 50;

/// Benchmark an operation [`TO_RUN`] times, returning the results.
///
/// `setup` is used to create the thing `op` acts on, and `undo` is called between each run to undo `op`.
/// If `undo` is invalid, this will return garbage results.
///
/// Warm-up for the setup is done beforehand.
fn benchmark_op<T, R>(
    mut setup: impl FnMut() -> T,
    mut op: impl FnMut(&mut T) -> R,
) -> Vec<BenchmarkResult> {
    let warmup_end = Instant::now() + WARM_UP_TIME;
    let mut warmup_n = 0;

    // Run warmup
    while Instant::now() < warmup_end {
        let mut target = black_box(setup());
        black_box(op(&mut target));
        warmup_n += 1;
    }

    println!(
        "benchmarking for estimated {}ms",
        (WARM_UP_TIME / warmup_n * TO_RUN as u32).as_millis()
    );
    let mut times = Vec::with_capacity(TO_RUN);

    // Benchmarking loop
    for _ in 0..TO_RUN {
        let mut target = black_box(setup());

        let start = Instant::now();
        black_box(op(&mut target));
        let end = Instant::now();
        drop(target);

        times.push((end - start).as_nanos() as f64);
    }

    times
}

use std::hint::black_box;

use primrose_library::traits::{Container, Stack};
use rand::{distributions::Standard, prelude::Distribution, rngs::StdRng, Rng};

use crate::{benchmark_op, print_results};

pub fn benchmark_stack<T, E>(rng: &mut StdRng, ns: &[usize])
where
    T: Stack<E> + Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    for n in ns {
        scenario_populate::<T, E>(rng, *n);
        scenario_drain::<T, E>(rng, *n);
    }
}

fn scenario_populate<T, E>(rng: &mut StdRng, n: usize)
where
    T: Stack<E> + Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let mut results = benchmark_op(
        || (T::default(), (0..n).map(|_| rng.gen()).collect::<Vec<E>>()),
        |(c, xs)| {
            for x in xs {
                c.push(*x);
            }
        },
    );

    // Since we've repeated n times in each run
    results.iter_mut().for_each(|t| *t /= n as f64);

    print_results("push", n, &results);
}

fn scenario_drain<T, E>(rng: &mut StdRng, n: usize)
where
    T: Stack<E> + Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let mut results = benchmark_op(
        || {
            let mut c = T::default();
            for _ in 0..n {
                c.push(rng.gen());
            }
            c
        },
        |c| {
            for _ in 0..n {
                black_box(c.pop());
            }
        },
    );

    // Since we've repeated n times in each run
    results.iter_mut().for_each(|t| *t /= n as f64);

    print_results("pop", n, &results);
}

use std::hint::black_box;

use primrose_library::traits::{Container, Indexable};
use rand::{distributions::Standard, prelude::Distribution, rngs::StdRng, Rng};

use crate::{benchmark_op, print_results};

pub fn benchmark_indexable<T, E>(rng: &mut StdRng, ns: &[usize])
where
    T: Indexable<E> + Container<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    for n in ns {
        scenario_first::<T, E>(rng, *n);
        scenario_last::<T, E>(rng, *n);
        scenario_nth::<T, E>(rng, *n);
    }
}

fn scenario_first<T, E>(rng: &mut StdRng, n: usize)
where
    T: Container<E> + Indexable<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();
            for _ in 0..n {
                c.insert(rng.gen());
            }

            c
        },
        |c| {
            black_box(c.first());
        },
    );

    print_results("first", n, &results);
}

fn scenario_last<T, E>(rng: &mut StdRng, n: usize)
where
    T: Container<E> + Indexable<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();
            for _ in 0..n {
                c.insert(rng.gen());
            }

            c
        },
        |c| {
            black_box(c.last());
        },
    );

    print_results("last", n, &results);
}

fn scenario_nth<T, E>(rng: &mut StdRng, n: usize)
where
    T: Container<E> + Indexable<E> + Default + Clone,
    E: Copy,
    Standard: Distribution<E>,
{
    let results = benchmark_op(
        || {
            let mut c = T::default();
            for _ in 0..n {
                c.insert(rng.gen());
            }

            (c, rng.gen_range(0..n))
        },
        |(c, idx)| {
            black_box(c.nth(*idx));
        },
    );

    print_results("nth", n, &results);
}

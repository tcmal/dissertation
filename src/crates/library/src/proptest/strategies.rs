use proptest::prelude::*;

use std::ops::Range;

use crate::{SortedVec, SortedVecSet, VecSet};

use proptest::collection::vec;

pub fn sorted_vec_set<T: Strategy + 'static>(
    element: T,
    size: Range<usize>,
) -> impl Strategy<Value = SortedVecSet<T::Value>>
where
    <T as Strategy>::Value: PartialEq + Ord,
{
    vec(element, size.clone()).prop_map(SortedVecSet::from_vec)
}

pub fn sorted_vec<T: Strategy + 'static>(
    element: T,
    size: Range<usize>,
) -> impl Strategy<Value = SortedVec<T::Value>>
where
    <T as Strategy>::Value: Ord,
{
    vec(element, size.clone()).prop_map(SortedVec::from_vec)
}

pub fn vec_set<T: Strategy + 'static>(
    element: T,
    size: Range<usize>,
) -> impl Strategy<Value = VecSet<T::Value>>
where
    <T as Strategy>::Value: Ord,
{
    vec(element, size.clone()).prop_map(VecSet::from_vec)
}

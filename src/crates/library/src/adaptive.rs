use std::marker::PhantomData;

use crate::traits::{Container, Indexable, Mapping, Stack};
use take_mut::take_or_recover;

pub enum AdaptiveContainer<const THRESHOLD: usize, LO, HI, E> {
    Low(LO, PhantomData<E>),
    High(HI),
}

impl<const THRESHOLD: usize, LO: Default, HI, E> Default
    for AdaptiveContainer<THRESHOLD, LO, HI, E>
{
    fn default() -> Self {
        Self::Low(LO::default(), PhantomData)
    }
}

macro_rules! get_inner {
    ($self:ident, $to:ident, $expr:expr) => {
        match $self {
            AdaptiveContainer::Low($to, _) => $expr,
            AdaptiveContainer::High($to) => $expr,
        }
    };
}

impl<const THRESHOLD: usize, LO: Default + Container<E>, HI: Default + Container<E>, E> Container<E>
    for AdaptiveContainer<THRESHOLD, LO, HI, E>
{
    #[inline(always)]
    fn len(&self) -> usize {
        get_inner!(self, c, c.len())
    }

    #[inline(always)]
    fn is_empty(&self) -> bool {
        get_inner!(self, c, c.is_empty())
    }

    #[inline(always)]
    fn contains(&self, x: &E) -> bool {
        get_inner!(self, c, c.contains(x))
    }

    #[inline(always)]
    fn insert(&mut self, elt: E) {
        get_inner!(self, c, c.insert(elt));
        if let Self::Low(l, _) = self {
            if l.len() < THRESHOLD {
                return;
            }
            // adapt!
            take_or_recover(self, Default::default, |old| {
                let Self::Low(l, _) = old else {
                    unreachable!();
                };
                Self::High(HI::from_iter(l))
            });
        }
    }

    #[inline(always)]
    fn remove(&mut self, elt: E) -> Option<E> {
        get_inner!(self, c, c.remove(elt))
    }

    #[inline(always)]
    fn clear(&mut self) {
        get_inner!(self, c, c.clear())
    }

    #[inline(always)]
    fn iter<'a>(&'a self) -> impl Iterator<Item = &'a E>
    where
        E: 'a,
    {
        match self {
            AdaptiveContainer::Low(lo, _) => AdaptiveContainerIter::Low(lo.iter(), PhantomData),
            AdaptiveContainer::High(hi) => AdaptiveContainerIter::High(hi.iter()),
        }
    }
}

impl<
        const THRESHOLD: usize,
        LO: Default + Container<E> + Stack<E>,
        HI: Default + Container<E> + Stack<E>,
        E,
    > Stack<E> for AdaptiveContainer<THRESHOLD, LO, HI, E>
{
    fn push(&mut self, elt: E) {
        get_inner!(self, c, c.push(elt));
        if let Self::Low(l, _) = self {
            if l.len() < THRESHOLD {
                return;
            }
            // adapt!
            take_or_recover(self, Default::default, |old| {
                let Self::Low(l, _) = old else {
                    unreachable!();
                };
                Self::High(HI::from_iter(l))
            });
        }
    }

    fn pop(&mut self) -> Option<E> {
        get_inner!(self, c, c.pop())
    }
}

impl<const THRESHOLD: usize, LO: Default + Indexable<E>, HI: Default + Indexable<E>, E> Indexable<E>
    for AdaptiveContainer<THRESHOLD, LO, HI, E>
{
    fn first(&self) -> Option<&E> {
        get_inner!(self, c, c.first())
    }

    fn last(&self) -> Option<&E> {
        get_inner!(self, c, c.last())
    }

    fn nth(&self, n: usize) -> Option<&E> {
        get_inner!(self, c, c.nth(n))
    }
}

impl<const THRESHOLD: usize, LO, HI, K, V> Mapping<K, V>
    for AdaptiveContainer<THRESHOLD, LO, HI, (K, V)>
where
    LO: Mapping<K, V>,
    HI: Mapping<K, V>,
{
    #[inline(always)]
    fn len(&self) -> usize {
        get_inner!(self, c, c.len())
    }

    #[inline(always)]
    fn contains(&mut self, x: &K) -> bool {
        get_inner!(self, c, c.contains(x))
    }

    #[inline(always)]
    fn insert(&mut self, key: K, val: V) -> Option<V> {
        let res = get_inner!(self, c, c.insert(key, val));
        if let Self::Low(l, _) = self {
            if l.len() < THRESHOLD {
                return res;
            }
            // adapt!
            take_or_recover(self, Default::default, |old| {
                let Self::Low(l, _) = old else {
                    unreachable!();
                };
                Self::High(HI::from_iter(l))
            });
        }
        res
    }

    #[inline(always)]
    fn get(&self, key: &K) -> Option<&V> {
        get_inner!(self, c, c.get(key))
    }

    #[inline(always)]
    fn remove(&mut self, key: &K) -> Option<V> {
        get_inner!(self, c, c.remove(key))
    }

    #[inline(always)]
    fn clear(&mut self) {
        *self = Self::Low(LO::default(), PhantomData);
    }

    #[inline(always)]
    fn iter<'a>(&'a self) -> impl Iterator<Item = (&'a K, &'a V)> + 'a
    where
        K: 'a,
        V: 'a,
    {
        match self {
            AdaptiveContainer::Low(lo, _) => AdaptiveContainerIter::Low(lo.iter(), PhantomData),
            AdaptiveContainer::High(hi) => AdaptiveContainerIter::High(hi.iter()),
        }
    }
}

impl<const THRESHOLD: usize, LO, HI, E> FromIterator<E> for AdaptiveContainer<THRESHOLD, LO, HI, E>
where
    LO: FromIterator<E>,
{
    fn from_iter<T: IntoIterator<Item = E>>(iter: T) -> Self {
        Self::Low(LO::from_iter(iter), PhantomData)
    }
}

impl<const THRESHOLD: usize, LO, HI, E> IntoIterator for AdaptiveContainer<THRESHOLD, LO, HI, E>
where
    LO: IntoIterator<Item = E>,
    HI: IntoIterator<Item = E>,
{
    type Item = E;

    type IntoIter = AdaptiveContainerIter<LO::IntoIter, HI::IntoIter, E>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            AdaptiveContainer::Low(l, _) => AdaptiveContainerIter::Low(l.into_iter(), PhantomData),
            AdaptiveContainer::High(h) => AdaptiveContainerIter::High(h.into_iter()),
        }
    }
}

pub enum AdaptiveContainerIter<LO, HI, E> {
    Low(LO, PhantomData<E>),
    High(HI),
}

impl<LO, HI, E> Iterator for AdaptiveContainerIter<LO, HI, E>
where
    LO: Iterator<Item = E>,
    HI: Iterator<Item = E>,
{
    type Item = E;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            AdaptiveContainerIter::Low(l, _) => l.next(),
            AdaptiveContainerIter::High(h) => h.next(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use crate::{traits::Container, AdaptiveContainer, SortedVecSet};

    #[test]
    fn adaptive_container_lo_functionality() {
        let mut c: AdaptiveContainer<10, SortedVecSet<_>, HashSet<_>, usize> = Default::default();

        for i in 0..5 {
            c.insert(i);
        }

        assert_eq!(c.len(), 5, "length is correct");
    }

    #[test]
    fn adaptive_container_adapts() {
        let mut c: AdaptiveContainer<10, SortedVecSet<_>, HashSet<_>, usize> = Default::default();

        for i in 1..=9 {
            c.insert(i);
        }

        if let AdaptiveContainer::High(_) = c {
            panic!("should not yet change to high variant");
        }

        c.insert(10);
        if let AdaptiveContainer::Low(_, _) = c {
            panic!("should change to high variant at threshold");
        }

        for i in 11..=20 {
            c.insert(i);
        }

        if let AdaptiveContainer::Low(_, _) = c {
            panic!("should remain at high variant");
        }
        assert_eq!(c.len(), 20, "length is correct");

        for i in 1..=20 {
            assert!(c.contains(&i), "contains correct data");
        }
    }
}

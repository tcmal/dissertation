/*LIBSPEC-NAME*
rust-hashmap-spec std::collections::BTreeMap
*ENDLIBSPEC-NAME*/

use crate::traits::Mapping;
pub use std::collections::BTreeMap;
use std::hash::Hash;

/*IMPL*
Mapping
*ENDIMPL*/
impl<K: Ord + Hash, V> Mapping<K, V> for BTreeMap<K, V> {
    /*LIBSPEC*
    /*OPNAME*
    len op-len pre-len post-len
    *ENDOPNAME*/
    (define (pre-len xs) (is-map? xs))
    (define (op-len xs) (cons xs (length xs)))
    (define (post-len xs r) (equal? r (len xs)))
    *ENDLIBSPEC*/
    fn len(&self) -> usize {
        BTreeMap::len(self)
    }

    /*LIBSPEC*
    /*OPNAME*
    contains op-contains pre-contains post-contains
     *ENDOPNAME*/
    (define (pre-contains xs) (is-map? xs))
    (define (op-contains xs k) (assoc k xs))
    (define (post-contains xs k r) (equal? r (contains xs k)))
    *ENDLIBSPEC*/
    fn contains(&mut self, x: &K) -> bool {
        BTreeMap::contains_key(self, x)
    }

    /*LIBSPEC*
    /*OPNAME*
    insert op-insert pre-insert post-insert
     *ENDOPNAME*/
    (define (pre-insert xs) (is-map? xs))
    (define (op-insert xs k v)
      (let ([idx (index-where xs (lambda (p) (equal? k (car p))))])
           (cond [idx (list-set xs idx (cons k v))]
                 [else (list* (cons k v) xs)])))
    (define (post-insert xs k v r) (equal? r (op-insert xs k v)))
    *ENDLIBSPEC*/
    fn insert(&mut self, key: K, val: V) -> Option<V> {
        BTreeMap::insert(self, key, val)
    }

    /*LIBSPEC*
    /*OPNAME*
    get get pre-get post-get
     *ENDOPNAME*/
    (define (pre-get xs) (is-map? xs))
    (define (op-get xs k) (cdr (assoc k xs)))
    (define (post-get xs k r) (equal? r (op-get xs k)))
    *ENDLIBSPEC*/
    fn get(&self, key: &K) -> Option<&V> {
        BTreeMap::get(self, key)
    }

    /*LIBSPEC*
    /*OPNAME*
    remove remove pre-remove post-remove
     *ENDOPNAME*/
    (define (pre-remove xs) (is-map? xs))
    (define (op-remove xs k) (cdr (assoc k xs)))
    (define (post-remove xs k r) (equal? r (op-remove xs k)))
    *ENDLIBSPEC*/
    fn remove(&mut self, key: &K) -> Option<V> {
        BTreeMap::remove(self, key)
    }

    /*LIBSPEC*
    /*OPNAME*
    clear clear pre-clear post-clear
    *ENDOPNAME*/
    (define (pre-clear xs) (is-map? xs))
    (define (op-clear xs) null)
    (define (post-clear xs r) (equal? r (op-clear xs)))
    *ENDLIBSPEC*/
    fn clear(&mut self) {
        BTreeMap::clear(self)
    }

    fn iter<'a>(&'a self) -> impl Iterator<Item = (&'a K, &'a V)> + 'a
    where
        K: 'a,
        V: 'a,
    {
        BTreeMap::iter(self)
    }
}

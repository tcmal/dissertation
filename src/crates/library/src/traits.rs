//! Common traits for primrose container types

pub trait Container<T>: Default + IntoIterator<Item = T> + FromIterator<T> {
    /// Get the current number of elements
    fn len(&self) -> usize;

    /// Check if the container is empty
    fn is_empty(&self) -> bool;

    /// Check if the specified item is in the container
    fn contains(&self, x: &T) -> bool;

    /// Insert the given item
    fn insert(&mut self, elt: T);

    /// Remove the first occurence of the given item
    fn remove(&mut self, elt: T) -> Option<T>;

    /// Remove all elements from the container
    fn clear(&mut self);

    /// Iterate over all items in this container
    fn iter<'a>(&'a self) -> impl Iterator<Item = &'a T>
    where
        T: 'a;
}

pub trait Stack<T> {
    fn push(&mut self, elt: T);
    fn pop(&mut self) -> Option<T>;
}

pub trait Indexable<T> {
    fn first(&self) -> Option<&T>;
    fn last(&self) -> Option<&T>;
    fn nth(&self, n: usize) -> Option<&T>;
}

pub trait Mapping<K, V>: Default + IntoIterator<Item = (K, V)> + FromIterator<(K, V)> {
    /// Get the current number of elements
    fn len(&self) -> usize;

    /// Check if the container is empty
    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Check if the specified item is in the container
    fn contains(&mut self, x: &K) -> bool;

    /// Insert the given item
    fn insert(&mut self, key: K, val: V) -> Option<V>;

    /// Get the value for the given key
    fn get(&self, key: &K) -> Option<&V>;

    /// Remove the first occurence of the given item
    fn remove(&mut self, key: &K) -> Option<V>;

    /// Remove all elements from the container
    fn clear(&mut self);

    /// Iterate over the elements in this container
    fn iter<'a>(&'a self) -> impl Iterator<Item = (&'a K, &'a V)> + 'a
    where
        K: 'a,
        V: 'a;
}

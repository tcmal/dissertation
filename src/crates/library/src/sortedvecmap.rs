/*LIBSPEC-NAME*
rust-sortedvecmap-spec primrose_library::SortedVecMap
*ENDLIBSPEC-NAME*/

use crate::{traits::Mapping, vecmap::extract_key};
use std::{hash::Hash, mem::replace};

#[derive(Clone)]
pub struct SortedVecMap<K, V> {
    v: Vec<(K, V)>,
}

/*IMPL*
Mapping
*ENDIMPL*/
impl<K: Ord + Hash, V> Mapping<K, V> for SortedVecMap<K, V> {
    /*LIBSPEC*
    /*OPNAME*
    len op-len pre-len post-len
    *ENDOPNAME*/
    (define (pre-len xs) (is-map? xs))
    (define (op-len xs) (cons xs (length xs)))
    (define (post-len xs r) (equal? r (op-len xs)))
    *ENDLIBSPEC*/
    fn len(&self) -> usize {
        self.v.len()
    }

    /*LIBSPEC*
    /*OPNAME*
    contains op-contains pre-contains post-contains
     *ENDOPNAME*/
    (define (pre-contains xs) (is-map? xs))
    (define (op-contains xs k) (assoc k xs))
    (define (post-contains xs k r) (equal? r (op-contains xs k)))
    *ENDLIBSPEC*/
    fn contains(&mut self, key: &K) -> bool {
        self.v.binary_search_by_key(&key, extract_key).is_ok()
    }

    /*LIBSPEC*
    /*OPNAME*
    insert op-insert pre-insert post-insert
     *ENDOPNAME*/
    (define (pre-insert xs) (is-map? xs))
    (define (op-insert xs k v)
      (let ([idx (index-where xs (lambda (p) (equal? k (car p))))])
           (cond [idx (list-set xs idx (cons k v))]
                 [else (list* (cons k v) xs)])))
    (define (post-insert xs k v r) (equal? r (op-insert xs k v)))
    *ENDLIBSPEC*/
    fn insert(&mut self, key: K, val: V) -> Option<V> {
        match self.v.binary_search_by_key(&&key, extract_key) {
            Ok(idx) => Some(replace(&mut self.v[idx].1, val)),
            Err(idx) => {
                self.v.insert(idx, (key, val));
                None
            }
        }
    }

    /*LIBSPEC*
    /*OPNAME*
    get op-get pre-get post-get
     *ENDOPNAME*/
    (define (pre-get xs) (is-map? xs))
    (define (op-get xs k) (cdr (assoc k xs)))
    (define (post-get xs k r) (equal? r (op-get xs k)))
    *ENDLIBSPEC*/
    fn get(&self, key: &K) -> Option<&V> {
        Some(&self.v[self.v.binary_search_by_key(&key, extract_key).ok()?].1)
    }

    /*LIBSPEC*
    /*OPNAME*
    remove op-remove pre-remove post-remove
     *ENDOPNAME*/
    (define (pre-remove xs) (is-map? xs))
    (define (op-remove xs k) (cdr (assoc k xs)))
    (define (post-remove xs k r) (equal? r (op-remove xs k)))
    *ENDLIBSPEC*/
    fn remove(&mut self, key: &K) -> Option<V> {
        Some(
            self.v
                .remove(self.v.binary_search_by_key(&key, extract_key).ok()?)
                .1,
        )
    }

    /*LIBSPEC*
    /*OPNAME*
    clear op-clear pre-clear post-clear
    *ENDOPNAME*/
    (define (pre-clear xs) (is-map? xs))
    (define (op-clear xs) null)
    (define (post-clear xs r) (equal? r (op-clear xs)))
    *ENDLIBSPEC*/
    fn clear(&mut self) {
        self.v.clear()
    }

    fn iter<'a>(&'a self) -> impl Iterator<Item = (&'a K, &'a V)> + 'a
    where
        K: 'a,
        V: 'a,
    {
        self.v.iter().map(|(k, v)| (k, v))
    }
}

impl<K, V> Default for SortedVecMap<K, V> {
    fn default() -> Self {
        Self { v: Vec::default() }
    }
}

impl<K: Ord, V> FromIterator<(K, V)> for SortedVecMap<K, V> {
    fn from_iter<T: IntoIterator<Item = (K, V)>>(iter: T) -> Self {
        let mut v: Vec<(K, V)> = iter.into_iter().collect();
        v.sort_by(|(k1, _), (k2, _)| k1.cmp(k2));
        v.dedup_by(|(k1, _), (k2, _)| k1 == k2);
        Self { v }
    }
}

impl<K, V> IntoIterator for SortedVecMap<K, V> {
    type Item = (K, V);

    type IntoIter = std::vec::IntoIter<(K, V)>;

    fn into_iter(self) -> Self::IntoIter {
        self.v.into_iter()
    }
}

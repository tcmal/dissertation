/*LIBSPEC-NAME*
rust-vecset-spec primrose_library::VecSet
*ENDLIBSPEC-NAME*/

use crate::traits::{Container, Indexable};

use std::vec::Vec;

/// A set represented by a Vector, where items aren't necessarily sorted.
#[derive(Debug, Clone)]
pub struct VecSet<T> {
    v: Vec<T>,
}

impl<T: PartialEq + std::cmp::Ord> VecSet<T> {
    pub fn from_vec(mut v: Vec<T>) -> VecSet<T> {
        v.sort();
        v.dedup();
        VecSet { v }
    }

    pub fn new() -> VecSet<T> {
        VecSet { v: Vec::new() }
    }

    pub fn len(&self) -> usize {
        self.v.len()
    }

    pub fn contains(&self, x: &T) -> bool {
        self.v.contains(x)
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    // Duplicated elements will be discarded
    pub fn push(&mut self, value: T) {
        if !self.contains(&value) {
            self.v.push(value);
        }
    }

    pub fn pop(&mut self) -> Option<T> {
        self.v.pop()
    }

    pub fn remove(&mut self, index: usize) -> T {
        self.v.swap_remove(index)
    }

    pub fn clear(&mut self) {
        self.v.clear()
    }

    pub fn first(&self) -> Option<&T> {
        self.v.first()
    }

    pub fn last(&self) -> Option<&T> {
        self.v.last()
    }

    pub fn to_vec(self) -> Vec<T> {
        self.v
    }
}

/*IMPL*
Container
*ENDIMPL*/
impl<T: Ord + PartialEq> Container<T> for VecSet<T> {
    /*LIBSPEC*
    /*OPNAME*
    len op-len pre-len post-len
    *ENDOPNAME*/
    (define (op-len xs) (cons xs (length xs)))
    (define (pre-len xs) (equal? xs (remove-duplicates xs)))
    (define (post-len xs r) (equal? r (op-len xs)))
    *ENDLIBSPEC*/
    fn len(&self) -> usize {
        VecSet::len(self)
    }

    /*LIBSPEC*
    /*OPNAME*
    contains op-contains pre-contains post-contains
    *ENDOPNAME*/
    (define (op-contains xs x)
      (cond
        [(list? (member x xs)) (cons xs #t)]
        [else (cons xs #f)]))
    (define (pre-contains xs) (equal? xs (remove-duplicates xs)))
    (define (post-contains xs x r) (equal? r (op-contains xs x)))
    *ENDLIBSPEC*/
    fn contains(&self, x: &T) -> bool {
        VecSet::contains(self, x) // use fully qualified syntax to avoid function name collision
    }

    /*LIBSPEC*
    /*OPNAME*
    is-empty op-is-empty pre-is-empty post-is-empty
    *ENDOPNAME*/
    (define (op-is-empty xs) (cons xs (null? xs)))
    (define (pre-is-empty xs) (equal? xs (remove-duplicates xs)))
    (define (post-is-empty xs r) (equal? r (op-is-empty xs)))
    *ENDLIBSPEC*/
    fn is_empty(&self) -> bool {
        VecSet::is_empty(self)
    }

    /*LIBSPEC*
    /*OPNAME*
    clear op-clear pre-clear post-clear
    *ENDOPNAME*/
    (define (op-clear xs) null)
    (define (pre-clear xs) (equal? xs (remove-duplicates xs)))
    (define (post-clear xs r) (equal? r (op-clear xs)))
    *ENDLIBSPEC*/
    fn clear(&mut self) {
        VecSet::clear(self);
    }

    /*LIBSPEC*
    /*OPNAME*
    insert op-insert pre-insert post-insert
    *ENDOPNAME*/
    (define (op-insert xs x) (remove-duplicates (append xs (list x))))
    (define (pre-insert xs) (equal? xs (remove-duplicates xs)))
    (define (post-insert xs x ys) (equal? ys (op-insert xs x)))
    *ENDLIBSPEC*/
    fn insert(&mut self, elt: T) {
        VecSet::push(self, elt);
    }

    /*LIBSPEC*
    /*OPNAME*
    remove op-remove pre-remove post-remove
    *ENDOPNAME*/
    (define (op-remove xs x)
      (cond
        [(list? (member x xs)) (cons (remove x xs) x)]
        [else (cons xs null)]))
    (define (pre-remove xs) (equal? xs (remove-duplicates xs)))
    (define (post-remove xs r) (equal? r (op-remove xs)))
    *ENDLIBSPEC*/
    fn remove(&mut self, elt: T) -> Option<T> {
        let idx = self.iter().position(|x| *x == elt)?;
        Some(self.v.remove(idx))
    }

    fn iter<'a>(&'a self) -> impl Iterator<Item = &'a T>
    where
        T: 'a,
    {
        self.v.iter()
    }
}

/*IMPL*
Indexable
*ENDIMPL*/
impl<T: PartialEq + Ord> Indexable<T> for VecSet<T> {
    /*LIBSPEC*
    /*OPNAME*
    first op-first pre-first post-first
    *ENDOPNAME*/
    (define (op-first xs)
      (cond
        [(null? xs) (cons xs null)]
        [else (cons xs (first xs))]))
    (define (pre-first xs) #t)
    (define (post-first xs r) (equal? r (op-first xs)))
    *ENDLIBSPEC*/
    fn first(&self) -> Option<&T> {
        VecSet::first(self)
    }

    /*LIBSPEC*
    /*OPNAME*
    last op-last pre-last post-last
    *ENDOPNAME*/
    (define (op-last xs)
      (cond
        [(null? xs) (cons xs null)]
        [else (cons xs (last xs))]))
    (define (pre-last xs) #t)
    (define (post-last xs r) (equal? r (op-last xs)))
    *ENDLIBSPEC*/
    fn last(&self) -> Option<&T> {
        VecSet::last(self)
    }

    /*LIBSPEC*
    /*OPNAME*
    nth op-nth pre-nth post-nth
    *ENDOPNAME*/
    (define (op-nth xs n)
      (cond
        [(>= n (length xs)) (cons xs null)]
        [(< n 0) (cons xs null)]
        [else (cons xs (list-ref xs n))]))
    (define (pre-nth xs) #t)
    (define (post-nth xs n r) (equal? r (op-nth xs n)))
    *ENDLIBSPEC*/
    fn nth(&self, n: usize) -> Option<&T> {
        self.v.get(n)
    }
}

impl<T: Ord> Default for VecSet<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> IntoIterator for VecSet<T> {
    type Item = T;
    type IntoIter = std::vec::IntoIter<T>;

    fn into_iter(self) -> Self::IntoIter {
        self.v.into_iter()
    }
}

impl<E: Ord> FromIterator<E> for VecSet<E> {
    fn from_iter<T: IntoIterator<Item = E>>(iter: T) -> Self {
        let mut v = Vec::from_iter(iter);
        v.sort();
        v.dedup();
        Self { v }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::proptest::{strategies::vec_set, *};
    use im::conslist::ConsList;
    use proptest::prelude::*;

    fn abstraction<T>(v: VecSet<T>) -> ConsList<T>
    where
        T: PartialEq + Ord,
    {
        let list: ConsList<T> = ConsList::from(v.to_vec());
        list
    }

    proptest! {
        #![proptest_config(ProptestConfig {
            cases: 100, .. ProptestConfig::default()
          })]

        #[test]
        fn test_vec_set_len(ref mut v in vec_set(".*", 0..100)) {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            assert_eq!(Container::<String>::len(v), abs_list.len());
            assert_eq!(abstraction(v.clone()), abs_list);
        }

        #[test]
        fn test_vec_set_contains(ref mut v in vec_set(".*", 0..100), a in ".*") {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            assert_eq!(Container::<String>::contains(v, &a), contains(&abs_list, &a));
            assert_eq!(abstraction(v.clone()), abs_list);
        }

        #[test]
        fn test_vec_set_is_empty(ref mut v in vec_set(".*", 0..100)) {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            assert_eq!(Container::<String>::is_empty(v), abs_list.is_empty());
            assert_eq!(abstraction(v.clone()), abs_list);
        }

        #[test]
        fn test_vec_set_insert(ref mut v in vec_set(0_usize..100, 0..100), a in 0_usize..100) {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            let after_list = unique(&abs_list.append(conslist![a.clone()]));
            Container::<usize>::insert(v, a.clone());
            assert_eq!(abstraction(v.clone()), after_list);
        }

        #[test]
        fn test_vec_set_clear(ref mut v in vec_set(".*", 0..100)) {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            let after_list = clear(&abs_list);
            Container::<String>::clear(v);
            assert_eq!(abstraction(v.clone()), after_list);
        }

        #[test]
        fn test_vec_set_remove(ref mut v in vec_set(".*", 0..100), a in ".*") {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            let (after_list, abs_elem) = remove(&abs_list, a.clone());
            let elem = Container::<String>::remove(v, a.clone());
            assert_eq!(abstraction(v.clone()), after_list);
            assert_eq!(elem, abs_elem);
        }

        #[test]
        fn test_vec_set_first(ref mut v in vec_set(".*", 0..100)) {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            let elem = Indexable::<String>::first(v);
            let abs_first = first(&abs_list);
            assert_eq!(elem, abs_first);
            assert_eq!(abstraction(v.clone()), abs_list);
        }

        #[test]
        fn test_vec_set_last(ref mut v in vec_set(".*", 0..100)) {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            let elem = Indexable::<String>::last(v);
            let abs_last = last(&abs_list);
            assert_eq!(elem, abs_last);
            assert_eq!(abstraction(v.clone()), abs_list);
        }

        #[test]
        fn test_vec_set_nth(ref mut v in vec_set(".*", 0..100), n in 0usize..100) {
            let abs_list = abstraction(v.clone());
            //pre
            assert_eq!(abs_list, unique(&abs_list));
            //post
            let elem = Indexable::<String>::nth(v, n);
            let abs_nth = nth(&abs_list, n);
            assert_eq!(elem, abs_nth);
            assert_eq!(abstraction(v.clone()), abs_list);
        }
    }
}

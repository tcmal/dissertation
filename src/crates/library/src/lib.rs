//! Container implementations that primrose can select from, and traits.

#![feature(linked_list_cursors)]

pub mod traits;

mod profiler;
pub use profiler::{MappingProfilerWrapper, ProfilerWrapper};

mod adaptive;
pub use adaptive::AdaptiveContainer;

mod btreemap;
mod hashmap;
mod hashset;
mod list;
mod treeset;
mod vector;

mod vecset;
pub use vecset::VecSet;

mod vecmap;
pub use vecmap::VecMap;

mod sortedvec;
pub use sortedvec::SortedVec;

mod sortedvecset;
pub use sortedvecset::SortedVecSet;

mod sortedvecmap;
pub use sortedvecmap::SortedVecMap;

#[cfg(test)]
pub mod proptest;

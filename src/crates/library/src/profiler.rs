use std::{
    cell::Cell,
    env::var,
    fs::{create_dir, metadata, File},
    io::Write,
    marker::PhantomData,
    time::SystemTime,
};

use crate::traits::{Container, Indexable, Mapping, Stack};

pub struct ProfilerWrapper<const ID: usize, T, E> {
    inner: Option<T>,
    max_n: Cell<usize>,
    n_contains: Cell<usize>,
    n_insert: usize,
    n_clear: usize,
    n_remove: usize,
    n_first: Cell<usize>,
    n_last: Cell<usize>,
    n_nth: Cell<usize>,
    n_push: usize,
    n_pop: usize,
    n_get: Cell<usize>,
    _d: PhantomData<E>,
}

impl<const ID: usize, T: Default, E> Default for ProfilerWrapper<ID, T, E> {
    fn default() -> Self {
        Self {
            inner: Some(T::default()),
            max_n: Cell::new(0),
            n_contains: Cell::new(0),
            n_insert: 0,
            n_clear: 0,
            n_remove: 0,
            n_first: Cell::new(0),
            n_last: Cell::new(0),
            n_nth: Cell::new(0),
            n_push: 0,
            n_pop: 0,
            n_get: Cell::new(0),
            _d: Default::default(),
        }
    }
}

impl<const ID: usize, T: Container<E>, E> ProfilerWrapper<ID, T, E> {
    fn add_n(&self) {
        self.max_n
            .set(self.max_n.get().max(self.inner.as_ref().unwrap().len()));
    }
}

impl<const ID: usize, T: Container<E>, E> Container<E> for ProfilerWrapper<ID, T, E> {
    fn len(&self) -> usize {
        self.inner.as_ref().unwrap().len()
    }

    fn contains(&self, x: &E) -> bool {
        self.add_n();
        self.n_contains.set(self.n_contains.get() + 1);
        self.inner.as_ref().unwrap().contains(x)
    }

    fn is_empty(&self) -> bool {
        self.inner.as_ref().unwrap().is_empty()
    }

    fn insert(&mut self, elt: E) {
        self.add_n();
        self.n_insert += 1;
        self.inner.as_mut().unwrap().insert(elt)
    }

    fn clear(&mut self) {
        self.add_n();
        self.n_clear += 1;
        self.inner.as_mut().unwrap().clear()
    }

    fn remove(&mut self, elt: E) -> Option<E> {
        self.add_n();
        self.n_remove += 1;
        self.inner.as_mut().unwrap().remove(elt)
    }

    fn iter<'a>(&'a self) -> impl Iterator<Item = &'a E>
    where
        E: 'a,
    {
        self.inner.as_ref().unwrap().iter()
    }
}

impl<const ID: usize, T: Indexable<E> + Container<E>, E> Indexable<E>
    for ProfilerWrapper<ID, T, E>
{
    fn first(&self) -> Option<&E> {
        self.add_n();
        self.n_first.set(self.n_first.get() + 1);
        self.inner.as_ref().unwrap().first()
    }

    fn last(&self) -> Option<&E> {
        self.add_n();
        self.n_last.set(self.n_last.get() + 1);
        self.inner.as_ref().unwrap().last()
    }

    fn nth(&self, n: usize) -> Option<&E> {
        self.add_n();
        self.n_nth.set(self.n_nth.get() + 1);
        self.inner.as_ref().unwrap().nth(n)
    }
}

impl<const ID: usize, T: Stack<E> + Container<E>, E> Stack<E> for ProfilerWrapper<ID, T, E> {
    fn push(&mut self, elt: E) {
        self.add_n();
        self.n_push += 1;
        self.inner.as_mut().unwrap().push(elt)
    }

    fn pop(&mut self) -> Option<E> {
        self.add_n();
        self.n_pop += 1;
        self.inner.as_mut().unwrap().pop()
    }
}

impl<const ID: usize, T, E> Drop for ProfilerWrapper<ID, T, E> {
    fn drop(&mut self) {
        let base_dir =
            var("PROFILER_OUT_DIR").expect("ProfilerWrapper used without environment variable");
        let unix_time = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap();
        let dir = format!("{}/{}", base_dir, ID);
        if metadata(&dir).is_err() {
            create_dir(&dir).unwrap();
        }
        let mut f = File::create(format!("{}/{}", dir, unix_time.as_nanos())).unwrap();

        writeln!(f, "{}", self.max_n.get()).unwrap();
        writeln!(f, "{}", self.n_contains.get()).unwrap();
        writeln!(f, "{}", self.n_insert).unwrap();
        writeln!(f, "{}", self.n_clear).unwrap();
        writeln!(f, "{}", self.n_remove).unwrap();
        writeln!(f, "{}", self.n_first.get()).unwrap();
        writeln!(f, "{}", self.n_last.get()).unwrap();
        writeln!(f, "{}", self.n_nth.get()).unwrap();
        writeln!(f, "{}", self.n_push).unwrap();
        writeln!(f, "{}", self.n_pop).unwrap();
        writeln!(f, "{}", self.n_get.get()).unwrap();
    }
}

impl<const ID: usize, T: IntoIterator, E> IntoIterator for ProfilerWrapper<ID, T, E> {
    type Item = T::Item;

    type IntoIter = T::IntoIter;

    fn into_iter(mut self) -> Self::IntoIter {
        self.inner.take().unwrap().into_iter()
    }
}

impl<const ID: usize, C: FromIterator<E> + Container<E>, E> FromIterator<E>
    for ProfilerWrapper<ID, C, E>
{
    fn from_iter<T: IntoIterator<Item = E>>(iter: T) -> Self {
        let c = C::from_iter(iter);
        Self {
            max_n: Cell::new(c.len()),
            inner: Some(c),
            n_contains: Cell::new(0),
            n_insert: 0,
            n_clear: 0,
            n_remove: 0,
            n_first: Cell::new(0),
            n_last: Cell::new(0),
            n_nth: Cell::new(0),
            n_push: 0,
            n_pop: 0,
            n_get: Cell::new(0),
            _d: PhantomData,
        }
    }
}

pub struct MappingProfilerWrapper<const ID: usize, T, E> {
    inner: Option<T>,
    max_n: Cell<usize>,
    n_contains: usize,
    n_insert: usize,
    n_clear: usize,
    n_remove: usize,
    n_first: Cell<usize>,
    n_last: Cell<usize>,
    n_nth: Cell<usize>,
    n_push: usize,
    n_pop: usize,
    n_get: Cell<usize>,
    _d: PhantomData<E>,
}

impl<const ID: usize, T: Default, E> Default for MappingProfilerWrapper<ID, T, E> {
    fn default() -> Self {
        Self {
            inner: Some(T::default()),
            max_n: Cell::new(0),
            n_contains: 0,
            n_insert: 0,
            n_clear: 0,
            n_remove: 0,
            n_first: Cell::new(0),
            n_last: Cell::new(0),
            n_nth: Cell::new(0),
            n_push: 0,
            n_pop: 0,
            n_get: Cell::new(0),
            _d: Default::default(),
        }
    }
}

impl<const ID: usize, T: Mapping<K, V>, K, V> MappingProfilerWrapper<ID, T, (K, V)> {
    fn add_n(&self) {
        self.max_n
            .set(self.max_n.get().max(self.inner.as_ref().unwrap().len()));
    }
}

impl<const ID: usize, T: Mapping<K, V>, K, V> Mapping<K, V>
    for MappingProfilerWrapper<ID, T, (K, V)>
{
    fn len(&self) -> usize {
        self.inner.as_ref().unwrap().len()
    }

    fn contains(&mut self, x: &K) -> bool {
        self.add_n();
        self.n_contains += 1;
        self.inner.as_mut().unwrap().contains(x)
    }

    fn insert(&mut self, key: K, val: V) -> Option<V> {
        self.add_n();
        self.n_insert += 1;
        self.inner.as_mut().unwrap().insert(key, val)
    }

    fn get(&self, key: &K) -> Option<&V> {
        self.add_n();
        self.n_get.set(self.n_get.get() + 1);
        self.inner.as_ref().unwrap().get(key)
    }

    fn remove(&mut self, key: &K) -> Option<V> {
        self.add_n();
        self.n_remove += 1;
        self.inner.as_mut().unwrap().remove(key)
    }

    fn clear(&mut self) {
        self.add_n();
        self.n_clear += 1;
        self.inner.as_mut().unwrap().clear()
    }

    fn iter<'a>(&'a self) -> impl Iterator<Item = (&'a K, &'a V)> + 'a
    where
        K: 'a,
        V: 'a,
    {
        self.inner.as_ref().unwrap().iter()
    }
}

impl<const ID: usize, T, E> Drop for MappingProfilerWrapper<ID, T, E> {
    fn drop(&mut self) {
        let base_dir = var("PROFILER_OUT_DIR")
            .expect("MappingProfilerWrapper used without environment variable");
        let unix_time = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap();
        let dir = format!("{}/{}", base_dir, ID);
        if metadata(&dir).is_err() {
            create_dir(&dir).unwrap();
        }
        let mut f = File::create(format!("{}/{}", dir, unix_time.as_nanos())).unwrap();

        writeln!(f, "{}", self.max_n.get()).unwrap();
        writeln!(f, "{}", self.n_contains).unwrap();
        writeln!(f, "{}", self.n_insert).unwrap();
        writeln!(f, "{}", self.n_clear).unwrap();
        writeln!(f, "{}", self.n_remove).unwrap();
        writeln!(f, "{}", self.n_first.get()).unwrap();
        writeln!(f, "{}", self.n_last.get()).unwrap();
        writeln!(f, "{}", self.n_nth.get()).unwrap();
        writeln!(f, "{}", self.n_push).unwrap();
        writeln!(f, "{}", self.n_pop).unwrap();
        writeln!(f, "{}", self.n_get.get()).unwrap();
    }
}

impl<const ID: usize, T: IntoIterator, E> IntoIterator for MappingProfilerWrapper<ID, T, E> {
    type Item = T::Item;

    type IntoIter = T::IntoIter;

    fn into_iter(mut self) -> Self::IntoIter {
        self.inner.take().unwrap().into_iter()
    }
}

impl<const ID: usize, C: Mapping<K, V>, K, V> FromIterator<(K, V)>
    for MappingProfilerWrapper<ID, C, (K, V)>
{
    fn from_iter<T: IntoIterator<Item = (K, V)>>(iter: T) -> Self {
        let c = C::from_iter(iter);
        Self {
            max_n: Cell::new(c.len()),
            inner: Some(c),
            n_contains: 0,
            n_insert: 0,
            n_clear: 0,
            n_remove: 0,
            n_first: Cell::new(0),
            n_last: Cell::new(0),
            n_nth: Cell::new(0),
            n_push: 0,
            n_pop: 0,
            n_get: Cell::new(0),
            _d: PhantomData,
        }
    }
}

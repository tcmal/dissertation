extern crate peg;
use peg::parser;

use std::iter::FromIterator;
use std::vec::Vec;

use crate::types::{Bounds, Name, Type, TypeVar};

pub type Id = String;

pub type Literal = String;

#[derive(Clone, Debug)]
pub enum Refinement {
    Prop(Term),
    AndProps(Box<Refinement>, Box<Refinement>),
}

#[derive(Clone, Debug)]
pub enum Term {
    Lit(Literal),
    Var(Id),
    Lambda((Id, Bounds), Box<Term>),
    App(Box<Term>, Box<Term>),
}

impl Term {
    pub fn is_quantifier(&self) -> bool {
        match self {
            Term::Var(id) => id.to_string().eq("forall"),
            _ => false,
        }
    }

    pub fn require_cdr(&self) -> bool {
        matches!(self, Term::Var(id) if id == "op-pop"
                || id == "op-len"
                || id == "op-contains"
                || id == "op-is-empty"
                || id == "op-remove"
                || id == "op-first"
                || id == "op-last"
                || id == "op-nth"
                || id == "op-get")
    }
}

impl ToString for Term {
    fn to_string(&self) -> String {
        match self {
            Term::Lit(l) => l.to_string(),
            Term::Var(id) => id.to_string(),
            Term::Lambda((id, _bounds), _t) => id.to_string(),
            Term::App(t1, t2) => t1.to_string() + &t2.to_string(),
        }
    }
}

#[derive(Clone, Debug)]
pub enum Decl {
    PropertyDecl(bool, (Id, Vec<Type>), Box<Term>),
    ConTypeDecl(Type, (Id, Bounds, Refinement)),
}

impl Decl {
    pub fn is_prop_decl(&self) -> bool {
        matches!(self, Decl::PropertyDecl(_, _, _))
    }

    pub fn is_contype_decl(&self) -> bool {
        matches!(self, Decl::ConTypeDecl(_, _))
    }

    pub fn get_name(&self) -> String {
        match self {
            Decl::ConTypeDecl(con_ty, _) => {
                let (con, _) = con_ty.get_con_elem().unwrap();
                con
            }
            Decl::PropertyDecl(_, (id, _), _) => id.to_string(),
        }
    }
}

pub type Spec = Vec<Decl>;
pub type Code = String;

#[derive(Clone, Debug)]
pub enum Block {
    SpecBlock(Box<Spec>, usize),
    CodeBlock(Box<Code>, usize),
}

impl Block {
    pub fn is_spec_block(&self) -> bool {
        matches!(self, Block::SpecBlock(_, _))
    }

    pub fn is_code_block(&self) -> bool {
        matches!(self, Block::CodeBlock(_, _))
    }

    pub fn extract_spec(&self) -> Spec {
        match self {
            Block::SpecBlock(spec, _) => spec.to_vec(),
            _ => Vec::new(),
        }
    }
}

pub type Prog = Vec<Block>;

parser! {
pub grammar spec() for str {
    pub rule id() -> Id
        = s:$(!keyword() ([ 'a'..='z' | 'A'..='Z' | '_' ]['a'..='z' | 'A'..='Z' | '0'..='9' | '_' | '-' | '?' ]*))
        { s.into() }

    pub rule name() -> Name
        = s:$(!keyword() ([ 'a'..='z' | 'A'..='Z' | '_' ]['a'..='z' | 'A'..='Z' | '0'..='9' ]*))
        { s.into() }

    pub rule keyword() -> ()
        = ("crate" / "super" / "self" / "Self" / "const" / "mut" / "true" / "false" / "pub" / "in" / "from" / "with"
            / "f32"/ "i32" / "u32" / "bool" / "let" / "if" / "else" / "for" / "while" / "fn" / "do")
            ![ 'a'..='z' | 'A'..='Z' | '0'..='9' | '_' | '-' | '?' ]

    pub rule literal() -> Literal
        = s:$("true" / "false")
        { s.into() }

    pub rule ty() -> Type
        = precedence! {
            n:name() "<" _ t:( ty() ** "," ) _ ">"
            { Type::Con(n, t, Bounds::from(["Container".to_string()])) }
            --
            n:name()
            { Type::Var(TypeVar::new(n)) }
        }

    pub rule term() -> Term
        = precedence!{
            lit: literal() { Term::Lit(lit) }
            --
            v:id() { Term::Var(v) }
            --
            "\\" v:id() _ "->" _ t:term() { Term::Lambda((v, std::collections::HashSet::default()), Box::new(t)) }
            --
            "\\" v:id() _ "<:" _ "(" _ b:bounds() _ ")" _ "->" _ t:term() { Term::Lambda((v, b), Box::new(t)) }
            --
            "(" _ t1:term() __ t2:term() _ ")" { Term::App(Box::new(t1), Box::new(t2)) }
        }

    pub rule refinement() -> Refinement
        = precedence!{
            t:term() { Refinement::Prop(t) }
            --
            "(" _ p1:refinement() __ "and" __ p2:refinement() _ ")" { Refinement::AndProps(Box::new(p1), Box::new(p2)) }
        }

    pub rule bounds() -> Bounds
        = l: ((_ n:name() _ {n}) ++ "," ) { Bounds::from_iter(l.iter().cloned()) }

    pub rule decl() -> Decl
        = precedence! {
            _ "property" __ p:id() _ "<" _ tys:( ty:ty() ** "," ) _ ">" _ "{" _ t:term() _ "}" _
            {
                Decl::PropertyDecl(false, (p, tys), Box::new(t))
            }
            --
            _ "dictProperty" __ p:id() _ "<" _ tys:( ty:ty() ** "," ) _ ">" _ "{" _ t:term() _ "}" _
            {
                Decl::PropertyDecl(true, (p, tys), Box::new(t))
            }
            --
            _ "type" __ ty:ty() _ "=" _ "{" _ c:id() _ "impl" __ "(" _ b:bounds() _ ")" _ "|" _ t:refinement() _ "}" _
            {
                Decl::ConTypeDecl(ty, (c, b, t))
            }
        }

    pub rule spec() -> Spec
        = _ "/*SPEC*" _ decls: (d:decl() { d }) ** _ _ "*ENDSPEC*/" _
        {
            decls
        }

    pub rule code() -> Code
        = _ "/*CODE*/" c:$((!"/*ENDCODE*/"!"/*SPEC*"!"*ENDSPEC*/"[_])*) "/*ENDCODE*/" _ { c.into() }

    pub rule block() -> Block
        = precedence! {
            _ p:position!() s:spec() _
            {
                Block::SpecBlock(Box::new(s), p)
            }
            --
            _ p:position!() c:code() _
            {
                Block::CodeBlock(Box::new(c), p)
            }
        }

    pub rule prog() -> Prog
        = _ blocks: (b:block() { b }) ** _ _
        {
            blocks
        }

    rule _ = quiet!{[' ' | '\n' | '\t']*}
    rule __ = quiet!{[' ' | '\n' | '\t']+}

}}

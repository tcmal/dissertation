use std::io;

use thiserror::Error;

use crate::type_check::TypeError;

/// The main error type for primrose
#[derive(Error, Debug)]
pub enum Error {
    #[error("error reading input: {0}")]
    InputRead(io::Error),

    #[error("error parsing input: {0}")]
    ParseError(#[from] peg::error::ParseError<peg::str::LineCol>),

    #[error("type error: {0}")]
    TypeError(#[from] TypeError),

    #[error("library specification error: {0}")]
    LibraryError(String),

    #[error("analyser error: {0}")]
    AnalyserError(String),

    #[error("error generating match script: {0}")]
    GenMatchScript(io::Error),

    #[error("error executing solver: {0}")]
    ExecutionError(String),

    #[error("Unable to find a struct which matches the specification in the library")]
    NoMatchingStructInLibrary,

    #[error("Error, cannot obtain provided operations from the library specification")]
    ProvidedOperationsNotInLibrarySpec,
}

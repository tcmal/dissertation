use std::collections::HashMap;

pub type InforMap = HashMap<TagId, Tag>;
pub type TagId = String;

pub type Description = String;
type ElemTypeName = String;
type ConName = String;
type BoundName = String;

#[derive(Eq, PartialEq, Clone, Debug)]
pub enum Tag {
    /// Links to a property by name
    Prop(Description),
    /// Places a bound on a container type.
    Bound((ConName, Vec<ElemTypeName>), Vec<Description>),
    /// Defines a container type
    Con(
        /// The name of the type variable used for the element type
        Vec<ElemTypeName>,
        /// The name of the bound
        BoundName,
        /// Bounds placed on the type
        Vec<Tag>,
    ),
}

impl Tag {
    pub fn is_prop_tag(&self) -> bool {
        matches!(self, Tag::Prop(..))
    }

    pub fn is_bound_tag(&self) -> bool {
        matches!(self, Tag::Bound(..))
    }

    pub fn is_con_tag(&self) -> bool {
        matches!(self, Tag::Con(..))
    }

    pub fn extract_prop_desc(&self) -> Description {
        match self {
            Tag::Prop(desc) => desc.to_string(),
            _ => String::new(),
        }
    }

    pub fn extract_bound_descs(&self) -> Vec<Description> {
        match self {
            Tag::Bound(_, descs) => descs.to_vec(),
            _ => Vec::new(),
        }
    }
}

//! Useful tools for benchmarking & selection

use rand::rngs::StdRng;
use rand::seq::SliceRandom;

use rand::SeedableRng;

use std::collections::HashMap;
use std::mem::size_of;
use std::{hash::Hash, vec::Vec};

pub fn gen_dataset_1() -> Vec<u32> {
    let size = 1024 * 1024; // 1 MB
    let amount = size / size_of::<u32>();
    let mut data: Vec<u32> = (1..amount as u32).collect();
    let mut rng = StdRng::seed_from_u64(222);
    data.shuffle(&mut rng);
    data
}

pub fn gen_dataset_128() -> Vec<u32> {
    let size = 128 * 1024 * 1024; // 128 MB
    let amount = size / size_of::<u32>();
    let mut data: Vec<u32> = (1..amount as u32).collect();
    let mut rng = StdRng::seed_from_u64(222);
    data.shuffle(&mut rng);
    data
}

pub fn gen_dataset_256() -> Vec<u32> {
    let size = 256 * 1024 * 1024; // 256 MB
    let amount = size / size_of::<u32>();
    let mut data: Vec<u32> = (1..amount as u32).collect();
    let mut rng = StdRng::seed_from_u64(222);
    data.shuffle(&mut rng);
    data
}

pub fn gen_dataset_512() -> Vec<u32> {
    let size = 512 * 1024 * 1024; // 512 MB
    let amount = size / size_of::<u32>();
    let mut data: Vec<u32> = (1..amount as u32).collect();
    let mut rng = StdRng::seed_from_u64(222);
    data.shuffle(&mut rng);
    data
}

/// Get the cartesian product of all of the values in set.
/// Returns a list of every possible combination created by picking a value from the matching key in `bins`.
pub fn nary_cartesian_product<K: Hash + Eq, V>(bins: &HashMap<K, Vec<V>>) -> Vec<HashMap<&K, &V>> {
    let bins = bins.iter().collect::<Vec<_>>();
    let mut indices = vec![0; bins.len()];
    let lengths = bins.iter().map(|(_, v)| v.len()).collect::<Vec<_>>();
    let mut outputs = Vec::with_capacity(lengths.iter().product());
    loop {
        let code = indices
            .iter()
            .enumerate()
            // get candidate we're using for each type
            .map(|(key_idx, val_idx)| (bins[key_idx].0, &bins[key_idx].1[*val_idx]))
            .collect::<HashMap<_, _>>();

        outputs.push(code);

        if inc_carrying(&mut indices, &lengths) {
            break;
        }
    }

    outputs
}

/// Increment the left hand side of indices, carrying as needed if any entries reach the
/// the value defined in `bounds`.
/// `max` and `indices` must be the same length.
/// Returns true if carried outside the list, otherwise false.
pub fn inc_carrying(indices: &mut [usize], bounds: &[usize]) -> bool {
    for i in 0..indices.len() {
        if indices[i] < bounds[i] - 1 {
            indices[i] += 1;
            return false;
        } else {
            indices[i] = 0;
            // carry to next bit
        }
    }

    true
}

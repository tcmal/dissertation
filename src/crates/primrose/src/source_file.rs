use std::{
    fs,
    io::{self},
    path::Path,
};

pub const CODE: &str = "/*CODE*/";
pub const CODEEND: &str = "/*ENDCODE*/";

pub const SPEC: &str = "/*SPEC*";
pub const SPECEND: &str = "*ENDSPEC*/";

pub fn read_src(filename: &Path) -> Result<String, io::Error> {
    let contents = fs::read_to_string(filename)?;
    Ok(preprocess_src(contents))
}

/// Mark all parts of the code so everything is between either CODE and CODEEND
/// or SPEC and SPECEND
pub fn preprocess_src(src: String) -> String {
    let mut trimed_src = src.trim();
    let mut result = String::new();
    while !trimed_src.is_empty() {
        match trimed_src.find(SPEC) {
            Some(n) => match trimed_src.find(SPECEND) {
                Some(m) => {
                    if n > 0 {
                        let code = &trimed_src[..n];
                        result = result + CODE + code + CODEEND;
                    }
                    let spec = &trimed_src[n..(m + SPECEND.len())];
                    trimed_src = &trimed_src[(m + SPECEND.len())..].trim();
                    result += spec;
                }
                None => {
                    result = result + CODE + trimed_src + CODEEND;
                    break;
                }
            },
            None => {
                result = result + CODE + trimed_src + CODEEND;
                break;
            }
        }
    }
    result
}

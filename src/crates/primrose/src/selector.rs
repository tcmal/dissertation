use std::{
    collections::HashMap,
    fs,
    io::{self, Write},
    path::Path,
};

use log::{debug, info, trace};

const MATCHSCRIPT: &str = "./racket_specs/gen_match/match-script.rkt";
const OPS: &str = "./racket_specs/gen_lib_spec/ops.rkt";

use crate::{
    analysis::Analyser,
    codegen::process_bound_decl,
    description::{Description, Tag, TagId},
    error::Error,
    library_specs::LibSpec,
    parser::{spec, Block},
    run_matching::{
        cleanup_script, gen_match_script, initialise_match_setup, run_matching, setup_dirs,
        LANGDECL,
    },
    source_file::read_src,
    spec_map::{LibSpecs, MatchSetup, ProvidedOps},
    type_check::TypeChecker,
};

/// Selects containers for a specific file.
/// Creating this requires doing some analysis on the file, so is relatively costly.
pub struct ContainerSelector {
    /// Analysis of the source file
    pub(crate) analyser: Analyser,

    pub(crate) blocks: Vec<Block>,
    pub(crate) bounds_decl: String,
    pub(crate) match_setup: MatchSetup,
    pub(crate) lib_specs: LibSpecs,
}

impl ContainerSelector {
    /// Load the file at the given path, perform analysis, and return a selector.
    pub fn from_path(path: &Path, lib_path: &Path, model_size: usize) -> Result<Self, Error> {
        Self::from_src(
            &read_src(path).map_err(Error::InputRead)?,
            lib_path,
            model_size,
        )
    }

    /// Analyse the given source and return a selector for it.
    pub fn from_src(src: &str, lib_path: &Path, model_size: usize) -> Result<Self, Error> {
        debug!("Setting up directories");
        setup_dirs();

        debug!("Parsing into blocks");
        let blocks = spec::prog(src)?;

        debug!("Running type checker");
        let mut tc = TypeChecker::new();
        tc.check_prog(blocks.clone())?;
        trace!("Results of type checking: {:#?}", &tc);

        debug!("Running analysis");
        let mut analyser = Analyser::new();
        analyser
            .analyse_prog(blocks.clone(), model_size)
            .map_err(Error::AnalyserError)?;
        trace!("Results of analysis: {:#?}", &analyser);

        Self::new(analyser, blocks, lib_path)
    }

    /// Create a new selector using the given analysis
    fn new(analyser: Analyser, blocks: Vec<Block>, lib_path: &Path) -> Result<Self, Error> {
        Ok(Self {
            blocks,
            bounds_decl: process_bound_decl(analyser.ctx()),
            match_setup: initialise_match_setup(),
            lib_specs: LibSpec::process_all(lib_path).map_err(Error::LibraryError)?,
            analyser,
        })
    }

    /// Get all container tags in this context
    pub fn container_tags(&self) -> impl Iterator<Item = &String> {
        self.analyser
            .ctx()
            .iter()
            .filter(|(_k, v)| v.is_con_tag())
            .map(|(k, _)| k)
    }

    /// Find candidates for all container tags in this context.
    /// Returns a map from tag name to list of candidates.
    pub fn find_all_candidates(&self) -> Result<HashMap<&TagId, Vec<String>>, Error> {
        let mut candidates = HashMap::new();
        for tag in self.container_tags() {
            let found = self.find_candidates(tag)?;

            candidates.insert(tag, found);
        }

        Ok(candidates)
    }

    /// Find candidate container types for a given Tag in this context.
    /// Panics if tag_id is not a key, or is not the correct type of tag
    pub fn find_candidates(&self, tag_id: &String) -> Result<Vec<String>, Error> {
        let tag = self.analyser.ctx().get(tag_id).expect("invalid tag_id");

        let Tag::Con(_elem_ty, _i_name, tags) = tag else {
            panic!("tag_id was not Tag::Con");
        };

        info!("Finding container types for tag {}", tag_id);
        let prop_descs: Vec<Description> = tags
            .iter()
            .filter(|t| t.is_prop_tag())
            .map(|t| t.extract_prop_desc())
            .collect();

        let mut bounds: Vec<Description> = tags
            .iter()
            .filter(|t| t.is_bound_tag())
            .flat_map(|t| t.extract_bound_descs())
            .collect();
        bounds.sort();

        let mut structs = Vec::new();

        // select library structs implement bounds decl in contype
        let lib_spec_impls = self.lib_specs.iter().filter(|(_name, spec)| {
            bounds.iter().all(|i| {
                spec.interface_provide_map
                    .keys()
                    .cloned()
                    .collect::<String>()
                    .contains(i)
            })
        });

        for (name, spec) in lib_spec_impls {
            debug!("{} - ...", name);
            match write_provided_ops(&spec.provided_ops) {
                Ok(_) => {}
                Err(_) => {
                    return Err(Error::ProvidedOperationsNotInLibrarySpec);
                }
            }
            let mut is_match = true;
            for p in prop_descs.iter() {
                for i in bounds.iter() {
                    let (prop_file, symbolics) =
                        self.analyser.prop_specs().get(p).expect(
                            &("Error: No property specification found for: ".to_string() + &p),
                        );
                    debug!("Checking bound {i}...");
                    gen_match_script(
                        p,
                        self.match_setup.get(i).unwrap(),
                        prop_file,
                        &spec.spec_name,
                        spec.interface_provide_map.get(i).unwrap(),
                        symbolics,
                    )
                    .map_err(Error::GenMatchScript)?;

                    // true - match; false - not match
                    is_match &=
                        run_matching(MATCHSCRIPT.to_string()).map_err(Error::ExecutionError)?;
                    if !is_match {
                        break;
                    }
                }
                if !is_match {
                    break;
                }
            }
            if is_match {
                debug!("{} - YAY", name);
                structs.push(name.to_string());
            } else {
                debug!("{} - NAY", name);
            }
        }

        cleanup_script();
        Ok(structs)
    }
}

/// Write the provided operation specifications from a library spec to the correct path ([`self::OPS`]).
fn write_provided_ops(provided_ops: &ProvidedOps) -> Result<(), io::Error> {
    let ops_path = OPS;
    let (code, ops) = provided_ops;
    let mut output = fs::File::create(ops_path)?;
    write!(output, "{}", LANGDECL)?;
    for item in code.iter() {
        write!(output, "{}", item)?;
    }
    let ops_string = ops.join(" ");
    let provide = "\n(provide ".to_string() + &ops_string + ")";
    write!(output, "{}", provide)?;
    Ok(())
}

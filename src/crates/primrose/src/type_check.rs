//! Performs type checking for specifications
use thiserror::Error;

use crate::inference::TypeEnv;
use crate::parser::{Decl, Prog, Refinement, Spec};
use crate::types::{Bounds, Type, TypeScheme, TypeVar, TypeVarGen};

use std::ops::Deref;

#[derive(Error, Debug)]
#[error("{0}")]
pub struct TypeError(String);

#[derive(Debug)]
pub struct TypeChecker {
    global_ctx: TypeEnv,
    tvg: TypeVarGen,
}

impl TypeChecker {
    /// Create a new type checking context.
    pub fn new() -> TypeChecker {
        let mut tc = TypeChecker {
            global_ctx: TypeEnv::new(),
            tvg: TypeVarGen::new(),
        };
        tc.predefined();

        tc
    }

    /// Add types for pre-defined functions
    fn predefined(&mut self) {
        // put for_all_unique_pair into context
        let binary_fn1 = Type::Fun(
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
            Box::new(Type::Fun(
                Box::new(Type::Var(TypeVar::new("T".to_string()))),
                Box::new(Type::Bool()),
            )),
        );
        self.global_ctx.insert(
            "for-all-unique-pairs".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: Type::Fun(
                    Box::new(Type::Con(
                        "Con".to_string(),
                        vec![Type::Var(TypeVar::new("T".to_string()))],
                        Bounds::from(["Container".to_string()]),
                    )),
                    Box::new(Type::Fun(Box::new(binary_fn1), Box::new(Type::Bool()))),
                ),
            },
        );

        // put for_all_unique_pair into context
        let binary_fn2 = Type::Fun(
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
            Box::new(Type::Fun(
                Box::new(Type::Var(TypeVar::new("T".to_string()))),
                Box::new(Type::Bool()),
            )),
        );
        self.global_ctx.insert(
            "for-all-consecutive-pairs".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: Type::Fun(
                    Box::new(Type::Con(
                        "Con".to_string(),
                        vec![Type::Var(TypeVar::new("T".to_string()))],
                        Bounds::from(["Container".to_string()]),
                    )),
                    Box::new(Type::Fun(Box::new(binary_fn2), Box::new(Type::Bool()))),
                ),
            },
        );

        let unary_fn = Type::Fun(
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
            Box::new(Type::Bool()),
        );
        self.global_ctx.insert(
            "for-all-elems".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: Type::Fun(
                    Box::new(Type::Con(
                        "Con".to_string(),
                        vec![Type::Var(TypeVar::new("T".to_string()))],
                        Bounds::from(["Container".to_string()]),
                    )),
                    Box::new(Type::Fun(Box::new(unary_fn), Box::new(Type::Bool()))),
                ),
            },
        );

        // put neq into context
        let neq_fn = Type::Fun(
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
            Box::new(Type::Fun(
                Box::new(Type::Var(TypeVar::new("T".to_string()))),
                Box::new(Type::Bool()),
            )),
        );
        self.global_ctx.insert(
            "neq".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: neq_fn,
            },
        );

        // put leq into context
        let leq_fn = Type::Fun(
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
            Box::new(Type::Fun(
                Box::new(Type::Var(TypeVar::new("T".to_string()))),
                Box::new(Type::Bool()),
            )),
        );
        self.global_ctx.insert(
            "leq?".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: leq_fn,
            },
        );

        let geq_fn = Type::Fun(
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
            Box::new(Type::Fun(
                Box::new(Type::Var(TypeVar::new("T".to_string()))),
                Box::new(Type::Bool()),
            )),
        );
        self.global_ctx.insert(
            "geq?".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: geq_fn,
            },
        );

        let equal = Type::Fun(
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
            Box::new(Type::Fun(
                Box::new(Type::Var(TypeVar::new("T".to_string()))),
                Box::new(Type::Bool()),
            )),
        );
        self.global_ctx.insert(
            "equal?".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: equal,
            },
        );

        let unique_count_fn = Type::Fun(
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
            Box::new(Type::Fun(
                Box::new(Type::Con(
                    "Con".to_string(),
                    vec![Type::Var(TypeVar::new("T".to_string()))],
                    Bounds::from(["Container".to_string()]),
                )),
                Box::new(Type::Bool()),
            )),
        );
        self.global_ctx.insert(
            "unique-count?".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: unique_count_fn,
            },
        );

        // the forall quantifier
        let forall = Type::Fun(
            Box::new(Type::Fun(
                Box::new(Type::Var(TypeVar::new("T".to_string()))),
                Box::new(Type::Bool()),
            )),
            Box::new(Type::Bool()),
        );
        self.global_ctx.insert(
            "forall".to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: forall,
            },
        );
    }

    /// Check an entire program
    pub fn check_prog(&mut self, prog: Prog) -> Result<(), TypeError> {
        let specs: Vec<Spec> = prog
            .iter()
            .filter(|block| block.is_spec_block())
            .map(|block| block.extract_spec())
            .collect();

        self.check_specs(&specs)
    }

    /// Check a list of specifications
    fn check_specs(&mut self, specs: &[Spec]) -> Result<(), TypeError> {
        let concat_specs = specs.concat();
        let prop_decls: Vec<&Decl> = concat_specs
            .iter()
            .filter(|decl| decl.is_prop_decl())
            .collect();
        let contype_decls: Vec<&Decl> = concat_specs
            .iter()
            .filter(|decl| decl.is_contype_decl())
            .collect();

        self.check_prop_decls(&prop_decls)?;
        self.check_contype_decls(&contype_decls)?;
        self.check_bound_decls(&contype_decls)
    }

    /// Check all bound declarations
    fn check_bound_decls(&mut self, decls: &[&Decl]) -> Result<(), TypeError> {
        for decl in decls.iter() {
            self.check_bound_decl(decl)?;
        }

        Ok(())
    }

    /// Check a single bound declaration
    fn check_bound_decl(&mut self, decl: &Decl) -> Result<(), TypeError> {
        let Decl::ConTypeDecl(_, (_, ins, _)) = decl else {
            return Err(TypeError("Not a valid bound declaration".to_string()));
        };

        // Duplicate bound name checking
        for i in ins.iter() {
            if self.global_ctx.get(&i.to_string()).is_some() {
                return Err(TypeError("Duplicate bound name declaration".to_string()));
            }
        }
        Ok(())
    }

    /// Check a list of property declarations
    fn check_prop_decls(&mut self, decls: &[&Decl]) -> Result<(), TypeError> {
        for decl in decls.iter() {
            self.check_prop_decl(decl)?;
        }

        Ok(())
    }

    /// Check a single property declaration
    fn check_prop_decl(&mut self, decl: &Decl) -> Result<(), TypeError> {
        let Decl::PropertyDecl(_, (id, _ty), term) = decl else {
            return Err(TypeError("Not a valid property declaration".to_string()));
        };

        // Duplicate property decl checking
        if self.global_ctx.get(&id.to_string()).is_some() {
            return Err(TypeError("Duplicate property declaration".to_string()));
        }

        // check well formedness
        let ty = self
            .global_ctx
            .type_inference(term, &mut self.tvg)
            .map_err(TypeError)?;

        // it should have type Con<T> -> Bool
        let Type::Fun(ref t1, ref t2) = ty else {
            return Err(TypeError(
                "Not a valid property decl: should have type Con<T> -> Bool".to_string(),
            ));
        };

        match (t1.deref(), t2.deref()) {
            (Type::Con(n, _t, _), Type::Bool()) => {
                if n == "Con" {
                    self.global_ctx.insert(
                        id.to_string(),
                        TypeScheme {
                            vars: Vec::new(),
                            ty,
                        },
                    );
                    Ok(())
                } else {
                    Err(TypeError("Not a valid property decl: input does not have basic container type Con<T>".to_string()))
                }
            }
            (_, Type::Bool()) => {
                self.global_ctx.insert(
                    id.to_string(),
                    TypeScheme {
                        vars: Vec::new(),
                        ty,
                    },
                );
                Ok(())
            }
            _ => Err(TypeError(
                "Not a valid property decl: should have type Con<T> -> Bool".to_string(),
            )),
        }
    }

    /// Check all container type declarations
    fn check_contype_decls(&mut self, decls: &[&Decl]) -> Result<(), TypeError> {
        for decl in decls.iter() {
            self.check_contype_decl(decl)?;
        }

        Ok(())
    }

    /// Check a single container type declaration
    fn check_contype_decl(&mut self, decl: &Decl) -> Result<(), TypeError> {
        let Decl::ConTypeDecl(con_ty, (vid, ins, r)) = decl else {
            return Err(TypeError(
                "Not a valid container type declaration".to_string(),
            ));
        };

        // Duplicate container type decl checking
        if self.global_ctx.get(&con_ty.to_string()).is_some() {
            return Err(TypeError(
                "Duplicate container type declaration".to_string(),
            ));
        }

        // Insert into local context
        let con = Type::Con(
            "Con".to_string(),
            vec![Type::Var(TypeVar::new("T".to_string()))],
            ins.clone(),
        );
        let mut local_ctx = self.global_ctx.clone();
        local_ctx.insert(
            vid.to_string(),
            TypeScheme {
                vars: Vec::new(),
                ty: con,
            },
        );

        // Check that it makes sense in the local context
        self.check_ref(&mut local_ctx, r)?;

        // If so, insert into global context
        self.global_ctx.insert(
            decl.get_name(),
            TypeScheme {
                vars: Vec::new(),
                ty: con_ty.clone(),
            },
        );
        Ok(())
    }

    fn check_ref(&mut self, ctx: &mut TypeEnv, r: &Refinement) -> Result<(), TypeError> {
        match r {
            Refinement::Prop(t) => {
                let t = ctx.type_inference(t, &mut self.tvg).map_err(TypeError)?;

                // t has to be boolean
                if t.is_bool() {
                    Ok(())
                } else {
                    Err(TypeError(
                        "The refinement has to be evaluated to a Bool type.".to_string(),
                    ))
                }
            }
            Refinement::AndProps(r1, r2) => {
                self.check_ref(ctx, r1)?;
                self.check_ref(ctx, r2)
            }
        }
    }
}

mod analysis;
mod bounded_ops;
mod description;
mod inference;
mod parser;
mod run_matching;
mod source_file;
mod spec_map;
mod type_check;
mod types;

pub mod tools;

mod codegen;
mod selector;
pub use codegen::ContainerSelection;
pub use selector::ContainerSelector;

mod library_specs;
pub use library_specs::LibSpec;
pub use spec_map::LibSpecs;

mod error;
pub use error::Error;

/// Names a container type we want to select.
pub type ConTypeName = String;

/// Name of a container implementation we are considering
pub type ImplName = String;

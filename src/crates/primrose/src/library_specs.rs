//! Process library files and extracts library specifications

use std::collections::BTreeMap;
use std::collections::HashMap;

use std::fs;

use std::io::{Error, Write};
use std::path::Path;

use log::debug;

use crate::analysis::REQUIRE;
use crate::spec_map::{Bounds, LibSpecs, ProvidedOps};

const LIBSPECNAME: &str = "/*LIBSPEC-NAME*";
const LIBSPECNAMEEND: &str = "*ENDLIBSPEC-NAME*/";
const LIBSPEC: &str = "/*LIBSPEC*";
const LIBSPECEND: &str = "*ENDLIBSPEC*/";
const LANGDECL: &str = "#lang rosette\n";
const GENPATH: &str = "./racket_specs/gen_lib_spec/";
const OPNAME: &str = "/*OPNAME*";
const OPNAMEEND: &str = "*ENDOPNAME*/";
const IMPL: &str = "/*IMPL*";
const IMPLEND: &str = "*ENDIMPL*/";

type ErrorMessage = String;

/// Specifications extracted from a library file
#[derive(Debug, Clone)]
pub struct LibSpec {
    /// Name of the specification
    pub spec_name: String,

    /// Name of the specified structs
    pub struct_name: String,

    /// All specification code defined
    pub specs: Vec<String>,

    /// The provided rosette module name
    pub provide: String,

    /// The bounds of each operation
    pub interface_provide_map: Bounds,

    /// The provided operations
    pub provided_ops: ProvidedOps,
}

impl LibSpec {
    /// Process all library specifications in the given directory.
    /// This will also write the required racket file for that spec (see [`Self::process`]).
    pub fn process_all(dirname: &Path) -> Result<LibSpecs, ErrorMessage> {
        let paths = fs::read_dir(dirname).unwrap();
        let files: Vec<String> = paths
            .into_iter()
            .map(|p| p.unwrap())
            .filter(|p| p.file_type().unwrap().is_file())
            .map(|path| path.path().into_os_string().into_string().unwrap())
            .filter(|path| !path.contains("/mod.rs") && !path.contains("/lib.rs"))
            .collect();
        let mut lib_specs = LibSpecs::new();
        for path in files {
            match Self::process(&path) {
                Ok(spec) => {
                    lib_specs.insert(spec.struct_name.clone(), spec);
                }
                Err(_) => {
                    debug!("Ignoring invalid library module {}", &path);
                }
            }
        }
        Ok(lib_specs)
    }

    /// Process a single library specification file.
    /// This will also write the required racket file for that spec.
    pub fn process(filename: &str) -> Result<Self, ErrorMessage> {
        let result = Self::read(filename);
        match result {
            Ok(spec) => {
                let _spec_name = format!("{}.rkt", &spec.spec_name);
                let state = write_lib_file(&spec.spec_name, &spec.specs, &spec.provide);
                if state.is_err() {
                    return Err("Unable to create lib specification file".to_string());
                }
                Ok(spec)
            }
            Err(e) => Err(e),
        }
    }

    /// Read all library specification files in the given directory.
    pub fn read_all(dirname: &Path) -> Result<LibSpecs, ErrorMessage> {
        let paths = fs::read_dir(dirname).map_err(|_e| "Library spec directory does not exist")?;

        let _lib_specs = LibSpecs::new();
        Ok(paths
            .into_iter()
            .flatten()
            .filter(|p| p.file_type().unwrap().is_file())
            .flat_map(|path| path.path().into_os_string().into_string())
            .filter(|path| !path.contains("/mod.rs") && !path.contains("/lib.rs"))
            .flat_map(|path| match Self::read(&path) {
                Ok(spec) => Some((spec.struct_name.clone(), spec)),
                Err(_) => {
                    debug!(
                        "Failed to process library module {}. Continuing anyway.",
                        &path
                    );
                    None
                }
            })
            .collect())
    }

    /// Read and parse a library specification file
    pub fn read(filename: &str) -> Result<Self, ErrorMessage> {
        let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
        let trimed_contents = contents.trim().to_string();

        let name_pragmas: Vec<&str> = trimed_contents.matches(LIBSPECNAME).collect();
        let name_end_pragmas: Vec<&str> = trimed_contents.matches(LIBSPECNAMEEND).collect();
        let spec_pragmas: Vec<&str> = trimed_contents.matches(LIBSPEC).collect();
        let spec_end_pragmas: Vec<&str> = trimed_contents.matches(LIBSPECEND).collect();
        if (name_pragmas.len() != 1) || (name_end_pragmas.len() != 1) {
            Err("Error, invalid declaration of library specification name**.".to_string())
        } else if spec_pragmas.len() != spec_end_pragmas.len() {
            return Err("Error, invalid declaration of library specification.".to_string());
        } else {
            let _specs = String::new();
            let v1: Vec<&str> = trimed_contents.split(LIBSPECNAME).collect();
            let s = v1
                .get(1)
                .expect("Error, invalid declaration of library specification name.");
            let v2: Vec<&str> = s.split(LIBSPECNAMEEND).collect();
            let s3 = v2.first().unwrap().trim().to_string();
            let v3: Vec<&str> = s3.split(' ').collect();
            let spec_name = v3.first().unwrap().trim().to_string();
            let struct_name = v3.get(1).unwrap().trim().to_string();
            let s1 = v1.first().expect("Unexpected error.");
            let s2 = v2.get(1).expect("Unexpected error.");
            // process interface blocks
            let mut trimed_contents = String::new();
            trimed_contents.push_str(s1);
            trimed_contents.push_str(s2);
            if !is_next_pragma_impl(&trimed_contents) && has_pragma_spec(&trimed_contents) {
                return Err("Specification without declared interface is not allowed".to_string());
            } else {
                let mut interfaces = Vec::<String>::new();
                let mut interface_info =
                    HashMap::<String, BTreeMap<String, (String, String, String)>>::new();
                let mut code = Vec::<String>::new();
                let mut provided_ops = Vec::<String>::new();
                while has_pragma_impl(&trimed_contents) {
                    let v4: Vec<&str> = trimed_contents.splitn(2, IMPL).collect();
                    let s4 = v4.get(1).expect("Error, invalid interface declaration.");
                    let v5: Vec<&str> = s4.splitn(2, IMPLEND).collect();
                    let interface_name = v5.first().unwrap().trim().to_string();
                    trimed_contents = v5.get(1).unwrap().trim().to_string();
                    let lib_specs = extract_lib_specs(trimed_contents);
                    match lib_specs {
                        Ok(RawLibSpec {
                            contents,
                            code: mut result,
                            op_infos,
                            provided_ops: mut ops,
                        }) => {
                            code.append(&mut result);
                            interface_info.insert(interface_name.clone(), op_infos);
                            interfaces.push(interface_name.clone());
                            provided_ops.append(&mut ops);
                            trimed_contents = contents;
                        }
                        Err(e) => {
                            return Err(e);
                        }
                    }
                }
                let (provide, interface_provide_map) = generate_provide(interface_info);
                Ok(LibSpec {
                    spec_name,
                    struct_name,
                    specs: code.clone(),
                    provide,
                    interface_provide_map,
                    provided_ops: (code, provided_ops),
                })
            }
        }
    }
}

/// Extracted, but not yet parsed directives from a library spec
struct RawLibSpec {
    /// The contents of the LIBSPEC block
    contents: String,
    /// The code define by the block
    code: Vec<String>,
    /// Hoare triples for each operation
    op_infos: BTreeMap<String, (String, String, String)>,
    /// Provided operation names
    provided_ops: Vec<String>,
}

fn is_next_pragma_impl(src: &str) -> bool {
    match src.find(IMPL) {
        Some(impl_pos) => match src.find(LIBSPEC) {
            Some(spec_pos) => impl_pos < spec_pos,
            None => true,
        },
        None => false,
    }
}

fn has_pragma_impl(src: &str) -> bool {
    src.contains(IMPL)
}

fn has_pragma_spec(src: &str) -> bool {
    src.contains(LIBSPEC)
}

fn generate_provide(
    interface_info: HashMap<String, BTreeMap<String, (String, String, String)>>,
) -> (String, Bounds) {
    let mut interfaces = Vec::<String>::new();
    let mut provide = String::new();
    let mut interface_provide_map = Bounds::new();
    for (interface, infos) in interface_info.iter() {
        let mut specs = Vec::<String>::new();
        let mut pres = Vec::<String>::new();
        for (_key, value) in infos.iter() {
            specs.push(value.0.clone());
            pres.push(value.1.clone());
        }
        let specs_name = interface.to_lowercase() + "-specs";
        let pres_name = interface.to_lowercase() + "-pres";
        let interface_name = interface.to_lowercase();
        let specs_str =
            "\n(define ".to_string() + &specs_name + " (list " + &specs.join(" ") + "))\n";
        let pres_str = "(define ".to_string() + &pres_name + " (list " + &pres.join(" ") + "))\n";
        let interface_str = "(define ".to_string()
            + &interface_name
            + " (cons "
            + &specs_name
            + " "
            + &pres_name
            + "))\n";
        provide = provide + &specs_str + &pres_str + &interface_str;
        interfaces.push(interface.to_lowercase());
        interface_provide_map.insert(interface.to_string(), interface_name);
    }
    let provide_str = "(provide ".to_string() + &interfaces.join(" ") + ")";

    provide = provide + &provide_str;
    (provide, interface_provide_map)
}

/// Extract the relevant LIBSPEC blocks
fn extract_lib_specs(src: String) -> Result<RawLibSpec, ErrorMessage> {
    let mut result = Vec::<String>::new();
    result.push(REQUIRE.to_string());
    let mut contents = src.trim();
    let mut op_infos = BTreeMap::<String, (String, String, String)>::new();
    let mut provided_ops = Vec::<String>::new();
    while !contents.is_empty() && !is_next_pragma_impl(contents) {
        if contents.contains(LIBSPEC) && contents.contains(LIBSPECEND) {
            let v1: Vec<&str> = contents.splitn(2, LIBSPEC).collect();
            let s = v1.get(1).expect("Error, invalid specification.");
            let v2: Vec<&str> = s.splitn(2, LIBSPECEND).collect();
            let spec = v2.first().unwrap().trim().to_string();
            let info = extract_op_info(spec.clone()).unwrap();
            op_infos.insert(info.0, (info.1.clone(), info.2, info.3));
            provided_ops.push(info.1);
            let v3: Vec<&str> = spec.splitn(2, OPNAMEEND).collect();
            let code = v3
                .get(1)
                .unwrap()
                .trim_matches(|c| c == '\t' || c == ' ')
                .to_string();
            result.push(code);
            contents = v2.get(1).unwrap().trim();
        } else {
            break;
        }
    }
    Ok(RawLibSpec {
        contents: contents.to_string(),
        code: result,
        op_infos,
        provided_ops,
    })
}

fn extract_op_info(spec: String) -> Result<(String, String, String, String), ErrorMessage> {
    let op_name_pragmas: Vec<&str> = spec.matches(OPNAME).collect();
    let op_name_end_pragmas: Vec<&str> = spec.matches(OPNAMEEND).collect();
    if spec.starts_with('/') && op_name_pragmas.len() == 1 && op_name_end_pragmas.len() == 1 {
        let v1: Vec<&str> = spec.split(OPNAME).collect();
        let s = v1
            .get(1)
            .expect("Error, invaild operation information declaration.");
        let v2: Vec<&str> = s.split(OPNAMEEND).collect();
        let info_string = v2
            .first()
            .expect("Error, invaild operation information declaration.");
        let mut infos: Vec<&str> = info_string.trim().split(' ').collect();
        if infos.len() == 4 {
            let post = infos.pop().unwrap();
            let pre = infos.pop().unwrap();
            let op_spec = infos.pop().unwrap();
            let name = infos.pop().unwrap();
            Ok((
                name.to_string(),
                op_spec.to_string(),
                pre.to_string(),
                post.to_string(),
            ))
        } else {
            Err("Error, invaild operation information declaration.".to_string())
        }
    } else {
        Err("Error, invaild operation information declaration.".to_string())
    }
}

fn write_lib_file(filename: &str, contents: &[String], provide: &str) -> Result<(), Error> {
    let path = GENPATH;

    let mut output = fs::File::create(path.to_owned() + filename)?;
    write!(output, "{}", LANGDECL)?;
    for item in contents.iter() {
        write!(output, "{}", item)?;
    }
    write!(output, "{}", provide)?;
    Ok(())
}

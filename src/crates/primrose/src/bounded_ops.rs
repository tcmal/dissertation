use crate::types::{Bounds, Type, TypeVar};

use std::collections::HashMap;

type BoundName = String;
type OpName = String;
type OpInfo = (OpName, Type);
pub type BoundedOps = HashMap<BoundName, Vec<OpInfo>>;

pub fn generate_bounded_ops() -> BoundedOps {
    let mut ops = BoundedOps::new();
    let push = (
        "op-push".to_string(),
        Type::Fun(
            Box::new(Type::Con(
                "Con".to_string(),
                vec![Type::Var(TypeVar::new("T".to_string()))],
                Bounds::from(["Stack".to_string()]),
            )),
            Box::new(Type::Fun(
                Box::new(Type::Var(TypeVar::new("T".to_string()))),
                Box::new(Type::Con(
                    "Con".to_string(),
                    vec![Type::Var(TypeVar::new("T".to_string()))],
                    Bounds::from(["Stack".to_string()]),
                )),
            )),
        ),
    );
    let pop = (
        "op-pop".to_string(),
        Type::Fun(
            Box::new(Type::Con(
                "Con".to_string(),
                vec![Type::Var(TypeVar::new("T".to_string()))],
                Bounds::from(["Stack".to_string()]),
            )),
            Box::new(Type::Var(TypeVar::new("T".to_string()))),
        ),
    );
    ops.insert("Stack".to_string(), vec![push, pop]);

    // Mapping operations
    let mapping_ty = Type::Con(
        "Con".to_string(),
        vec![TypeVar::new("K").into(), TypeVar::new("V").into()],
        Bounds::from(["Mapping".to_string()]),
    );
    ops.insert(
        "Mapping".to_string(),
        vec![
            (
                "op-len".to_string(),
                Type::Fun(Box::new(mapping_ty.clone()), Box::new(Type::Int)),
            ),
            (
                "op-is_empty".to_string(),
                Type::Fun(Box::new(mapping_ty.clone()), Box::new(Type::Bool())),
            ),
            (
                "op-contains".to_string(),
                Type::Fun(
                    Box::new(mapping_ty.clone()),
                    Box::new(Type::Fun(
                        Box::new(TypeVar::new("K").into()),
                        Box::new(Type::Bool()),
                    )),
                ),
            ),
            (
                "op-insert".to_string(),
                Type::Fun(
                    Box::new(mapping_ty.clone()),
                    Box::new(Type::Fun(
                        Box::new(TypeVar::new("K").into()),
                        Box::new(Type::Fun(
                            Box::new(TypeVar::new("V").into()),
                            Box::new(mapping_ty.clone()),
                        )),
                    )),
                ),
            ),
            (
                "op-get".to_string(),
                Type::Fun(
                    Box::new(mapping_ty.clone()),
                    Box::new(Type::Fun(
                        Box::new(TypeVar::new("K").into()),
                        Box::new(TypeVar::new("V").into()),
                    )),
                ),
            ),
            (
                "op-remove".to_string(),
                Type::Fun(
                    Box::new(mapping_ty.clone()),
                    Box::new(Type::Fun(
                        Box::new(TypeVar::new("K").into()),
                        Box::new(mapping_ty.clone()),
                    )),
                ),
            ),
            (
                "clear".to_string(),
                Type::Fun(Box::new(mapping_ty.clone()), Box::new(mapping_ty.clone())),
            ),
        ],
    );

    let indexable_ty = Type::Con(
        "Con".to_string(),
        vec![TypeVar::new("T").into()],
        Bounds::from(["Indexable".to_string()]),
    );

    ops.insert(
        "Indexable".to_string(),
        vec![
            (
                "op-last".to_string(),
                Type::Fun(
                    Box::new(indexable_ty.clone()),
                    Box::new(TypeVar::new("T").into()),
                ),
            ),
            (
                "op-first".to_string(),
                Type::Fun(
                    Box::new(indexable_ty.clone()),
                    Box::new(TypeVar::new("T").into()),
                ),
            ),
            (
                "op-nth".to_string(),
                Type::Fun(
                    Box::new(indexable_ty.clone()),
                    Box::new(Type::Fun(
                        Box::new(Type::Int),
                        Box::new(TypeVar::new("T").into()),
                    )),
                ),
            ),
        ],
    );

    let container_ty = Type::Con(
        "Con".to_string(),
        vec![TypeVar::new("T").into()],
        Bounds::from(["Container".to_string()]),
    );
    ops.insert(
        "Container".to_string(),
        vec![
            (
                "op-len".to_string(),
                Type::Fun(Box::new(container_ty.clone()), Box::new(Type::Int)),
            ),
            (
                "op-is_empty".to_string(),
                Type::Fun(Box::new(container_ty.clone()), Box::new(Type::Bool())),
            ),
            (
                "op-contains".to_string(),
                Type::Fun(
                    Box::new(container_ty.clone()),
                    Box::new(Type::Fun(
                        Box::new(TypeVar::new("T").into()),
                        Box::new(Type::Bool()),
                    )),
                ),
            ),
            (
                "op-insert".to_string(),
                Type::Fun(
                    Box::new(container_ty.clone()),
                    Box::new(Type::Fun(
                        Box::new(TypeVar::new("T").into()),
                        Box::new(container_ty.clone()),
                    )),
                ),
            ),
            (
                "op-remove".to_string(),
                Type::Fun(
                    Box::new(container_ty.clone()),
                    Box::new(Type::Fun(
                        Box::new(TypeVar::new("T").into()),
                        Box::new(container_ty.clone()),
                    )),
                ),
            ),
            (
                "op-clear".to_string(),
                Type::Fun(
                    Box::new(container_ty.clone()),
                    Box::new(container_ty.clone()),
                ),
            ),
        ],
    );

    ops
}

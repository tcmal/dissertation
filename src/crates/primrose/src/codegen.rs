//! Generating rust code from results of

use std::fmt::Display;

use crate::{
    description::{InforMap, Tag},
    parser::Block,
    selector::ContainerSelector,
    ImplName,
};

const CODEGEN: &str = "/*CODEGEN*/\n";
const CODEGENEND: &str = "/*ENDCODEGEN*/\n";
const TRAITCRATE: &str = "primrose_library::traits::";

/// A selected concrete type that we can use
#[derive(Clone)]
pub enum ContainerSelection {
    Singular(ImplName),
    Profile(ImplName, usize),
    Split {
        before: ImplName,
        threshold: usize,
        after: ImplName,
    },
}

impl ContainerSelection {
    /// Parse a container selection from a string.
    /// The string should either be the name of a single implementation, or in the format
    /// `{before}-UNTIL-{threshold}-THEN-{after}`
    pub fn parse(inp: &str) -> Option<Self> {
        if inp.contains("-UNTIL-") {
            let (before, inp) = inp.split_once("-UNTIL-")?;
            let (threshold, after) = inp.split_once("-THEN-")?;
            Some(Self::Split {
                before: before.to_string(),
                threshold: threshold.parse().ok()?,
                after: after.to_string(),
            })
        } else {
            Some(Self::Singular(inp.to_string()))
        }
    }
}

impl std::fmt::Debug for ContainerSelection {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl Display for ContainerSelection {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ContainerSelection::Singular(x) => write!(f, "{}", x),
            ContainerSelection::Split {
                before,
                threshold,
                after,
            } => {
                write!(f, "{} until n={}, then {}", before, threshold, after)
            }
            ContainerSelection::Profile(inner, idx) => {
                write!(f, "profiled: index = {} inner = {}", idx, inner)
            }
        }
    }
}

impl ContainerSelector {
    /// Generate replacement code for the whole file, with the given `(tag_id, selection)` pairs.
    /// This will generate invalid code if any selection is invalid, or panic if any `tag_id` is invalid.
    /// Returns the original file with the generated code in place of the original specification.
    pub fn gen_replacement_file<'a, T: Iterator<Item = (&'a String, &'a ContainerSelection)>>(
        &self,
        selections: T,
    ) -> String {
        let mut result = String::new();
        result += &codegen_block(&self.bounds_decl);
        for (tag_id, selection) in selections {
            result += &self.gen_replacement_code(tag_id, selection);
        }

        // rest of the rust code, minus the spec stuff
        add_all_code(&mut result, self.blocks.iter());
        result
    }

    /// Generate replacement code for the given tag, choosing the given library spec.
    /// This will generate invalid code if selection is invalid, or panic if `tag_id` is invalid.
    /// Returns only the required code, and not the rest of the code originally in the file.
    pub(crate) fn gen_replacement_code(
        &self,
        tag_id: &String,
        selection: &ContainerSelection,
    ) -> String {
        let tag = self.analyser.ctx().get(tag_id).expect("invalid tag_id");
        let Tag::Con(elem_ty, _, _) = tag else {
            panic!("tag_id was not Tag::Con");
        };
        let bounds = to_trait_bounds(elem_ty);
        let vars = elem_ty.join(", ");

        let inner = match selection {
            ContainerSelection::Singular(inner) => format!(
                r#"
#[allow(non_snake_case)]
fn _{tag_id}<{bounds}>() -> {tag_id}<{vars}> {{
    {inner}::<{vars}>::default()
}}
"#
            ),
            ContainerSelection::Profile(inner, idx) => {
                let wrapper_name = if self
                    .lib_specs
                    .get(inner)
                    .expect("invalid inner implementation of profiled container")
                    .interface_provide_map
                    .contains_key("Mapping")
                {
                    "MappingProfilerWrapper"
                } else {
                    "ProfilerWrapper"
                };
                format!(
                    r#"
#[allow(non_snake_case)]
fn _{tag_id}<{bounds}>() -> {tag_id}<{vars}> {{
    ::primrose_library::{wrapper_name}::<{idx}, {inner}<{vars}>, _>::default()
}}
"#
                )
            }
            ContainerSelection::Split {
                before,
                threshold,
                after,
            } => {
                format!(
                    r#"
#[allow(non_snake_case)]
fn _{tag_id}<{bounds}>() -> {tag_id}<{vars}> {{
    ::primrose_library::AdaptiveContainer::<{threshold}, {before}<{vars}>, {after}<{vars}>, ({vars})>::default()
}}
"#
                )
            }
        };

        codegen_block(&inner)
    }
}

fn codegen_block(x: &str) -> String {
    format!("{}{}{}", CODEGEN, x, CODEGENEND)
}

fn add_all_code<'a>(result: &mut String, blocks: impl Iterator<Item = &'a Block>) {
    for block in blocks.filter(|block| block.is_code_block()) {
        match block {
            Block::CodeBlock(code, _) => *result += code,
            _ => unreachable!(),
        };
    }
}

pub fn process_bound_decl(ctx: &InforMap) -> String {
    let mut code = String::new();
    for (_, tag) in ctx.iter() {
        match tag {
            Tag::Bound((c, t), decs) => {
                let traits = decs
                    .iter()
                    .map(|name| process_bound_elem_ty(name, &t.join(", ")))
                    .collect::<Vec<String>>()
                    .join(" + ");
                code = code + &gen_trait_code(c, &to_trait_bounds(t), &traits);
            }
            _ => continue,
        }
    }

    code
}

fn gen_trait_code(typ_name: &str, elem_type: &str, traits: &str) -> String {
    format!(
        r#"
pub type {typ_name}<{elem_type}> = impl {traits} + Default;
"#
    )
}

fn process_bound_elem_ty(t: &str, elem_ty: &str) -> String {
    TRAITCRATE.to_string() + t + "<" + elem_ty + ">"
}

fn to_trait_bounds(ks: &[String]) -> String {
    ks.iter()
        .map(|t| format!("{}: PartialEq + Ord + std::hash::Hash", t))
        .collect::<Vec<_>>()
        .join(", ")
}

use anyhow::Result;
use argh::FromArgs;
use log::info;

use crate::State;

/// List selection sites and their candidates
#[derive(FromArgs)]
#[argh(subcommand, name = "candidates")]
pub struct Args {}

impl State {
    pub fn cmd_candidates(&self, _: Args) -> Result<()> {
        for proj in self.projects.iter() {
            info!("Processing project {}", &proj.name);

            let candidates = self.inner.project_candidate_list(proj)?;
            for (file, con_types) in candidates.iter_2d() {
                self.print_table(con_types.flat_map(|(ctn, candidates)| {
                    candidates.iter().map(move |v| {
                        (
                            ctn,
                            [("implementation", v.as_str()), ("file", file.as_str())],
                        )
                    })
                }))
            }
        }
        Ok(())
    }
}

use anyhow::{bail, Result};
use argh::FromArgs;
use candelabra::types::Mapping2D;
use cargo_metadata::camino::Utf8PathBuf;
use log::info;
use primrose::ContainerSelection;

use crate::State;

/// Generate code for the project that uses the given assignments
#[derive(FromArgs)]
#[argh(subcommand, name = "place")]
pub struct Args {
    /// the assignments, written `<file path> <type name> <implementation>` ...
    #[argh(positional, greedy)]
    assignments: Vec<String>,
}

impl State {
    pub fn cmd_place(&self, args: Args) -> Result<()> {
        if self.projects.len() > 1 {
            bail!("estimate must target only one project!")
        }
        let proj = &self.projects[0];

        // parse assignment list
        if args.assignments.len() % 3 != 0 {
            bail!("assignments list must have length divisible by 3");
        }
        let assignments =
            args.assignments
                .iter()
                .step_by(3)
                .map(Utf8PathBuf::from)
                .zip(
                    args.assignments
                        .iter()
                        .skip(1)
                        .step_by(3)
                        .map(String::to_string),
                )
                .zip(args.assignments.iter().skip(2).step_by(3).map(|s| {
                    ContainerSelection::parse(s).expect("invalid container selection format")
                }))
                .collect::<Mapping2D<_, _, _>>();

        info!("Using assignments: {:?}", &assignments);

        self.inner.save_choices(proj, &assignments)?;

        Ok(())
    }
}

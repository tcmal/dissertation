use std::iter::once;

use anyhow::Result;
use argh::FromArgs;
use log::info;

use crate::State;

/// Show the cost model for the given implementation
#[derive(FromArgs)]
#[argh(subcommand, name = "cost-model")]
pub struct Args {
    /// the implementation to get the cost model of.
    #[argh(positional)]
    name: String,
}

impl State {
    pub fn cmd_model(&self, args: Args) -> Result<()> {
        info!("Calculating cost model for {}", &args.name);
        let (model, results) = self.inner.cost_info(&args.name)?;

        // Table of parameters
        self.print_table(model.by_op.iter().map(|(op, est)| {
            let obvs = results.by_op.get(op).unwrap();
            (
                op,
                est.coeffs
                    .iter()
                    .enumerate()
                    .map(|(pow, coeff)| (format!("coeff {}", pow), coeff.to_string()))
                    .chain(once(("nrmse".to_string(), est.nrmse(obvs).to_string())))
                    .chain(once((
                        "formula".to_string(),
                        format!(
                            "{} + {}*x + {}*x^2 + {}*log_2 x",
                            est.coeffs[0], est.coeffs[1], est.coeffs[2], est.coeffs[3]
                        ),
                    ))),
            )
        }));

        Ok(())
    }
}

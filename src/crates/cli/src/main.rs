use anyhow::{anyhow, Context, Result};
use argh::FromArgs;
use candelabra::{Paths, Project};
use log::info;

mod candidates;
mod display;
mod library;
mod model;
mod place;
mod profile;
mod select;

#[derive(FromArgs)]
/// Find the best performing container type using primrose
pub struct Args {
    /// path to Cargo.toml
    #[argh(option)]
    pub manifest_path: Option<String>,

    /// project to run on, if in a workspace
    #[argh(option, short = 'p')]
    pub project: Option<String>,

    /// also output LaTeX tables
    #[argh(switch, short = 'l')]
    pub with_latex: bool,

    #[argh(subcommand)]
    pub cmd: Subcommand,
}

#[derive(FromArgs)]
#[argh(subcommand)]
pub enum Subcommand {
    Model(model::Args),
    Library(library::Args),
    Candidates(candidates::Args),
    Profile(profile::Args),
    Select(select::Args),
    Place(place::Args),
}

fn main() -> Result<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();
    let args: Args = argh::from_env();

    // Build shared state
    let paths = Paths::default();
    info!("Using source dir: {:?}", &paths.base);
    let state = State {
        inner: candelabra::State::new(paths)?,
        projects: get_projects(&args)?,
        with_latex: args.with_latex,
    };

    match args.cmd {
        Subcommand::Model(a) => state.cmd_model(a),
        Subcommand::Library(a) => state.cmd_library(a),
        Subcommand::Candidates(a) => state.cmd_candidates(a),
        Subcommand::Profile(a) => state.cmd_profile(a),
        Subcommand::Select(a) => state.cmd_select(a),
        Subcommand::Place(a) => state.cmd_place(a),
    }
}

struct State {
    inner: candelabra::State,
    projects: Vec<Project>,
    with_latex: bool,
}

fn get_projects(args: &Args) -> Result<Vec<Project>> {
    let mut cmd = cargo_metadata::MetadataCommand::new();
    if let Some(p) = &args.manifest_path {
        cmd.manifest_path(p);
    }

    let metadata = cmd.exec().context("failed to get manifest metadata")?;

    if let Some(p) = &args.project {
        // Select a specific project
        Ok(vec![metadata
            .packages
            .iter()
            .find(|pkg| pkg.name == *p)
            .map(|pkg| Project::new(pkg.clone()))
            .ok_or_else(|| {
                anyhow!("specified project does not exist")
            })?])
    } else {
        // Default to all workspace members
        Ok(metadata
            .workspace_members
            .iter()
            .flat_map(|member| metadata.packages.iter().find(|pkg| pkg.id == *member))
            .map(|pkg| Project::new(pkg.clone()))
            .collect())
    }
}

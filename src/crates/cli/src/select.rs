use std::collections::HashMap;

use anyhow::Result;
use argh::FromArgs;
use cargo_metadata::camino::Utf8PathBuf;
use log::info;
use primrose::{tools::nary_cartesian_product, ConTypeName, ContainerSelection};
use tabled::builder::Builder;

use crate::State;

/// Select an implementation for each container type and project
#[derive(FromArgs)]
#[argh(subcommand, name = "select")]
pub struct Args {
    /// run project benchmarks with each candidate and compare estimated vs real position
    #[argh(switch)]
    compare: bool,
}

impl State {
    pub fn cmd_select(&self, a: Args) -> Result<()> {
        for proj in self.projects.iter() {
            info!("Processing project {}", &proj.name);

            let costs = self.inner.rank_candidates(proj).unwrap();

            let mut builder = Builder::default();
            builder.set_header(["name", "implementation", "estimated cost", "file"]);
            for (f, ctn, candidates) in costs.iter() {
                for (candidate, cost) in candidates.iter() {
                    builder.push_record([
                        ctn.as_str(),
                        candidate.to_string().as_str(),
                        cost.to_string().as_str(),
                        f.as_str(),
                    ]);
                }
            }

            self.print_table_raw(builder.build());

            let ideal = self.inner.select(proj)?;
            self.inner.save_choices(proj, &ideal)?;
            info!("Saved best choices");

            if !a.compare {
                continue;
            }

            // 'brute force' and time all possibilities
            let candidates = costs
                .iter()
                .map(|(f, ctn, cs)| ((f, ctn), cs.iter().map(|(i, _)| i.clone()).collect()))
                .collect::<HashMap<(&Utf8PathBuf, &ConTypeName), Vec<ContainerSelection>>>();
            let possible_assignments = nary_cartesian_product(&candidates);

            let mut assignments_results = HashMap::new();
            for assignment in possible_assignments.iter() {
                info!("Running benchmarks with {:?}", &assignment);
                assignments_results.insert(
                    format!("{:?}", &assignment),
                    self.inner.run_benchmarks_with(
                        proj,
                        &assignment
                            .iter()
                            .map(|((f, ctn), sel)| ((*f).clone(), (*ctn).clone(), (*sel).clone()))
                            .collect(),
                    )?,
                );
            }

            self.print_table(assignments_results);
        }
        Ok(())
    }
}

use anyhow::Result;
use argh::FromArgs;
use log::info;

use crate::State;

/// Profile the selected projects and print the results
#[derive(FromArgs)]
#[argh(subcommand, name = "profile")]
pub struct Args {}

impl State {
    pub fn cmd_profile(&self, _: Args) -> Result<()> {
        for proj in self.projects.iter() {
            info!("Processing project {}", &proj.name);

            let all_info = self.inner.usage_profile(proj)?;

            for (f, ctn, usage_profile) in all_info.iter() {
                info!("{} / {}:", f, ctn);
                self.display_usage_info(usage_profile);
            }
        }
        Ok(())
    }
}

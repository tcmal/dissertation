use anyhow::Result;
use argh::FromArgs;
use log::info;

use crate::State;

/// List all implementations in the library
#[derive(FromArgs)]
#[argh(subcommand, name = "list-library")]
pub struct Args {}

impl State {
    pub fn cmd_library(&self, _: Args) -> Result<()> {
        info!("Available container implementations:");
        for name in self.inner.lib_specs().keys() {
            info!("  {}", name);
        }

        Ok(())
    }
}

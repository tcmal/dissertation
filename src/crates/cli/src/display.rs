use std::{collections::HashSet, fmt::Display, hash::Hash, iter::once};

use candelabra::profiler::UsageProfile;
use tabled::{builder::Builder, grid::records::vec_records::Cell, settings::Style, Table};

use crate::State;

impl State {
    // Print the given 2D map as a table, where the first key is the left-most column, and the second key the column index
    pub fn print_table<I1, I2, A, B, C>(&self, map: I1)
    where
        I1: IntoIterator<Item = (A, I2)>,
        I2: IntoIterator<Item = (B, C)>,
        A: Display,
        B: Display + Eq + Hash + Ord,
        C: Display,
    {
        // Collect everything up
        let map = map
            .into_iter()
            .map(|(k, v)| (k, v.into_iter().collect::<Vec<(_, _)>>()))
            .collect::<Vec<(_, _)>>();

        // Get keys
        let mut keys = map
            .iter()
            .flat_map(|(_, vs)| vs.iter().map(|(k, _)| k))
            .collect::<HashSet<_>>() // dedup
            .into_iter()
            .collect::<Vec<_>>(); // consistent order
        keys.sort();

        let mut builder = Builder::new();
        builder.set_header(once("".to_string()).chain(keys.iter().map(|s| s.to_string())));

        // Add rows
        for (key, vals) in map.iter() {
            builder.push_record(once(key.to_string()).chain(keys.iter().map(|k| {
                vals.iter()
                    .find(|(search, _)| search == *k)
                    .map(|(_, v)| v.to_string())
                    .unwrap_or("".to_string())
            })));
        }

        let mut table = builder.build();
        println!("{}", table.with(Style::sharp()));

        if self.with_latex {
            print_latex(table);
        }
    }

    pub fn display_usage_info(&self, profile_info: &UsageProfile) {
        self.print_table(profile_info.0.iter().enumerate().map(|(i, p)| {
            (
                i,
                [
                    ("n".to_string(), p.avg_n),
                    ("occurences".to_string(), p.occurences),
                ]
                .into_iter()
                .chain(p.avg_op_counts.clone()),
            )
        }))
    }

    pub fn print_table_raw(&self, mut t: Table) {
        println!("{}", t.with(Style::sharp()));
        if self.with_latex {
            print_latex(t);
        }
    }
}

fn print_latex(t: Table) {
    println!(
        "\\begin{{center}}\n\\begin{{tabular}}{{{}|}}",
        "|c".repeat(t.shape().1)
    );
    for (i, row) in t.get_records().iter().enumerate() {
        if row.is_empty() {
            continue;
        }

        print!("{} ", escape_latex(row[0].text()));
        for col in &row[1..] {
            print!("& {} ", escape_latex(col.text()));
        }
        println!("\\\\");

        // header line
        if i == 0 {
            println!("\\hline");
        }
    }
    println!("\\end{{tabular}}\n\\end{{center}}");
}

fn escape_latex(s: &str) -> String {
    let mut out = String::with_capacity(s.len());
    for c in s.chars() {
        match c {
            '\\' | '{' | '}' | '_' | '^' | '#' | '&' | '$' | '%' | '~' => {
                out.push('\\');
                out.push(c);
            }
            c => out.push(c),
        }
    }

    out
}

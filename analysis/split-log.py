#!/usr/bin/env python3
from sys import argv
from pathlib import Path

log_path = Path(argv[1])
sections_path = log_path.parent / "sections"
sections_path.mkdir(exist_ok=True)
f = log_path.open("r")

sections = f.read().split("[0Ksection_start")

for section in sections:
    try:
        section_name = section.split("\n")[0].split(":")[2]
        if "cost-model" not in section_name and "compare" not in section_name:
            continue
        out_path = sections_path / section_name
        out_path.touch
        out_path.write_text(section)
        print("wrote section", section_name, "to", out_path)
    except Exception as e:
        print("in one section", e)

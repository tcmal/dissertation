# Analysis

This folder contains our raw data from testing Candelabra, and the notebook we used to create visualisations used in the thesis.

To run the notebook, start a [livebook](https://livebook.dev) server with `livebook server`, and open `vis.livemd` using it.

## Using new data

To use new data, first generate some new data locally. In `../src/`:

```
$ just cost-models # approx 10m
$ just selections --compare 2>&1 | tee ../analysis/current/log # approx 1hr 30m
```

We need to also write the log output when comparing since we use this during analysis. To put this data in the right place, in `../analysis`:

```
$ just split-log
$ just fetch-cost-models
$ just fetch-comparisons
```

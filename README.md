# Candelabra

These are the supplementary materials for my thesis, Candelabra.

  - `src/` is a Rust workspace containing the actual source code
  - `analysis/` contains the benchmark results, and the Elixir livebook notebook used to process and visualise the data.
  - `thesis/` contains the LaTeX code used to build the actual thesis, as well as VegaLite JSON files for graphs

Check the `README` in each directory for more information.

## Getting dependencies

The easiest way to install the required dependencies is using Nix.
After [installing it](https://nixos.org/download/), simply run one of the two commands below:

  - `nix develop .#full` for a full environment with everything required to build the project, visualisations, and thesis. (approx 3.4G disk space)
  - `nix develop .#minimal` for a smaller environment, with only enough to build the project and run tests (approx 1.9G disk space).

You can also build the program itself with `nix build .#default`, or see below to run a test VM.

Alternatively, manually set up the following programs:

  - Rust nightly toolchain 2024-01-22
  - Racket v8.10, with the `rosette` package installed - `raco pkg install rosette`. You may also need to manually install `z3`.
  - [Just](https://crates.io/crates/just) 1.24.0
  - To generate visualisations:
    - [Livebook](https://livebook.dev/) 0.11.3
    - [vega-cli](https://www.npmjs.com/package/vega-cli 5.26.0
  - To build the thesis:
    - Texlive, with the packages listed in `nix/tex-env.nix`
    - `latexmk`

Once you have dependencies installed, check the `README` in each directory for details on how to reproduce our results.

## Building a testing VM

The VM which we used for testing can be fully rebuilt by running `nix build .#vm`. This requires around 3.1G on disk.

After building, run `./result/bin/run-candelabra-vm` to start it.
The credentials are `root` / `candelabra`, and you can use SSH if desired.

The VM has candelabra prebuilt as `candelabra-cli`, but no tools for visualisation or building the thesis.

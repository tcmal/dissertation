{
  description = "Aria Shrimpton dissertation";
  inputs = {
    crane.url = "github:ipetkov/crane";
    crane.inputs.nixpkgs.follows = "nixpkgs";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    nixos-generators,
    crane,
    rust-overlay,
  }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
      overlays = [(import rust-overlay)];
    };
    tex-env = pkgs.callPackage ./nix/tex-env.nix inputs;
    racket-env = pkgs.callPackage ./nix/racket-env.nix inputs;
    rust-toolchain = pkgs.rust-bin.fromRustupToolchain ((builtins.fromTOML (builtins.readFile ./src/rust-toolchain.toml)).toolchain // {profile = "minimal";});
    candelabra = pkgs.callPackage ./nix/candelabra.nix inputs;
  in rec {
    devShells.${system} = {
      minimal = pkgs.mkShell {
        buildInputs = [
          rust-toolchain
          racket-env
          pkgs.just # command runner
          pkgs.inotify-tools
        ];
      };
      full = pkgs.mkShell {
        buildInputs = [
          rust-toolchain
          racket-env
          pkgs.just # command runner
          pkgs.inotify-tools

          tex-env
          pkgs.biber # bibliography backend

          pkgs.livebook # datavis
          # god is dead
          pkgs.nodejs
          pkgs.nodePackages.vega-cli
          pkgs.nodePackages.vega-lite
        ];
      };
    };

    packages.${system} = {
      default = candelabra;
      vm = nixos-generators.nixosGenerate {
        inherit system;
        specialArgs = {
          inherit inputs tex-env racket-env candelabra;
        };
        modules = [./nix/configuration.nix];

        format = "vm";
      };
    };
  };
}

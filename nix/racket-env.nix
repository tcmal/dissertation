{pkgs ? import <nixpkgs> {}, ...}: let
  inherit (pkgs) fetchurl fetchgit;
  racket = pkgs.racket-minimal;
  collection = pkgs.stdenv.mkDerivation {
    name = "raco-collection";
    buildInputs = [racket];
    phases = ["unpackPhase" "buildPhase"];

    src = ./racket-pkgs.tar.xz;

    buildPhase = ''
      mkdir -p /tmp/install
      for f in ./*; do
        fname=`basename $f`
        ln -s `readlink -e $f` "/tmp/install/''${fname:33}"
      done

      mkdir -p $out
      export PLTADDONDIR=$out

      raco pkg install --batch --fail-fast --no-docs -j 4 --deps force --copy /tmp/install/*
    '';
  };
in
  pkgs.stdenv.mkDerivation {
    name = "racket-wrapped";
    buildInputs = [pkgs.makeWrapper];
    phases = ["installPhase" "fixupPhase"];
    installPhase = ''
      mkdir -p $out/bin
      makeWrapper ${racket}/bin/racket $out/bin/racket \
        --set PLTADDONDIR ${collection} \
        --prefix PATH : ${pkgs.z3}/bin;
    '';
  }

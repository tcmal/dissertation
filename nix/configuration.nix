# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).
{
  config,
  lib,
  pkgs,
  modulesPath,
  candelabra,
  ...
}: {
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  environment.systemPackages = with pkgs; [
    git
    just
    candelabra # :D
  ];

  nix = {
    settings.experimental-features = ["nix-command" "flakes" "ca-derivations"];
  };

  networking.hostName = "candelabra";
  networking.useDHCP = lib.mkDefault true;

  time.timeZone = "Europe/London";
  i18n.defaultLocale = "en_US.UTF-8";

  # SSH Access
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "yes";
  };

  users.users.root.password = "candelabra";
  services.getty.autologinUser = "root";

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "23.11";

  system.activationScripts.copy-candelabra-src = ''
    echo "copying candelabra source to /root/...";
    cp -r ${../.} /root/candelabra;
  '';
}

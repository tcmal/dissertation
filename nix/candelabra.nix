inputs @ {
  pkgs,
  crane,
  ...
}: let
  inherit (pkgs) system;
  toolchain = pkgs.rust-bin.fromRustupToolchain ((builtins.fromTOML (builtins.readFile ../src/rust-toolchain.toml)).toolchain // {profile = "minimal";});
  craneLib = crane.lib.${system}.overrideToolchain toolchain;

  rustCrate = craneLib.buildPackage {
    pname = "candelabra-unwrapped";
    version = "1.0.0";

    src = craneLib.cleanCargoSource (craneLib.path ../src);
    doCheck = false;
    propagatedBuildInputs = [];

    cargoExtraArgs = "-p candelabra-cli";
  };
in
  pkgs.runCommand "candelabra" {} ''
    . ${pkgs.makeWrapper}/nix-support/setup-hook
    makeWrapper ${rustCrate}/bin/candelabra-cli $out/bin/candelabra-cli \
      --set CANDELABRA_SRC_DIR ${pkgs.lib.sourceByRegex ../src [
      ".*crates"
      ".*racket_specs.*"
      ".*crates/library.*"
      ".*crates/benchmarker.*"
      ".*rust-toolchain.toml.*"
      ".*Cargo.toml.*"
      ".*Cargo.lock.*"
    ]} \
        --prefix PATH : ${pkgs.callPackage ./racket-env.nix inputs}/bin \
        --prefix PATH : ${toolchain}/bin \
        --prefix PATH : ${pkgs.gcc}/bin;
  ''

{pkgs, ...}:
pkgs.texlive.combine {
  inherit
    (pkgs.texlive)
    scheme-small
    microtype
    sectsty
    printlen
    psnfss
    helvetic
    courier
    biblatex
    listings
    zapfchan
    latex-bin
    latexmk
    adjustbox
    algorithms
    algorithmicx
    ;
}

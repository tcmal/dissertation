
%% *** Introduce problem

%% **** Container types common in programs

Almost every program makes extensive use of container data types -- structures which hold a collection of values.
Often, programmers will also have some requirements they want to impose on this collection, such as not storing duplicate elements, or storing the items in sorted order.

%% **** Functionally identical implementations
However, implementing these collection types wastes time, and can be hard to do right for more complicated structures.
Most programmers will instead use one or two of the collection types provided by their language.
Some languages, such as Python, go a step further, providing built-in implementations of common data structures, with special syntax and handling.

%% **** Large difference in performance
However, the underlying implementation of container types which function the same can have a drastic effect on performance (\cite{l_liu_perflint_2009}, \cite{jung_brainy_2011}).
By largely ignoring the performance characteristics of their implementation, programmers may be missing out on large performance gains.

%% *** Motivate w/ effectiveness claims
We propose a system, Candelabra, for the automatic selection of container implementations, based on both user-specified requirements and inferred requirements for performance.
In our testing, we are able to accurately select the best performing containers for a program in significantly less time than brute force.

%% *** Overview of aims & approach
%% **** Ease of adding new container types
We have designed our system with flexibility in mind: adding new container implementations requires little effort.
%% **** Ease of integration into existing projects
It is easy to adopt our system incrementally, and we integrate with existing tools to make doing so easy.
%% **** Scalability to larger projects
The time it takes to select containers scales roughly linearly, even in complex cases, allowing our system to be used even on larger projects.

%% **** Flexibility of selection
We are also able to suggest adaptive containers: containers which switch from one underlying implementation to another once they get past a certain size.
%% **** Overview of results
Whilst we saw reasonable suggestions in our test cases, we found important performance concerns which future work could improve on.

In chapter \ref{chap:background}, we give a more thorough description of the container selection problem, and examine previous work. We outline gaps in existing literature, and how we aim to contribute.

Chapter \ref{chap:design} explains the design of our solution, and how it fulfills the aims set out in chapter \ref{chap:background}. Chapter \ref{chap:implementation} expands on this, describing the implementation work in detail and the challenges faced.

We evaluate the effectiveness of our solution in chapter \ref{chap:results}, and identify several shortcomings that future work could improve upon.

We now elaborate on our implementation, explaining some of the finer details of our design.
With reference to the source code, we explain the structure of our system's implementation, and highlight areas with difficulties.

\section{Modifications to Primrose}

%% API
In order to facilitate integration with Primrose, we refactored large parts of the code to support being called as an API, rather than only through the command line.
This also required updating the older code to a newer edition of Rust, and improving the error handling throughout.

%% Mapping trait
As suggested in the original paper, we added the ability to deal with associative container types: key to value mappings.
We added the \code{Mapping} trait to the implementation library, and updated the type checking and analysis code to support multiple type variables.

Operations on mapping implementations can be modelled and checked against constraints in the same way that regular containers can be.
They are modelled in Rosette as a list of key-value pairs.
\code{src/crates/library/src/hashmap.rs} shows how mapping container types can be declared, and operations on them modelled.

Table \ref{table:library} shows the library of container types we used.
Most come from the Rust standard library, with the exceptions of the \code{SortedVec} family of containers, which use \code{Vec} internally.
The library source can be found in \code{src/crates/library}.

\begin{table}[h]
  \centering
  \begin{tabular}{|c|c|c|}
    Implementation & Description \\
    \hline
    \code{LinkedList} & Doubly-linked list \\
    \code{Vec} & Contiguous growable array \\
    \code{VecSet} & Vec with no duplicates \\
    \code{VecMap} & A Vec of (K, V) tuples, used as a Mapping \\
    \code{SortedVec} & Vec kept in sorted order \\
    \code{SortedVecSet} & Vec kept in sorted order, with no duplicates \\
    \code{VecMap} & A Vec of (K, V) tuples sorted by key, used as a Mapping \\
    \code{HashMap} & Hash map with quadratic probing \\
    \code{HashSet} & Hash map with empty values \\
    \code{BTreeMap} & B-Tree \citep{bayer_organization_1970} map with linear search. \\
    \code{BTreeSet} & B-Tree map with empty values \\
  \end{tabular}
  \caption{Implementations in our library}
  \label{table:library}
\end{table}

We also added new syntax to Primrose's domain-specific language to support defining properties that only make sense for mappings (\code{dictProperty}), however this was unused.

%% Resiliency, etc
While performing integration testing, we found and fixed several other issues with the existing code:

\begin{enumerate}
\item Only push and pop operations could be modelled in properties. Other operations would raise an error during type-checking.
\item The Rosette code generated for properties using other operations was incorrect.
\item Some trait methods used mutable borrows unnecessarily, making it difficult or impossible to write safe Rust using them.
\item The generated code would perform an unnecessary heap allocation for every created container, which could affect performance.
\end{enumerate}

We also added requirements to the \code{Container} and \code{Mapping} traits related to Rust's \code{Iterator} API.
Among other things, this allows us to use for loops, and to more easily move data from one implementation to another.

\section{Building cost models}

%% Benchmarker crate
In order to benchmark container types, we use a seperate crate containing benchmarking code for each trait in the Primrose library (\code{src/crates/benchmarker}).
When benchmarks need to be run for an implementation, we dynamically generate a new crate, which runs all benchmark methods appropriate for the given implementation (\code{src/crate/candelabra/src/cost/benchmark.rs}).

As Rust's generics are monomorphised, our generic code is compiled as if we were using the concrete type in our code, so we don't need to worry about affecting the benchmark results.

Each benchmark is run in a 'warmup' loop for a fixed amount of time (currently 500ms), then runs for a fixed number of iterations (currently 50).
This is important because we are using least squares fitting - if there are less data points at higher $n$ values then our resulting model may not fit those points as well.
We repeat each benchmark at a range of $n$ values: $10, 50, 100, 250, 500, 1000, 6000, 12000, 24000, 36000, 48000, 60000$.

Each benchmark we run corresponds to one container operation.
For most operations, we insert $n$ random values to a new container, then run the operation once per iteration.
For certain operations which are commonly amortized (\code{insert}, \code{push}, and \code{pop}), we instead run the operation itself $n$ times and divide all data points by $n$.

As discussed previously, we discard all points that are outwith one standard deviation of the mean for each $n$ value.
We use the least squares method to fit a polynomial of form $x_0 + x_1 n + x_2 n^2 + x_3 \log_2 n$.

As most operations on common data structures are polynomial or logarithmic complexity, we believe that this function is good enough to capture the cost of most operations.
We originally experimented with coefficients up to $x^3$, but found that this led to overfitting.

\section{Profiling}

We implement profiling using the \code{ProfilerWrapper} type (\code{src/crates/library/src/profiler.rs}), which takes as type parameters the inner container implementation and an index, used later to identify what container type the output corresponds to.
We then implement any Primrose traits that the inner container implements, counting the number of times each operation is called.
We also check the length of the container after each insert operation, and track the maximum.

Tracking is done per-instance, and recorded when the container goes out of scope and its \code{Drop} implementation is called.
We write the counts of each operation and maximum size of the collection to a location specified by an environment variable.

When we want to profile a program, we pick any valid inner implementation for each selection site, and use that candidate with our profiling wrapper as the concrete implementation for that site.
We then run all of the program's benchmarks once, which gives us an equal sample of data from each of them.

This approach has the advantage of giving us information on each individual collection allocated, rather than only statistics for the type as a whole.
For example, if one instance of a container type is used in a very different way from the rest, we will be able to see it more clearly than a normal profiling tool would allow us to.

Although there is noticeable overhead in our current implementation, this is not important as we aren't measuring the program's execution time when profiling.
We could likely reduce profiling overhead by batching file outputs, however this wasn't necessary for us.

\section{Container Selection}
\label{section:impl_selection}

%% Selection Algorithm incl Adaptiv
Selection is done per container type.
For each candidate implementation, we calculate its cost on each partition in the profiler output, then sum these values to get the total estimated cost for each implementation.
This is implemented in \code{src/crates/candelabra/src/profiler/info.rs} and \code{src/crates/candelabra/src/select.rs}.

In order to try and suggest an adaptive container, we use algorithm \ref{alg:adaptive_container}.

\begin{algorithm}
  \caption{Adaptive container suggestion algorithm}
  \label{alg:adaptive_container}
  \begin{algorithmic}
    \State Sort $\mathit{partitions}$ in order of ascending maximum n values.
    \State $\mathit{costs} \gets$ the cost for each candidate in each partition.
    \State $\mathit{best} \gets$ the best candidate per partition
    \State $i \gets$ the lowest index where $\mathit{best}[i] \neq \mathit{best}[0]$
    \If{exists $j < i$ where $\mathit{best}[j] \neq \mathit{best}[0]$, or exists $j\geq i$ where $\mathit{best}[j] \neq \mathit{best}[i]$}
    \State \Return no suggestion
    \EndIf
    \State $\mathit{before} \gets$ name of the candidate in \code{best[0]}
    \State $\mathit{after} \gets$ name of the candidate in \code{best[i]}
    \State $\mathit{threshold} \gets$ halfway between the max n values of partition $i$ and $i-1$
    \State $\mathit{switching\_cost} \gets C_{\mathit{before,clear}}(\mathit{threshold}) + \mathit{threshold} * C_{\mathit{after,insert}}(\mathit{threshold})$
    \State $\mathit{not\_switching\_cost} \gets$ the sum of the difference in cost between $\mathit{before}$ and $\mathit{after}$ for all partitions with index $> i$.
    \If{$\mathit{switching\_cost} > \mathit{not\_switching\_cost}$}
      \State \Return no suggestion
    \Else
      \State \Return suggestion for an adaptive container which switches from $\mathit{before}$ to $\mathit{after}$ when $n$ gets above $\mathit{threshold}$. Its estimated cost is $\mathit{switching\_cost} + \sum_{k=0}^i \mathit{costs}[\mathit{before}][k] + \sum_{k=i}^{|partitions|} \mathit{costs}[\mathit{after}][k]$
    \EndIf
  \end{algorithmic}
\end{algorithm}

\section{Code Generation}

%% Generated code (opaque types)
As mentioned in chapter \ref{chap:design}, we made modifications to Primrose's code generation in order to improve the resulting code's performance.
The original Primrose code would generate code as in Listing \ref{lst:primrose_codegen}.
In order to ensure that users specify all of the traits they need, this code only exposes methods on the implementation that are part of the trait bounds given.
However, it does this by using a \code{dyn} object, Rust's mechanism for dynamic dispatch.

\begin{figure}[h]
  \begin{lstlisting}[caption=Code generated by original Primrose project,label={lst:primrose_codegen},language=Rust]
pub trait StackTrait<T> : Container<T> + Stack<T> {}
impl<T: 'static + Ord + std::hash::Hash> StackTrait<T> for <Stack<T> as ContainerConstructor>::Impl {}


pub struct Stack<T> {
    elem_t: core::marker::PhantomData<T>,
}

impl<T: 'static + Ord + std::hash::Hash> ContainerConstructor for Stack<T> {
    type Impl = Vec<T>;
    type Bound = dyn StackTrait<T>;
    fn new() -> Box<Self::Bound> {
        Box::new(Self::Impl::new())
    }
}
\end{lstlisting}
\end{figure}

Although this approach works, it adds an extra layer of indirection to every call: The caller must use the dyn object's vtable to find the method it needs to call.
This also prevents the compiler from optimising across this boundary.

In order to avoid this, we make use of Rust's support for existential types: Types that aren't directly named, but are inferred by the compiler.
Existential types only guarantee their users the given trait bounds, therefore they accomplish the same goal of forcing users to specify all of their trait bounds upfront.

Figure \ref{lst:new_codegen} shows our equivalent generated code.
The type alias \code{Stack<S>} only allows users to use the \code{Container<S>}, \code{Stack<S>}, and \code{Default} traits.
Our unused 'dummy' function \code{_StackCon} has the return type \code{Stack<S>}.
Rust's type inference step sees that its actual return type is \code{Vec<S>}, and therefore sets the concrete type of \code{Stack<S>} to \code{Vec<S>} at compile time.

\begin{figure}[h]
  \begin{lstlisting}[caption=Code generated with new method,label={lst:new_codegen},language=Rust]
pub type StackCon<S: PartialEq + Ord + std::hash::Hash> = impl Container<S> + Stack<S> + Default;

#[allow(non_snake_case)]
fn _StackCon<S: PartialEq + Ord + std::hash::Hash>() -> StackCon<S> {
    std::vec::Vec::<S>::default()
}
\end{lstlisting}
\end{figure}

Unfortunately, this feature is not yet in stable Rust, meaning we have to opt in to it using an unstable compiler flag (\code{feature(type_alias_impl_trait)}).
At time of writing, the main obstacle to stabilisation appears to be design decisions that only apply to more complicated use-cases, therefore we are confident that this code will remain valid and won't encounter any compiler bugs.


# Thesis

First, ensure that your LaTeX environment is setup properly, as described in the `README` at the project's root.
Then, run `make`.

This converts the graphs from JSON to png using `vl2png` from `vega-cli`, and runs `latexmk`.
